<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Account extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company', 'domain', 'email',
    ];

    /**
     * Get the users of the account
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany('App\User');
    }

    /**
     * Get the stores of the account
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function stores()
    {
        return $this->hasMany('App\Store');
    }

    /**
     * Get the cashiers of the account
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function cashiers()
    {
        return $this->hasMany('App\Cashier');
    }
}
