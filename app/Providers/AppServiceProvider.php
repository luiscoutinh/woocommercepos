<?php

namespace App\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /**
         * Validate that an attribute contains only alphabetic characters and spaces.
         */
        Validator::extend('alpha_spaces', function($attribute, $value, $parameters, $validator) {
            return is_string($value) && preg_match('/^[\pL\pM[:space:]]+$/u', $value);
        });

        /**
         * Validate that an attribute contains only alphabetic characters and spaces.
         */
        Validator::extend('alpha_num_spaces', function($attribute, $value, $parameters, $validator) {
	        if (! is_string($value) && ! is_numeric($value)) {
		        return false;
	        }

	        return preg_match('/^[\pL\pM\pN[:space:]]+$/u', $value) > 0;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        require_once __DIR__ . '/../Http/helpers.php';
    }
}
