<?php

namespace App;

class WcProduct extends WcModel
{
	/** @var string WooCommerce endpoint */
	protected static $endpoint = 'products';

	/** @var array Available parameters */
	protected static $params = [
		'context' => 'view', // String
		'page' => 1, // Integer
		'per_page' => 12, // Integer
		'search' => null, // String
		'after' => null, // String
		'before' => null, // String
		'exclude' => null, // String
		'include' => null, // String
		'offset' => null, // Integer
		'order' => 'desc', // String: asc || desc
		'orderby' => 'date', // String: date || id || include || title || slug
		'filter' => null, // String
		'slug' => null, // String
		'status' => 'any', // String: any || draft || pending || private || publish
		'customer' => null, // String
		'category' => null, // String
		'tag' => null, // String
		'shipping_class' => null, // String
		'attribute' => null, // String
		'attribute_term' => null, // String
		'sku' => null, // String
	];

	/** @var array Variations as objects */
	protected $completeVariations = [];

	/**
	 * Get id
	 *
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

    /**
     * Get date created
     *
     * @return string
     */
	public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Get date modified
     *
     * @return string
     */
	public function getDateModified()
    {
        return $this->date_modified;
    }

	/**
	 * Get SKU
	 *
	 * @return string
	 */
	public function getSku()
	{
		return $this->sku;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Get type
	 *
	 * @return string
	 */
	public function getType()
	{
		switch ($this->type) {
			case 'simple' :
				$type = 'Simple';
				break;
			case 'grouped' :
				$type = 'Grouped';
				break;
			case 'external' :
				$type = 'External';
				break;
			case 'variable' :
				$type = 'Variable';
				break;
			default : // Simple
				$type = 'Simple';
				break;
		}

		return $type;
	}

	/**
	 * Get status
	 *
	 * @return string
	 */
	public function getStatus()
	{
		switch ($this->status) {
			case 'draft' :
				$status = 'Draft';
				break;
			case 'pending' :
				$status = 'Pending';
				break;
			case 'private' :
				$status = 'Private';
				break;
			case 'publish' :
				$status = 'Publish';
				break;
			default : // Publish
				$status = 'Publish';
				break;
		}

		return $status;
	}

	/**
	 * Get catalog visibility
	 *
	 * @return string
	 */
	public function getCatalogVisibility()
	{
		switch ($this->catalog_visibility) {
			case 'visible' :
				$catalogVisibility = ['name' => $this->catalog_visibility, 'description' => 'Catalog and search'];
				break;
			case 'catalog' :
				$catalogVisibility = ['name' => $this->catalog_visibility, 'description' => 'Only in catalog'];
				break;
			case 'search' :
				$catalogVisibility = ['name' => $this->catalog_visibility, 'description' => 'Only in search'];
				break;
			case 'hidden' :
				$catalogVisibility = ['name' => $this->catalog_visibility, 'description' => 'Hidden from all'];
				break;
			default : // Visible
				$catalogVisibility = ['name' => $this->catalog_visibility, 'description' => 'Catalog and search'];
				break;
		}

		return $catalogVisibility;
	}

	/**
	 * Get short description
	 *
	 * @return string
	 */
	public function getShortDescription()
	{
		return $this->short_description;
	}

	/**
	 * Get description
	 *
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * Get price
	 *
	 * @return string
	 */
	public function getPrice()
	{
		return $this->price;
	}

	/**
	 * Get regular price
	 *
	 * @return string
	 */
	public function getRegularPrice()
	{
		return $this->regular_price;
	}

	/**
	 * Get sale price
	 *
	 * @return string
	 */
	public function getSalePrice()
	{
		return $this->sale_price;
	}

	/**
	 * Get price in html
	 *
	 * @return mixed
	 */
	public function getPriceHtml()
	{
		return $this->price_html;
	}

	/**
	 * Get date on sale from
	 *
	 * @return string
	 */
	public function getDateOnSaleFrom()
	{
		return $this->date_on_sale_from;
	}

	/**
	 * Get date on sale to
	 *
	 * @return string
	 */
	public function getDateOnSaleTo()
	{
		return $this->date_on_sale_to;
	}

	/**
	 * Get total sales
	 *
	 * @return int
	 */
	public function getTotalSales()
	{
		return $this->total_sales;
	}

	/**
	 * Get tax status
	 *
	 * @return string
	 */
	public function getTaxStatus()
	{
		switch ($this->tax_status) {
			case 'taxable' :
				$taxStatus = 'Taxable';
				break;
			case 'shipping' :
				$taxStatus = 'Shipping';
				break;
			case 'none' :
				$taxStatus = 'None';
				break;
			default : // taxable
				$taxStatus = 'Taxable';
				break;
		}
		return $taxStatus;
	}

	/**
	 * Get tax class
	 *
	 * @return string
	 */
	public function getTaxClass()
	{
		return $this->tax_class;
	}

	/**
	 * Get stock quantity
	 *
	 * @return int
	 */
	public function getStockQuantity()
	{
		return $this->stock_quantity;
	}

	/**
	 * Get backorders
	 *
	 * @return array
	 */
	public function getBackorders()
	{
		switch ($this->backorders) {
			case 'no' :
				$backorders = ['name' => $this->backorders, 'description' => 'Do not allow'];
				break;
			case 'notify' :
				$backorders = ['name' => $this->backorders, 'description' => 'Allow, but notify customer'];
				break;
			case 'yes' :
				$backorders = ['name' => $this->backorders, 'description' => 'Allow'];
				break;
			default : // no
				$backorders = ['name' => $this->backorders, 'description' => 'Do not allow'];
				break;
		}

		return $backorders;
	}

	/**
	 * Get weight
	 *
	 * @return string
	 */
	public function getWeight()
	{
		return $this->weight;
	}

	/**
	 * Get dimensions (length, width and height)
	 *
	 * @return array
	 */
	public function getDimensions()
	{
		return $this->dimensions;
	}

	/**
	 * Get average rating
	 *
	 * @return string
	 */
	public function getAverageRating()
	{
		return $this->average_rating;
	}

	/**
	 * Get rating count
	 *
	 * @return int
	 */
	public function getRatingCount()
	{
		return $this->rating_count;
	}

	/**
	 * Get purchase note
	 *
	 * @return string
	 */
	public function getPurchaseNote()
	{
		return $this->purchase_note;
	}

	/**
	 * Get attributes
	 *
	 * @return array
	 */
	public function getAttributes()
	{
		return $this->attributes;
	}

	/**
	 * Get default attributes
	 *
	 * @return array
	 */
	public function getDefaultAttributes()
	{
		return $this->default_attributes;
	}

	/**
	 * Get variations
	 *
	 * @return array
	 */
	public function getVariations()
	{
		if (strtolower($this->getType()) === 'variable' && !$this->completeVariations) {
			foreach ($this->variations as $var) {
				$this->completeVariations[] = WcProduct::filledWith($var);
			}
		}

		return $this->completeVariations;
	}

	/**
	 * Get images
	 *
	 * @return array
	 */
	public function getImages()
	{
		return $this->images;
	}

	/**
	 * Get variation image
	 *
	 * @return array
	 */
	public function getImage()
	{
		return $this->image;
	}

	/**
	 * Get permalink url
	 *
	 * @return string
	 */
	public function getPermalink()
	{
		return $this->permalink;
	}

	/**
	 * Get categories
	 *
	 * @return array
	 */
	public function getCategories()
	{
		return $this->categories;
	}

	/**
	 * Get tags
	 *
	 * @return array
	 */
	public function getTags()
	{
		return $this->tags;
	}

	/**
	 * Check if the product is featured
	 *
	 * @return bool
	 */
	public function isFeatured()
	{
		return $this->featured;
	}

	/**
	 * Check if the product is on sale
	 *
	 * @return bool
	 */
	public function isOnSale()
	{
		return $this->on_sale;
	}

	/**
	 * Check if the product is purchasable
	 *
	 * @return bool
	 */
	public function isPurchaseable()
	{
		return $this->purchasable;
	}

	/**
	 * Check if the product is a virtual product
	 *
	 * @return bool
	 */
	public function isVirtual()
	{
		return $this->virtual;
	}

	/**
	 * Check if the product is downloadable
	 *
	 * @return bool
	 */
	public function isDownloadable()
	{
		return $this->downloadable;
	}

	/**
	 * Check if backorders are allowed
	 *
	 * @return bool
	 */
	public function areBackordersAllowed()
	{
		return $this->backorders_allowed;
	}

	/**
	 * Check if the product is backordered
	 *
	 * @return string
	 */
	public function isBackordered()
	{
		return $this->backordered;
	}

	/**
	 * Check if the product is sold individually
	 *
	 * @return bool
	 */
	public function isSoldIndividually()
	{
		return $this->sold_individually;
	}

	/**
	 * Check if the product is stock manageable
	 *
	 * @return bool
	 */
	public function isStockManageable()
	{
		return $this->manage_stock;
	}

	/**
	 * Check if the reviews are allowed for this product
	 *
	 * @return bool
	 */
	public function isReviewable()
	{
		return $this->reviews_allowed;
	}

	/**
	 * Check if the product is in stock
	 *
	 * @return bool
	 */
	public function inStock()
	{
		return $this->in_stock;
	}
}
