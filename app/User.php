<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type', 'username', 'password', 'name',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the account that owns the user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function account()
    {
        return $this->belongsTo('App\Account');
    }

    /**
     * Get the stores that belong to the user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function stores()
    {
        return $this->belongsToMany('App\Store')->withTimestamps();
    }

    /**
     * Get all the cashiers for the user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function cashiers()
    {
        return $this->from('cashiers')
            ->join('cashier_store', 'cashier_store.cashier_id', '=', 'cashiers.id')
            ->join('store_user', 'store_user.store_id', '=', 'cashier_store.store_id')
            ->join('users', 'users.id', '=', 'store_user.user_id')
            ->where('store_user.user_id', $this->id)
            ->distinct()
            ->select('cashiers.id', 'cashiers.*');
    }
}
