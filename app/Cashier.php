<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cashier extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'password',
    ];

    /**
     * Bind the authenticated user to Store Model queries
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(function($query) {
            $query->where('account_id', auth()->user()->account->id);
        });
    }

    /**
     * Get the account that owns the cashier
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function account()
    {
        return $this->belongsTo('App\Account');
    }

    /**
     * Get all the users for the cashier
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->hasManyThrough('App\User', 'App\Store')->withTimestamps();
    }

    /**
     * Get the stores that belong to the cashier
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function stores()
    {
        return $this->belongsToMany('App\Store')->withTimestamps();
    }
}
