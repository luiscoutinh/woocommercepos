<?php

namespace App\Http\Controllers;

use App\WcCustomer;
use App\WcProduct;
use Illuminate\Http\Request;

use App\Http\Requests;

class PosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Show the POS dashboard
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showDashboard()
    {
        if (($test = test_wc_settings()) !== true) return $test;

        if (session()->has('store') && session()->has('cashier')) {
            $view = view(
                'pos.index',
                [
                    'order' => session('order')
                ]
            );
        } else {

            /** @var Stores $stores */
            $stores = $this->user->type === 'admin'
                ? $this->user->account->stores
                : $this->user->stores;

            /** @var Cashiers $cashiers */
            $cashiers = $this->user->account->cashiers;

            $view = view(
                'pos.login.stores',
                [
                    'stores' => $stores,
                    'cashiers' => $cashiers,
                ]
            );
        }

        return $view;
    }

    /**
     * Cancel the order and create another one
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function cancelOrder()
    {
        pos_destroy_order();
        pos_create_order(session('store'), session('cashier'));

        return redirect('/pos');
    }

    /**
     * Get product by searching on select2
     */
    public function searchProduct($subdomain, Request $request)
    {
        $search = [
            'page' => $request->page,
            'per_page' => 5,
            'search' => $request->q,
        ];

        $products = WcProduct::all($search);
        foreach($products->toArray()['data'] as &$product) {
            $product->view = view(
                'pos/products/select2',
                [
                    'product' => $product
                ]
            )->render();

            if (strtolower($product->getType()) === 'variable') {
                $product->variations_view = view(
                    'pos/products/variations',
                    [
                        'product' => $product,
                    ]
                )->render();
            }
        }

        return response()->json(
            [
                'items' => $products->toArray()['data'],
                'total_count' => $products->total(),
            ]
        );
    }

    /**
     * Get response to refresh order line items and totals by ajax
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLineItemsAndTotals()
    {
        return response()
            ->json(
                [
                    'products' => view(
                        'orders/order/line_items',
                        [
                            'order' => session('order'),
                            'isPos' => true,
                        ]
                    )->render(),
                    'totals' => view(
                        'pos/totals/index',
                        [
                            'order' => session('order')
                        ]
                    )->render(),
                ]
            );
    }

    /**
     * Add a product to the order
     *
     * @param $subdomain
     * @return \Illuminate\Http\JsonResponse|null
     */
    public function addProduct($subdomain, Request $request)
    {
        $sku = $request->sku;
        $products = WcProduct::all(['sku' => $sku]);

        if (!$products->total()) {
            return null;
        }

        $product = WcProduct::filledWith($products[0]);
        session('order')->addProduct($product);

        return $this->getLineItemsAndTotals();
    }

    /**
     * @param $subdomain
     * @param $lineItemId
     * @param $qty
     * @param $price
     * @return \Illuminate\Http\JsonResponse|null
     */
    public function updateProduct($subdomain, $lineItemId, Request $request)
    {
        $lineItem = session('order')->getLineItem($lineItemId);

        $isPercentage = strpos($request->price, '%') !== false;
        $qty = $request->qty;
        $price = trim($request->price, '%');


        if (!$isPercentage && $price > 0) {
            $finalPrice = $price * $qty;
        } elseif (!$isPercentage && $price < 0) {
            $finalPrice = $lineItem['subtotal'] * $qty + $price * $qty;
        } elseif ($isPercentage && $price > 0) {
            $finalPrice = $lineItem['subtotal'] * $qty * $price * $qty / 100;
        } elseif ($isPercentage && $price < 0) {
            $finalPrice = $lineItem['subtotal'] * $qty - $lineItem['subtotal'] * abs($price) * $qty / 100;
        }

        session('order')->updateProduct($lineItem, $qty, $finalPrice);

        return $this->getLineItemsAndTotals();
    }

    /**
     * Remove a product to the order
     *
     * @param $subdomain
     * @param $lineItemId
     * @return \Illuminate\Http\JsonResponse|null
     */
    public function removeProduct($subdomain, $lineItemId, Request $request)
    {
        $lineItem = session('order')->getLineItem($lineItemId);
        session('order')->removeProduct($lineItem);

        return $this->getLineItemsAndTotals();
    }

    /**
     * Get product by searching on select2
     */
    public function searchCustomer($subdomain, Request $request)
    {
        $search = [
            'page' => $request->page,
            'per_page' => 5,
            'search' => $request->q,
        ];

        $customers = WcCustomer::all($search);
        foreach($customers->toArray()['data'] as &$customer) {
            $customer->view = view(
                'pos/customers/select2',
                [
                    'customer' => $customer
                ]
            )->render();
        }

        return response()->json(
            [
                'items' => $customers->toArray()['data'],
                'total_count' => $customers->total(),
            ]
        );
    }

    /**
     * Add a customer to the order
     *
     * @param $subdomain
     * @return \Illuminate\Http\JsonResponse|null
     */
    public function addCustomer($subdomain, Request $request)
    {
        $customerId = $request->customer_id;
        $billing = [
            'company' => $request->billing['company'],
            'first_name' => $request->billing['first_name'],
            'last_name' => $request->billing['last_name'],
            'address_1' => $request->billing['address_1'],
            'address_2' => $request->billing['address_2'],
            'city' => $request->billing['city'],
            'state' => $request->billing['state'],
            'postcode' => $request->billing['postcode'],
            'country' => $request->billing['country'],
            'email' => $request->billing['email'],
            'phone' => $request->billing['phone'],
        ];

        session('order')->addCustomer($customerId, $billing);

        return response()->json(
            [
                'status' => 1,
            ]
        );
    }

    /**
     * Add a payment to the order
     *
     * @param $subdomain
     * @return \Illuminate\Http\JsonResponse|null
     */
    public function addPayment($subdomain, Request $request)
    {
        $paymentMethods = $request->payment_methods;

        session('order')->addPayment($paymentMethods);

        pos_create_order(session('store'), session('cashier'));

        return redirect('/pos');
    }
}