<?php

namespace App\Http\Controllers;

use App\WcCategory;
use App\WcProduct;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    use HandlesAuthorization;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @param $subdomain
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($subdomain, Request $request)
    {
        if (($test = test_wc_settings()) !== true) return $test;

        $hasSearch = isset($request->search) ? true : false;
        $search = [
                'context' => 'view',
                'page' => $request->page,
                'per_page' => 12,
                'search' => $request->search,
                'sku' => $request->sku,
                'status' => $request->status,
                'category' => $request->category,
                'after' => $request->after,
                'before' => $request->before,
                'order' => $request->order,
                'orderby' => $request->orderby,
            ];

        $products = WcProduct::all($search);
        $categories = WcCategory::all();

        return view(
            'products.index',
            [
                'has_search' => $hasSearch,
                'search' => $search,
                'products' => $products,
                'categories' => $categories,
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

	/**
	 * Display the specified resource.
	 *
	 * @param $subdomain
	 * @param $id
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
    public function show($subdomain, $id)
    {
	    /** @var WcProduct $customer */
	    $product = WcProduct::findOrFail($id);

	    return view(
		    'products.show',
		    [
			    'product' => $product
		    ]
	    );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
