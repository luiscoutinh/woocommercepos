<?php

namespace App\Http\Controllers;

use App\User;
use App\Store;
use Illuminate\Http\Request;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    use HandlesAuthorization;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get a validator for store
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'type' => 'required|in:admin,manager,user',
            'username' => 'required|alpha_num|max:100',
            'name' => 'required|alpha_spaces|max:120',
            'password' => '',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->user->can('view', User::class) || $this->deny();

        $admins = $this->user->account->users->where('type', 'admin')->sortBy('name');
        $users = $this->user->account->users->where('type', '<>', 'admin')->sortBy('name')->sortBy('type');
        $stores = $this->user->account->stores;

        return view(
            'users.index',
            [
                'admins' => $admins,
                'users' => $users,
                'stores' => $stores,
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->user->can('create', User::class) || $this->deny();

        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param $subdomain
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store($subdomain, Request $request)
    {
        $this->user->can('create', User::class) || $this->deny();

        $this->validator($request->all())->validate();

        $user = new User();
        $user->account_id = $this->user->account_id;
        $user->type = $request->type;
        $user->username = $request->username;
        $user->password = Hash::make($request->password);
        $user->name = $request->name;

        $user->save();

        return redirect()->route(
            'users.index',
            [
                'subdomain' => $subdomain
            ]
        );
    }

    /**
     * Display the specified resource.
     *
     * @param $subdomain
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($subdomain, $id)
    {
        $user = User::findOrFail($id);

        $this->user->can('view', $user) || $this->deny();

        $stores = $user->type === 'admin'
            ? $this->user->account->stores
            : $user->stores;

        return view(
            'users.show',
            [
                'user' => $user,
                'stores' => $stores,
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $subdomain
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($subdomain, $id)
    {
        $user = User::findOrFail($id);

        $this->user->can('update', $user) || $this->deny();

        return view(
            'users.edit',
            [
                'user' => $user
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $subdomain
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($subdomain, Request $request, $id)
    {
        $user = User::findOrFail($id);

        $this->user->can('update', $user) || $this->deny();

        $this->validator($request->all())->validate();

        $user->type = $request->type;
        $user->username = $request->username;
        $user->name = $request->name;

        if ($request->password) {
            $user->password = Hash::make($request->password);
        }

        $user->save();

        return redirect()
            ->route(
                'users.index',
                [
                    'subdomain' => $subdomain
                ]
            );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $subdomain
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($subdomain, $id)
    {
        $user = User::findOrFail($id);

        $this->user->can('delete', $user) || $this->deny();

        $user->delete();

        return redirect()
            ->route(
                'users.index',
                [
                    'subdomain' => $subdomain
                ]
            );
    }

    /**
     * Attach store to an user
     *
     * @param $subdomain
     * @param Request $request
     * @param $userId
     * @return $this|void
     */
    public function attachStore($subdomain, Request $request, $userId)
    {
        $storeId = $request->input('store_id', null);

        $user = User::findOrFail($userId);
        $store = Store::findOrFail($storeId);

        ($this->user->can('update', $user) && $this->user->can('update', $store)) || $this->deny();

        if (!$user->stores->contains($store)) {
            $user->stores()->attach($store);
            return response()
                ->view(
                    'users/store_user',
                    [
                        'user' => $user,
                        'store' => $store
                    ]
                )
                ->header('Content-Type', 'html');
        }

        return;
    }

    /**
     * Detach a store from an user
     *
     * @param $subdomain
     * @param Request $request
     * @param $userId
     * @param $storeId
     * @return \Illuminate\Http\JsonResponse
     */
    public function detachStore($subdomain, Request $request, $userId, $storeId)
    {
        $user = User::findOrFail($userId);
        $store = Store::findOrFail($storeId);

        ($this->user->can('update', $user) && $this->user->can('update', $store)) || $this->deny();

        $removed = $user->stores->contains($store) && $user->stores()->detach($store) ? true : false;

        return response()
            ->json(
                [
                    'removed' => $removed,
                ]
            );
    }
}
