<?php

namespace App\Http\Controllers;

use App\Cashier;
use App\Store;
use App\Http\Requests;
use App\WcOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Access\HandlesAuthorization;
use Validator;

class StoreController extends Controller
{
    use HandlesAuthorization;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get a validator for store
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|alpha_spaces|max:255',
            'timezone' => 'required|timezone',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->user->can('view', Store::class) || $this->deny();

        $stores = $this->user->type === 'admin'
            ? $this->user->account->stores
            : $this->user->stores;
        $cashiers = $this->user->account->cashiers;

        return view(
            'stores.index',
            [
                'stores' => $stores,
                'cashiers' => $cashiers
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->user->can('create', Store::class) || $this->deny();

        return view('stores.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param $subdomain
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store($subdomain, Request $request)
    {
        $this->user->can('create', Store::class) || $this->deny();

        $this->validator($request->all())->validate();

        $store = new Store();
        $store->account_id = $this->user->account_id;
        $store->name = $request->name;
        $store->timezone = $request->timezone;

        $store->save();

        return redirect()->route(
            'stores.index',
            [
                'subdomain' => $subdomain
            ]
        );
    }

    /**
     * Display the specified resource.
     *
     * @param $subdomain
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($subdomain, $id)
    {
        $store = Store::findOrFail($id);

        $this->user->can('view', $store) || $this->deny();

        return view(
            'stores.show',
            [
                'store' => $store
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $subdomain
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($subdomain, $id)
    {
        $store = Store::findOrFail($id);

        $this->user->can('update', $store) || $this->deny();

        return view(
            'stores.edit',
            [
                'store' => $store
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $subdomainc
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($subdomain, Request $request, $id)
    {
        $store = Store::findOrFail($id);

        $this->user->can('update', $store) || $this->deny();

        $this->validator($request->all())->validate();

        $store->name = $request->name;
        $store->timezone = $request->timezone;

        $store->save();

        return redirect()
            ->route(
                'stores.index',
                [
                    'subdomain' => $subdomain
                ]
            );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $subdomain
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($subdomain, $id)
    {
        $store = Store::findOrFail($id);

        $this->user->can('delete', $store) || $this->deny();

        $store->delete();

        return redirect()
            ->route('stores.index',
                [
                    'subdomain' => $subdomain
                ]
            );
    }

    /**
     * Show form to login a cashier
     *
     * @param $subdomain
     * @param Request $request
     * @param $storeId
     * @param $cashierId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function showCashierLogin($subdomain, Request $request, $storeId, $cashierId)
    {
        /** @var Store $store */
        $store = Store::findOrFail($storeId);
        /** @var Cashier $cashier */
        $cashier = Cashier::findOrFail($cashierId);

        $this->user->can('view', $store) || $this->deny();

        // Redirects the user to cashiers list of the store, if the given user has not authorized to access the given store
        if (!$cashier->stores->contains($store)) {
            return redirect()->route('stores.show', ['subdomain' => $subdomain, 'id' => $storeId]);
        } elseif ($this->user->type === 'admin' || !$cashier->password) {
            // Add store and cashier to session
            $this->saveLoggedInCashierStore($store, $cashier);

            return redirect()
                ->route(
                    'showPos',
                    [
                        'subdomain' => $subdomain
                    ]
                );
        }

        return view(
            'stores.login',
            [
                'store' => $store,
                'cashier' => $cashier
            ]
        );
    }

    /**
     * Login cashier
     *
     * @param $subdomain
     * @param Request $request
     * @param $storeId
     * @param $cashierId
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function doCashierLogin($subdomain, Request $request, $storeId, $cashierId)
    {
        $store = Store::findOrFail($storeId);
        $cashier = Cashier::findOrFail($cashierId);

        $this->user->can('view', $store) || $this->deny();

        $password = $request->input('password', '');

        // Wrong cashier password
        if (!Hash::check($password, $cashier->password)) {
            return redirect()
                ->route(
                    'stores.cashier_login',
                    [
                        'subdomain' => $subdomain,
                        'store_id' => $storeId,
                        'cashier_id' => $cashierId
                    ]
                )
                ->withErrors(
                    [
                        'password' => 'Invalid password'
                    ]
                );
        } else {
            // Rehash password when is needed
            if (Hash::needsRehash($cashier->password)) {
                $cashier->password = Hash::make($password);
                $cashier->save();
            }
        }

        // Add store and cashier to session
        $this->saveLoggedInCashierStore($store, $cashier);

        return redirect()
            ->route(
                'showPos',
                [
                    'subdomain' => $subdomain
                ]
            );
    }

    /**
     * Add store and cashier to session
     *
     * @param Store $store
     * @param Cashier $cashier
     */
    protected function saveLoggedInCashierStore(Store $store, Cashier $cashier)
    {
        session(
            [
                'store' => $store,
                'cashier' => $cashier,
            ]
        );

        pos_create_order($store, $cashier);
    }

    /**
     * Forget store and cashier from session
     *
     */
    public function forgetLoggedInCashierStore()
    {
        session()->forget('store');
        session()->forget('cashier');

        pos_destroy_order();

        return redirect('home');
    }

    /**
     * Attach cashier to a store
     *
     * @param $subdomain
     * @param Request $request
     * @param $storeId
     * @return $this|void
     */
    public function attachCashier($subdomain, Request $request, $storeId)
    {
        $cashierId = $request->input('cashier_id', null);

        $store = Store::findOrFail($storeId);
        $cashier = Cashier::findOrFail($cashierId);

        $this->user->can('update', $store) || $this->deny();

        if (!$store->cashiers->contains($cashier)) {
            $store->cashiers()->attach($cashier);
            return response()
                ->view(
                    'stores/cashier_store',
                    [
                        'store' => $store,
                        'cashier' => $cashier
                    ]
                )
                ->header('Content-Type', 'html');
        }

        return;
    }

    /**
     * Detach a cashier from a store
     *
     * @param $subdomain
     * @param Request $request
     * @param $storeId
     * @param $cashierId
     * @return \Illuminate\Http\JsonResponse
     */
    public function detachCashier($subdomain, Request $request, $storeId, $cashierId)
    {
        $store = Store::findOrFail($storeId);
        $cashier = Cashier::findOrFail($cashierId);

        $this->user->can('update', $store) || $this->deny();

        $removed = $store->cashiers->contains($cashier) && $store->cashiers()->detach($cashier) ? true : false;

        return response()
            ->json(
                [
                    'removed' => $removed,
                ]
            );
    }
}
