<?php

namespace App\Http\Controllers\Auth;

use App\Account;
use App\Cashier;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration (defined in create method, because depends on the subdomain)
     *
     * @var string
     */
    public $redirectTo = '';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'company' => 'required|max:255',
            'subdomain' => 'required|alpha|max:200|unique:accounts',
            'email' => 'required|email|max:255',
            'name' => 'required|alpha_spaces|max:120',
            'username' => 'required|alpha_num|max:100',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $subdomain = $data['subdomain'] . '.' . config('app.domain');
        $this->redirectTo = 'http://' . $subdomain;

        $account = new Account();
        $account->company = $data['company'];
        $account->subdomain = $data['subdomain'];
        $account->email = $data['email'];

        $user = new User();
        $user->name = $data['name'];
        $user->username = $data['username'];
        $user->password = Hash::make($data['password']);

        $cashier = new Cashier();
        $cashier->name = $data['name'];

        DB::transaction(function () use ($account, $user, $cashier) {
            // Create the account
            $account->save();

            // Create an admin user
            $user->account_id = $account->id;
            $user->save();

            // Create a cashier for admin user
            $cashier->account_id = $account->id;
            $cashier->save();
        });

        return $user;
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        return redirect($this->redirectPath());
    }
}
