<?php

namespace App\Http\Controllers\Auth;

use App\Account;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Get a validator for store
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorSubdomain(array $data)
    {
        return Validator::make($data, [
            'subdomain' => 'required|exists:accounts,subdomain'
        ]);
    }

    /**
     * Get the login username to be used by the controller.
     * (vendor: email)
     *
     * @return string
     */
    public function username()
    {
        return 'username';
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        $subdomain = $request->route()->parameter('account');

        /** @var Account $account */
        $account = Account::where('subdomain', $subdomain)->firstOrFail();

        return array_merge($request->only($this->username(), 'password'), ['account_id' => $account->getKey()]);
    }

    /**
     * Redirect users to theirs own subdomains
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function loginSubdomain(Request $request)
    {
        $this->validatorSubdomain($request->all())->validate();

        return redirect()->route('login', ['account' => $request->input('subdomain')]);
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm($account)
    {
        $account = Account::where('subdomain', $account)->firstOrFail();

        return view(
            'auth.login',
            [
                'account' => $account,
            ]
        );
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginSubdomainForm()
    {
        return view(
            'auth.login_subdomain'
        );
    }
}
