<?php

namespace App\Http\Controllers;

use App\Cashier;
use App\Store;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Hash;

class CashierController extends Controller
{
    use HandlesAuthorization;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get a validator for cashier
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|alpha_spaces|max:60',
            'password' => 'min:4',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->user->can('view', Cashier::class) || $this->deny();

        $cashiers = $this->user->account->cashiers;
        $stores = $this->user->type === 'admin'
            ? $this->user->account->stores
            : $this->user->stores;

        return view(
            'cashiers.index',
            [
                'cashiers' => $cashiers,
                'stores' => $stores,
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->user->can('create', Cashier::class) || $this->deny();

        return view('cashiers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param $subdomain
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store($subdomain, Request $request)
    {
        $this->user->can('create', Cashier::class) || $this->deny();

        $this->validator($request->all())->validate();

        $cashier = new Cashier();
        $cashier->account_id = $this->user->account_id;
        $cashier->name = $request->name;

        if ($request->password) {
            $cashier->password = Hash::make($request->password);
        }

        $cashier->save();

        return redirect()->route(
            'cashiers.index',
            [
                'subdomain' => $subdomain
            ]
        );
    }

    /**
     * Display the specified resource.
     *
     * @param $subdomain
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($subdomain, $id)
    {
        $cashier = Cashier::findOrFail($id);

        $this->user->can('view', $cashier) || $this->deny();

        $stores = $cashier->stores;

        return view(
            'cashiers.show',
            [
                'cashier' => $cashier,
                'stores' => $stores,
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $subdomain
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($subdomain, $id)
    {
        $cashier = Cashier::findOrFail($id);

        $this->user->can('update', $cashier) || $this->deny();

        return view(
            'cashiers.edit',
            [
                'cashier' => $cashier
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $subdomain
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($subdomain, Request $request, $id)
    {
        $cashier = Cashier::findOrFail($id);

        $this->user->can('update', $cashier) || $this->deny();

        $this->validator($request->all())->validate();

        $cashier->name = $request->name;

        if ($request->password) {
            $cashier->password = Hash::make($request->password);
        }

        $cashier->save();

        return redirect()
            ->route(
                'cashiers.index',
                [
                    'subdomain' => $subdomain
                ]
            );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $subdomain
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($subdomain, $id)
    {
        $cashier = Cashier::findOrFail($id);

        $this->user->can('delete', $cashier) || $this->deny();

        $cashier->delete();

        return redirect()
            ->route(
                'cashiers.index',
                [
                    'subdomain' => $subdomain
                ]
            );
    }

    /**
     * Attach store to a cashier
     *
     * @param $subdomain
     * @param Request $request
     * @param $cashierId
     * @return $this|void
     */
    public function attachStore($subdomain, Request $request, $cashierId)
    {
        $storeId = $request->input('store_id', null);

        $cashier = Cashier::findOrFail($cashierId);
        $store = Store::findOrFail($storeId);

        $this->user->can('update', $store) || $this->deny();

        if (!$cashier->stores->contains($store)) {
            $cashier->stores()->attach($store);
            return response()
                ->view(
                    'cashiers/cashier_store',
                    [
                        'cashier' => $cashier,
                        'store' => $store,
                    ]
                )
                ->header('Content-Type', 'html');
        }

        return;
    }

    /**
     * Detach a store from a cashier
     *
     * @param $subdomain
     * @param Request $request
     * @param $cashierId
     * @param $storeId
     * @return \Illuminate\Http\JsonResponse
     */
    public function detachStore($subdomain, Request $request, $cashierId, $storeId)
    {
        $cashier = Cashier::findOrFail($cashierId);
        $store = Store::findOrFail($storeId);

        $this->user->can('update', $store) || $this->deny();

        $removed = $cashier->stores->contains($store) && $cashier->stores()->detach($store) ? true : false;

        return response()
            ->json(
                [
                    'removed' => $removed,
                ]
            );
    }
}
