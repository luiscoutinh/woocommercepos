<?php

namespace App\Http\Controllers;

use App\WcOrder;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    use HandlesAuthorization;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @param $subdomain
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($subdomain, Request $request)
    {
        if (($test = test_wc_settings()) !== true) return $test;

        $hasSearch = isset($request->search) ? true : false;
        $search = [
            'page' => $request->page,
            'per_page' => 12,
            'search' => $request->search,
            'product' => $request->product,
            'status' => $request->status,
            'after' => $request->after,
            'before' => $request->before,
            'order' => $request->order,
            'orderby' => $request->orderby,
        ];

        $orders = WcOrder::all($search);

        return view(
            'orders.index',
            [
                'has_search' => $hasSearch,
                'search' => $search,
                'orders' => $orders,
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param $subdomain
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($subdomain, $id)
    {
        /** @var WcOrder $order */
        $order = WcOrder::findOrFail($id);

        return view(
            'orders.show',
            [
                'order' => $order
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
