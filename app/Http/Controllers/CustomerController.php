<?php

namespace App\Http\Controllers;

use App\WcCustomer;
use Automattic\WooCommerce\HttpClient\HttpClientException;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CustomerController extends Controller
{
    use HandlesAuthorization;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get a validator for store
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'alpha_spaces|max:255',
            'last_name' => 'alpha_spaces|max:255',
            'email' => 'required|email',
            'username' => 'alpha_num',
            'billing.company' => 'alpha_num_spaces',
            'billing.first_name' => 'alpha_spaces',
            'billing.last_name' => 'alpha_spaces',
            'billing.address_1' => 'alpha_num_spaces',
            'billing.address_2' => 'alpha_num_spaces',
            'billing.city' => 'alpha_num_spaces',
            'billing.state' => 'alpha_num_spaces',
            'billing.postcode' => 'alpha_dash',
            'billing.email' => 'email',
            'billing.phone' => 'alpha_num_spaces',
            'shipping.company' => 'alpha_num_spaces',
            'shipping.first_name' => 'alpha_spaces',
            'shipping.last_name' => 'alpha_spaces',
            'shipping.address_1' => 'alpha_num_spaces',
            'shipping.address_2' => 'alpha_num_spaces',
            'shipping.city' => 'alpha_num_spaces',
            'shipping.state' => 'alpha_num_spaces',
            'shipping.postcode' => 'alpha_dash',
        ]);
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @param $subdomain
	 * @param Request $request
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
    public function index($subdomain, Request $request)
    {
        if (($test = test_wc_settings()) !== true) return $test;

        $hasSearch = isset($request->search) ? true : false;
        $search = [
            'context' => 'view',
            'page' => $request->page,
            'per_page' => 12,
            'search' => $request->search,
            'email' => $request->email,
            'order' => $request->order,
            'orderby' => $request->orderby,
        ];

        $customers = WcCustomer::all($search);

        return view(
            'customers.index',
            [
                'has_search' => $hasSearch,
                'search' => $search,
                'customers' => $customers,
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customers.create');
    }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param $subdomain
	 * @param Request $request
	 *
	 * @return $this|\Illuminate\Http\RedirectResponse
	 */
    public function store($subdomain, Request $request)
    {
	    $this->validator($request->all())->validate();

	    $customer = new WcCustomer();
	    $customer->first_name = $request->first_name;
	    $customer->last_name = $request->last_name;
	    $customer->email = $request->email;
	    $customer->username = $request->username;
	    $customer->billing = [
	    	'company' => $request->billing['company'],
	    	'first_name' => $request->billing['first_name'],
	    	'last_name' => $request->billing['last_name'],
	    	'address_1' => $request->billing['address_1'],
	    	'address_2' => $request->billing['address_2'],
	    	'city' => $request->billing['city'],
	    	'state' => $request->billing['state'],
	    	'postcode' => $request->billing['postcode'],
	    	'country' => $request->billing['country'],
	    	'email' => $request->billing['email'],
	    	'phone' => $request->billing['phone'],
	    ];
	    $customer->shipping = [
	    	'company' => $request->shipping['company'],
	    	'first_name' => $request->shipping['first_name'],
	    	'last_name' => $request->shipping['last_name'],
	    	'address_1' => $request->shipping['address_1'],
	    	'address_2' => $request->shipping['address_2'],
	    	'city' => $request->shipping['city'],
	    	'state' => $request->shipping['state'],
	    	'postcode' => $request->shipping['postcode'],
	    	'country' => $request->shipping['country'],
	    ];

	    try {
		    $customer->save();
	    } catch (HttpClientException $e) {
		    $error = json_decode($e->getResponse()->getBody());

		    return redirect()
			    ->route(
				    'customers.create',
				    [
					    'subdomain' => $subdomain
				    ]
			    )
			    ->withErrors(
			    	[
			    		$error->message ?: '',
				    ]
			    )
			    ->withInput();
	    }

	    return redirect()->route(
		    'customers.index',
		    [
			    'subdomain' => $subdomain
		    ]
	    );
    }

    /**
     * Display the specified resource.
     *
     * @param $subdomain
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($subdomain, $id)
    {
        /** @var WcCustomer $customer */
        $customer = WcCustomer::findOrFail($id);

        return view(
            'customers.show',
            [
                'customer' => $customer
            ]
        );
    }

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param $subdomain
	 * @param $id
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
    public function edit($subdomain, $id)
    {
	    $customer = WcCustomer::findOrFail($id);


	    return view(
		    'customers.edit',
		    [
			    'customer' => $customer
		    ]
	    );
    }

	/**
	 * Update the specified resource in storage.
	 *
	 * @param $subdomain
	 * @param Request $request
	 * @param $id
	 *
	 * @return $this|\Illuminate\Http\RedirectResponse
	 */
    public function update($subdomain, Request $request, $id)
    {
	    $this->validator($request->all())->validate();

	    $customer = WcCustomer::findOrFail($id);
	    $customer->first_name = $request->first_name;
	    $customer->last_name = $request->last_name;
	    $customer->email = $request->email;
	    $customer->billing = [
		    'company' => $request->billing['company'],
		    'first_name' => $request->billing['first_name'],
		    'last_name' => $request->billing['last_name'],
		    'address_1' => $request->billing['address_1'],
		    'address_2' => $request->billing['address_2'],
		    'city' => $request->billing['city'],
		    'state' => $request->billing['state'],
		    'postcode' => $request->billing['postcode'],
		    'country' => $request->billing['country'],
		    'email' => $request->billing['email'],
		    'phone' => $request->billing['phone'],
	    ];
	    $customer->shipping = [
		    'company' => $request->shipping['company'],
		    'first_name' => $request->shipping['first_name'],
		    'last_name' => $request->shipping['last_name'],
		    'address_1' => $request->shipping['address_1'],
		    'address_2' => $request->shipping['address_2'],
		    'city' => $request->shipping['city'],
		    'state' => $request->shipping['state'],
		    'postcode' => $request->shipping['postcode'],
		    'country' => $request->shipping['country'],
	    ];

	    try {
		    $customer->save();
	    } catch (HttpClientException $e) {
		    $error = json_decode($e->getResponse()->getBody());

		    return redirect()
			    ->route(
				    'customers.edit',
				    [
					    'subdomain' => $subdomain,
					    'id' => $id
				    ]
			    )
			    ->withErrors(
				    [
					    $error->message ?: '',
				    ]
			    )
			    ->withInput();
	    }

	    return redirect()
		    ->route(
			    'customers.index',
			    [
				    'subdomain' => $subdomain
			    ]
		    );
    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param $subdomain
	 * @param $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
    public function destroy($subdomain, $id)
    {
    	WcCustomer::destroy($id);

	    return redirect()->route(
		    'customers.index',
		    [
			    'subdomain' => $subdomain
		    ]
	    );
    }
}
