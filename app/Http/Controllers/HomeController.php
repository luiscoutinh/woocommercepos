<?php

namespace App\Http\Controllers;

use App\WcReportSales;
use App\WcReportTopSellers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Show the homepage for logged in users
     *
     * @return \Illuminate\Http\Response
     */
    public function index($subdomain, Request $request)
    {
        if (!session('errors') && ($test = test_wc_settings()) !== true) return $test;

        /** @var Stores $stores */
        $stores = $this->user->type === 'admin'
            ? $this->user->account->stores
            : $this->user->stores;

        /** @var Cashiers $cashiers */
        $cashiers = $this->user->account->cashiers;

        $weekReport = $topSellersReport = $thisMonthReport = $lastMonthReport = $thisYearReport = null;
        if (!session('errors')) {
            $weekReport = WcReportSales::getByWeek();
            $topSellersReport = WcReportTopSellers::getByWeek();
            $thisMonthReport = WcReportSales::getByMonth();
            $lastMonthReport = WcReportSales::getLastMonth();
            $thisYearReport = WcReportSales::getByYear();
        }

        return view(
            'home',
            [
                'stores' => $stores,
                'cashiers' => $cashiers,
                'week_report' => $weekReport,
                'week_top_sellers_report' => $topSellersReport,
                'this_month_report' => $thisMonthReport,
                'last_month_report' => $lastMonthReport,
                'this_year_report' => $thisYearReport,
            ]
        );
    }
}
