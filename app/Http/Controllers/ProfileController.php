<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator;
use Illuminate\Http\Request;
use App\Http\Requests;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Show profile form page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showProfileForm()
    {
        return view('profile');
    }

    /**
     * Get a validator for account data
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorAccount(array $data)
    {
        return Validator::make($data, [
            'company' => 'required|max:255',
            'subdomain' => 'required|alpha|max:200|unique:accounts,subdomain,' . $this->user->account->id,
            'email' => 'required|email|max:255',
            'wc_url' => 'required|url|max:255',
            'wc_consumer_key' => 'required|max:255',
            'wc_consumer_secret' => 'required|max:255',
            'wc_currency' => 'required',
            'wc_price_format' => 'required|in:left,right,left_space,right_space',
            'wc_thousand_separator' => 'required|max:1',
            'wc_decimal_separator' => 'required|max:1',
            'wc_price_decimals' => 'required|numeric|max:9',
            'wc_tax_total_display' => 'required|in:single,itemized',
            'wc_order_username' => 'required|alpha_dash',
            'wc_order_store' => 'required|alpha_dash',
            'wc_order_cashier' => 'required|alpha_dash',
            'wc_payment_methods' => 'required|alpha_dash',
        ]);
    }

    /**
     * Get a validator for user data
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorUser(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|alpha_spaces|max:120',
            'username' => 'required|alpha_num|max:100|unique:users,username,' . $this->user->id . ',id,account_id,' . $this->user->account_id,
            'password' => 'min:6|confirmed',
        ]);
    }

    /**
     * Update the account instance after validate the submitted data
     *
     * @param $subdomain
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function saveAccount($subdomain, Request $request)
    {
        $validator = $this->validatorAccount($request->all());

        if ($validator->fails()) {
            return redirect()->route('profile', ['account' => $subdomain])->withErrors($validator, 'accountErrors');
        }

        $this->user->account->company = $request->company;
        $this->user->account->subdomain = $request->subdomain;
        $this->user->account->email = $request->email;
        $this->user->account->wc_url = rtrim($request->wc_url, '/');
        $this->user->account->wc_consumer_key = $request->wc_consumer_key;
        $this->user->account->wc_consumer_secret = $request->wc_consumer_secret;
        $this->user->account->wc_currency = $request->wc_currency;
        $this->user->account->wc_price_format = $request->wc_price_format;
        $this->user->account->wc_thousand_separator = $request->wc_thousand_separator;
        $this->user->account->wc_decimal_separator = $request->wc_decimal_separator;
        $this->user->account->wc_price_decimals = $request->wc_price_decimals;
        $this->user->account->wc_tax_enabled = (bool) $request->wc_tax_enabled;
        $this->user->account->wc_tax_total_display = $request->wc_tax_total_display;
        $this->user->account->wc_order_username = $request->wc_order_username;
        $this->user->account->wc_order_store = $request->wc_order_store;
        $this->user->account->wc_order_cashier = $request->wc_order_cashier;
        $this->user->account->wc_payment_methods = $request->wc_payment_methods;

        $this->user->account->save();

        // Refresh user session data
        Auth::loginUsingId($this->user->id, Auth::viaRemember());
        $this->user = Auth::user();

        return redirect('/profile');
    }

    /**
     * Update the user instance after validate the submitted data
     *
     * @param $subdomain
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function saveUser($subdomain, Request $request)
    {
        $validator = $this->validatorUser($request->all());

        if ($validator->fails()) {
            return redirect()->route('profile', ['account' => $subdomain])->withErrors($validator, 'profileErrors');
        }

        $this->user->name = $request->name;
        $this->user->username = $request->username;

        if ($request->password) {
            $this->user->password = Hash::make($request->password);
        }

        $this->user->save();

        // Refresh user session data
        Auth::loginUsingId($this->user->id, Auth::viaRemember());
        $this->user = Auth::user();

        return redirect('/profile');
    }
}
