<?php

namespace App\Http\Middleware;

use Closure;

class AuthCashier
{
    /**
     * Send the user to home if a cashier is not logged in
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!session()->has('store') || !session()->has('cashier')) {
            return redirect('/home');
        }

        return $next($request);
    }
}
