<?php

namespace App;

class WcCategory extends WcModel
{
    /** @var string WooCommerce endpoint */
    protected static $endpoint = 'products/categories';

    /** @var array Available parameters */
    protected static $params = [
        'context' => 'view', // String
        'page' => 1, // Integer
        'per_page' => 100, // Integer
        'search' => null, // String
        'exclude' => null, // String
        'include' => null, // String
        'order' => 'asc', // String: asc || desc
        'orderby' => 'name', // String: id || include || name || slug || term_group || description || count
        'hide_empty' => true, // Bool: true || false
        'parent' => null, // Integer
        'product' => null, // Integer
        'slug' => null, // String
    ];

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
