<?php

namespace App;

class WcOrder extends WcModel
{
    /** @var string WooCommerce endpoint */
    protected static $endpoint = 'orders';
    
    /** @var array Available parameters */
    protected static $params = [
        'context' => 'view', // String
        'page' => 1, // Integer
        'per_page' => 12, // Integer
        'search' => null, // String
        'after' => null, // String
        'before' => null, // String
        'exclude' => null, // String
        'include' => null, // String
        'offset' => null, // Integer
        'order' => 'desc', // String: asc || desc
        'orderby' => 'date', // String: date || id || include || title || slug
        'filter' => null, // String
        'status' => 'any', // String: any || pending || processing || on-hold || completed || cancelled || refunded || failed
        'customer' => null, // String
        'product' => null, // Integer
        'dp' => null, // String
    ];

    /** @var array Complete order refunds info */
    protected $completeRefunds = [];

    /** @var array Complete order notes info */
    protected $completeNotes = [];

    /**
     * Get complete order refunds info
     *
     * @return array
     */
    public function getCompleteRefunds()
    {
        if ($this->completeRefunds) {
            return $this->completeRefunds;
        }

        if (!$this->getRefunds()) {
            $this->completeRefunds = [];
        } else {
            $search = static::instance()->get(static::$endpoint . '/' . $this->id . '/refunds');

            $this->completeRefunds = array_filter($search, function ($value) {
                return !isset($value['errors']);
            });
        }

        return $this->completeRefunds;
    }

    /**
     * Get complete order notes info
     *
     * @return array
     */
    public function getCompleteNotes()
    {
        if ($this->completeNotes) {
            return $this->completeNotes;
        }

        $this->completeNotes = static::instance()->get(static::$endpoint . '/' . $this->id . '/notes');

        return $this->completeNotes;
    }

    /**
     * Get item total price
     *
     * @param $item
     * @param bool $inc_tax
     * @param bool $round
     *
     * @return float|int
     */
    public function getItemTotal($item, $inc_tax = false, $round = false)
    {
        $qty = (!empty($item['quantity'])) ? $item['quantity'] : 1;

        if ($inc_tax) {
            $price = ($item['total'] + $item['total_tax']) / max(1, $qty);
        } else {
            $price = $item['total'] / max(1, $qty);
        }

        $price = $round ? round($price, 2) : $price;

        return $price;
    }

    /**
     * Get item subtotal price
     *
     * @param $item
     * @param bool $inc_tax
     * @param bool $round
     *
     * @return float|int|string
     */
    public function getItemSubtotal($item, $inc_tax = false, $round = true)
    {
        if ($inc_tax) {
            $price = ($item['subtotal'] + $item['subtotal_tax']) / max(1, $item['quantity']);
        } else {
            $price = ($item['subtotal'] / max(1, $item['quantity']));
        }

        $price = $round ? number_format((float) $price, 2, '.', '') : $price;

        return $price;
    }

	/**
	 * Get id
	 *
	 * @return int
	 */
    public function getId()
    {
    	return $this->id;
    }

	/**
	 * Get number
	 *
	 * @return int|string
	 */
    public function getNumber()
    {
    	return $this->number;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get date completed
     *
     * @return string
     */
    public function getDateCompleted()
    {
        return $this->date_completed;
    }

    /**
     * Get date paid
     *
     * @return string
     */
    public function getDatePaid()
    {
        return $this->date_paid;
    }

	/**
	 * Get currency
	 *
	 * @return string
	 */
    public function getCurrency()
    {
    	return $this->currency;
    }

    /**
     * Get billing address
     *
     * @return array
     */
    public function getBilling()
    {
        return $this->billing;
    }

    /**
     * Get shipping address
     *
     * @return array
     */
    public function getShipping()
    {
        return $this->shipping;
    }

    /**
     * Get line items
     *
     * @return array
     */
    public function getLineItems()
    {
        return $this->line_items;
    }

    /**
     * Get line item by id
     * @param $id
     * @return mixed
     */
    public function getLineItem($id)
    {
        foreach ($this->line_items as $line_item) {
            if ($line_item['id'] == $id) {
                return $line_item;
            }
        }
    }

    /**
     * Get shipping lines
     *
     * @return array
     */
    public function getShippingLines()
    {
        return $this->shipping_lines;
    }

    /**
     * Get refunds
     *
     * @return array
     */
    public function getRefunds()
    {
        return $this->refunds;
    }

    /**
     * Get coupon lines
     *
     * @return array
     */
    public function getCouponLineS()
    {
        return $this->coupon_lines;
    }

    /**
     * Get tax lines
     *
     * @return array
     */
    public function getTaxLines()
    {
        return $this->tax_lines;
    }

    /**
     * Get total discount
     *
     * @return float
     */
    public function getTotalDiscount()
    {
        return $this->discount_total;
    }

    /**
     * Gets order total.
     *
     * @return float
     */
    public function getTotal() {
        return (double) $this->total;
    }

    /**
     * Get total shipping
     *
     * @return float|int
     */
    public function getTotalShipping() {
        return $this->shipping_total;
    }

    /**
     * Get total refunded
     *
     * @return float|int
     */
    public function getTotalRefunded() {
        return array_sum(array_column($this->refunds, 'total')) * -1;
    }

    /**
     * Get tax totals
     *
     * @return array
     */
    public function getTaxTotals() {
        return $this->tax_lines;
    }

    /**
     * Get status class
     *
     * @return string
     */
    public function getStatusClass()
    {
        switch ($this->status) {
            case 'pending':
                $status_class = 'warning';
                break;
            case 'processing':
                $status_class = 'default';
                break;
            case 'on-hold':
                $status_class = 'dark';
                break;
            case 'completed':
                $status_class = 'info';
                break;
            case 'cancelled':
                $status_class = 'danger';
                break;
            case 'refunded':
                $status_class = 'dark';
                break;
            case 'failed':
                $status_class = 'dark';
                break;
        }

        return $status_class;
    }

    /**
     * Get the refunded amount for a line item.
     *
     * @param $product_id
     * @param $variation_id
     *
     * @return int
     */
    public function getQtyRefundedForItem($product_id, $variation_id) {
        $qty = 0;

        foreach ($this->getCompleteRefunds() as $refund) {
            foreach ($refund['line_items'] as $refunded_item) {
                if ($refunded_item['product_id'] == $product_id && $refunded_item['variation_id'] == $variation_id) {
                    $qty += $refunded_item['quantity'];
                }
            }
        }

        return $qty;
    }

    /**
     * Get the refunded amount for a line item.
     *
     * @param $product_id
     * @param $variation_id
     * @param string $item_type
     *
     * @return int
     */
    public function getTotalRefundedForItem( $product_id, $variation_id, $item_type = 'line_item' ) {
        $total = 0;

        foreach ($this->getCompleteRefunds() as $refund) {
            if ($item_type === 'shipping') {
                $total += !array_sum(array_column($refund['line_items'], 'total')) ? $refund['amount'] : 0;
                continue;
            }
            foreach ($refund['line_items'] as $refunded_item) {
                if ($refunded_item['product_id'] == $product_id && $refunded_item['variation_id'] == $variation_id) {
                    switch ($item_type) {
                        case 'shipping' :
                            $total += $refunded_item['amount'];
                            break;
                        default :
                            $total += $refunded_item['total'];
                            break;
                    }
                }
            }
        }

        return $total * -1;
    }

    /**
     * Get total shipping refunded
     *
     * @return float|int
     */
    public function getTotalShippingRefunded() {
        $total = 0;

        foreach ($this->getCompleteRefunds() as $refund) {
            $total += !array_sum(array_column($refund['line_items'], 'total')) ? $refund['amount'] : 0;
        }

        return $total;
    }

    /**
     * Get the refunded amount for a line item.
     *
     * @param $product_id
     * @param $variation_id
     * @param $tax_id
     * @param string $item_type
     *
     * @return int
     */
    public function getTaxRefundedForItem( $product_id, $variation_id, $tax_id, $item_type = 'line_item' ) {
        $total = 0;
        foreach ($this->getCompleteRefunds() as $refund) {
            foreach ($refund['line_items'] as $refunded_item) {
                if ($refunded_item['product_id'] == $product_id && $refunded_item['variation_id'] == $variation_id) {
                    switch ( $item_type ) {
                        case 'shipping' :
                            if ( isset( $refunded_item['taxes'][ $tax_id ] ) ) {
                                $total += $refunded_item['taxes'][ $tax_id ];
                            }
                            break;
                        default :
                            foreach ($refunded_item['taxes'] as $tax) {
                                if ($tax['id'] == $tax_id) {
                                    $total += $tax['total'];
                                }
                            }
                            break;
                    }
                }
            }
        }
        return $total * -1;
    }

    /**
     * Get total tax refunded by rate ID.
     *
     * @param  int $rate_id
     *
     * @return float
     */
    public function getTotalTaxRefundedByRateId( $rate_id ) {
        $total = 0;
        foreach ( $this->getCompleteRefunds() as $refund ) {
            foreach ($refund['line_items'] as $item) {
                foreach ($item['taxes'] as $tax) {
                    if ($tax['id'] == $rate_id) {
                        $total += abs($tax['total']); // + abs( $refunded_item['shipping_tax_amount'] );
                    }
                }
            }
        }

        return $total;
    }

    /**
     * Get formatted order total
     *
     * @param string $tax_display
     * @param bool $display_refunded
     *
     * @return float|string
     */
    public function getFormattedOrderTotal( $tax_display = '', $display_refunded = true ) {
        $formatted_total = $this->getTotal();
        $order_total    = $this->getTotal();
        $total_refunded = $this->getTotalRefunded();
        $tax_string     = '';

        // Tax for inclusive prices
        /*
        if (tax_enabled() && 'incl' == $tax_display) {
            $tax_string_array = array();

            if ('itemized' == tax_total_display()) {
                foreach ( $this->getTaxTotals() as $code => $tax ) {
                    $tax_amount         = ( $total_refunded && $display_refunded ) ? wc_price( WC_Tax::round( $tax->amount - $this->get_total_tax_refunded_by_rate_id( $tax->rate_id ) ), array( 'currency' => $this->get_order_currency() ) ) : $tax->formatted_amount;
                    $tax_string_array[] = sprintf( '%s %s', $tax_amount, $tax->label );
                }
            } else {
                $tax_amount         = ( $total_refunded && $display_refunded ) ? $this->get_total_tax() - $this->get_total_tax_refunded() : $this->get_total_tax();
                $tax_string_array[] = sprintf( '%s %s', wc_price( $tax_amount, array( 'currency' => $this->get_order_currency() ) ), WC()->countries->tax_or_vat() );
            }
            if ( ! empty( $tax_string_array ) ) {
                $tax_string = ' ' . sprintf( __( '(includes %s)', 'woocommerce' ), implode( ', ', $tax_string_array ) );
            }
        }
        */

        if ( $total_refunded && $display_refunded ) {
            $formatted_total = '<del>' . formatted_price( $formatted_total ) . '</del> <ins>' . formatted_price( $order_total - $total_refunded) . $tax_string . '</ins>';
        } else {
            $formatted_total = formatted_price($formatted_total) . $tax_string;
        }

        return $formatted_total;
    }

    /**
     * Add product to the order
     *
     * @param $product
     */
    public function addProduct($product)
    {
        $data = [
            'id' => $this->getId(),
            'line_items' => [
                [
                    'product_id' => $product->getId(),
                    'quantity' => 1,
                ],
            ]
        ];

        $this->fillWith($this->save($data));
    }

    /**
     * Update a line item
     *
     * @param $lineItem
     * @param $qty
     * @param $price
     */
    public function updateProduct($lineItem, $qty, $price)
    {
        $data = [
            'id' => $this->getId(),
            'line_items' => [
                [
                    'id' => $lineItem['id'],
                    'product_id' => $lineItem['product_id'],
                    'quantity' => $qty,
                    'total' => $price,
                ],
            ]
        ];

        $this->fillWith($this->save($data));
    }

    /**
     * Remove a line item
     *
     * @param $lineItem
     */
    public function removeProduct($lineItem)
    {
        $data = [
            'id' => $this->getId(),
            'line_items' => [
                [
                    'id' => $lineItem['id'],
                    'product_id' => null,
                ],
            ]
        ];

        $this->fillWith($this->save($data));
    }

    /**
     * Add customer to the order
     *
     * @param $customerId
     * @param $billing
     */
    public function addCustomer($customerId, $billing)
    {
        $data = [
            'id' => $this->getId(),
            'customer_id' => $customerId,
            'billing' => $billing,
        ];

        $this->fillWith($this->save($data));
    }

    /**
     * Add payment to the order
     *
     * @param $paymentMethods
     */
    public function addPayment($paymentMethods)
    {
        $data = [
            'id' => $this->getId(),
            'meta_data' => $paymentMethods,
            'set_paid' => true,
            'status' => 'completed',
        ];

        $this->fillWith($this->save($data));
    }
}
