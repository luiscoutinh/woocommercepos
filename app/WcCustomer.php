<?php

namespace App;

class WcCustomer extends WcModel
{
	/** @var string WooCommerce endpoint */
	protected static $endpoint = 'customers';

	/** @var array Available parameters */
	protected static $params = [
		'context' => 'view', // String
		'page' => 1, // Integer
		'per_page' => 12, // Integer
		'search' => null, // String
		'exclude' => null, // String
		'include' => null, // String
		'offset' => null, // Integer
		'order' => 'desc', // String: asc || desc
		'orderby' => 'name', // String: id || include || name || registered_date
		'email' => null, // String
		'role' => 'customer', // String: all || administrator || editor || author || contributor || subscriber || customer || shop_manager
	];

	/**
	 * Get id
	 *
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Get first name
	 *
	 * @return string
	 */
	public function getFirstName()
	{
		return $this->first_name;
	}

	/**
	 * Get last name
	 *
	 * @return string
	 */
	public function getLastName()
	{
		return $this->last_name;
	}

	/**
	 * Get username
	 *
	 * @return string
	 */
	public function getUsername()
	{
		return $this->username;
	}

	/**
	 * Get e-mail
	 *
	 * @return string
	 */
	public function getEmail()
	{
		return $this->email;
	}

	/**
	 * Get avatar url
	 *
	 * @return string
	 */
	public function getAvatarUrl()
	{
		return $this->avatar_url;
	}

    /**
     * Get phone
     *
     * @return string
     */
	public function getPhone()
    {
        return $this->billing['phone'];
    }

	/**
	 * Get billing address
	 *
	 * @return array
	 */
	public function getBilling()
	{
		return $this->billing;
	}

	/**
	 * Get shipping address
	 *
	 * @return array
	 */
	public function getShipping()
	{
		return $this->shipping;
	}

	/**
	 * Get last order
	 *
	 * @return array
	 */
	public function getLastOrder()
	{
		return $this->last_order;
	}

	/**
	 * Get orders count
	 *
	 * @return int
	 */
	public function getOrdersCount()
	{
		return $this->orders_count;
	}
}
