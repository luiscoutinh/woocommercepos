<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Store extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'timezone',
    ];

    /**
     * Bind the authenticated user to Store Model queries
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(function($query) {
            $query->where('account_id', auth()->user()->account->id);
        });
    }

    /**
     * Get the account that owns the store
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function account()
    {
        return $this->belongsTo('App\Account');
    }

    /**
     * Get the users that belong to the store
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\User')->withTimestamps();
    }

    /**
     * Get the cashiers that belong to the store
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function cashiers()
    {
        return $this->belongsToMany('App\Cashier')->withTimestamps();
    }
}
