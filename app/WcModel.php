<?php

namespace App;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;

abstract class WcModel
{
	/** @var string WooCommerce endpoint */
    protected static $endpoint = '';

    /** @var array Available parameters */
    protected static $params = [];

    /**
     * Get WooCommerce instance
     *
     * @return mixed
     */
    public static function instance()
    {
        $app = app();
        $config = array_merge(
            $app['config']->get('woocommerce'),
            [
                'store_url' => Auth::user()->account->wc_url,
                'consumer_key' => Auth::user()->account->wc_consumer_key,
                'consumer_secret' => Auth::user()->account->wc_consumer_secret,
            ]
        );

        $app->singleton('woocommerce.client', function() use ($config) {
            return new \Automattic\WooCommerce\Client(
                $config['store_url'],
                $config['consumer_key'],
                $config['consumer_secret'],
                [
                    'version' => 'wc/'.$config['api_version'],
                    'verify_ssl' => $config['verify_ssl'],
                    'wp_api' => $config['wp_api'],
                    'query_string_auth' => $config['query_string_auth'],
                    'timeout' => $config['timeout'],
                ]);
        });

        $app->singleton('App\Woocommerce\WoocommerceClient', function($app) {
            return new Woocommerce\WoocommerceClient($app['woocommerce.client']);
        });

        return \App::make('App\Woocommerce\WoocommerceClient');
    }

    /**
     * Filter and prepare params
     *
     * @param array $params
     * @return array
     */
    public static function filterParams($params = [])
    {
        $params = array_merge(static::$params, $params);

        if (isset($params['before']) && $params['before']) {
            $params['before'] = $params['before'] . 'T00:00:00';
        }

        if (isset($params['after']) && $params['after']) {
            $params['after'] .= 'T23:59:59';
        }

        return array_filter(
            $params,
            function ($value) {
                return $value !== null && $value !== '';
            }
        );
    }

    /**
     * Get all records based on parameters
     *
     * @param $params
     * @return array
     */
    public static function all($params = [])
    {
        $instance = static::instance();
        $params = static::filterParams($params);
        $search = $instance->get(static::$endpoint, $params);

        $results = [];
        foreach ($search as $item) {
            $obj = new static();
            foreach ($item as $key => $value) {
                $obj->$key = $value;
            }

            $results[] = $obj;
        }

        return new LengthAwarePaginator(
            $results,
            $instance->totalPages() * $params['per_page'],
            $params['per_page'],
            $instance->currentPage()
        );
    }

    /**
     * Get a single record by ID
     *
     * @param $id
     * @return static
     */
    public static function findOrFail($id)
    {
        $item = static::instance()->get(static::$endpoint . '/' . $id);

        $obj = new static();
        foreach ($item as $key => $value) {
            $obj->$key = $value;
        }

        return $obj;
    }

    /**
     * Refresh object
     *
     * @param $options
     * @return static
     */
    public function fillWith($options)
    {
        foreach ($options as $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * Get an object filled with an array options
     *
     * @param $options
     * @return static
     */
    public static function filledWith($options)
    {
        $obj = new static();
        foreach ($options as $key => $value) {
            $obj->$key = $value;
        }

        return $obj;
    }

    /**
     * Create a new object
     *
     * @param bool $force Force to create on the store
     * @return WcModel
     */
    public static function create($force = false, $params = [])
    {
        $obj = new static();

        if ($params) {
            foreach ($params as $key => $value)
            $obj->$key = $value;
        }

        return $force ? $obj::filledWith($obj->save()) : $obj;
    }

    /**
     * Create or update a model
     *
     */
    public function save($data = null)
    {
    	$data = $data ?: (array) $this;
    	$id = isset($data['id']) && (int) $data['id'] ? (int) $data['id'] : null;

    	return $id
		    ? static::instance()->put(static::$endpoint . '/' . $id, $data)
		    : static::instance()->post(static::$endpoint, $data);
    }

    /**
     * Destroy the models for the given IDs.
     *
     * @param $ids array|int $ids
     * @return int Total deleted models
     */
    public static function destroy($ids)
    {
        $ids = is_array($ids) ? $ids : func_get_args();

        $count = 0;
        foreach ($ids as $id) {
            static::instance()->delete(static::$endpoint . '/' . $id, ['force' => true]);
            $count++;
        }

        return $count;
    }
}
