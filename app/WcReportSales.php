<?php

namespace App;

class WcReportSales extends WcModel
{
    /** @var string WooCommerce endpoint */
    protected static $endpoint = 'reports/sales';

    /** @var array Available parameters */
    protected static $params = [
        'context' => 'view', // String
        'period' => 'week', // String: week, month, last_month, year
        'date_min' => null, // String: format YYYY-MM-AA
        'date_max' => null, //String: format YYYY-MM-AA
    ];

    /**
     * Get report by week
     *
     * @param null $dateMin
     * @param null $dateMax
     */
    public static function getByWeek($dateMin = null, $dateMax = null)
    {
        $params = static::filterParams([
            'period' => 'week',
            'date_min' => $dateMin,
            'date_max' => $dateMax,
        ]);

        return static::instance()->get(static::$endpoint, $params);
    }

    /**
     * Get report y month
     *
     * @param null $dateMin
     * @param null $dateMax
     */
    public static function getByMonth($dateMin = null, $dateMax = null)
    {
        $params = static::filterParams([
            'period' => 'month',
            'date_min' => $dateMin,
            'date_max' => $dateMax,
        ]);

        return static::instance()->get(static::$endpoint, $params);
    }

    /**
     * Get report about last month
     * @param null $dateMin
     * @param null $dateMax
     */
    public static function getLastMonth($dateMin = null, $dateMax = null)
    {
        $params = static::filterParams([
            'period' => 'last_month',
            'date_min' => $dateMin,
            'date_max' => $dateMax,
        ]);

        return static::instance()->get(static::$endpoint, $params);
    }

    /**
     * Get report by year
     * @param null $dateMin
     * @param null $dateMax
     */
    public static function getByYear($dateMin = null, $dateMax = null)
    {
        $params = static::filterParams([
            'period' => 'year',
            'date_min' => $dateMin,
            'date_max' => $dateMax,
        ]);

        return static::instance()->get(static::$endpoint, $params);
    }
}
