<?php

namespace App\Policies;

use App\User;
use App\Cashier;
use Illuminate\Auth\Access\HandlesAuthorization;

class CashierPolicy
{
    use HandlesAuthorization;

    /**
     * Give full access to admins
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->type === 'admin') {
            return true;
        }
    }

    /**
     * Determine if the account has not created cashiers and whether the logged in user can see an alert about that.
     *
     * @param User $user
     * @return bool
     */
    public function seeCashiersAlert(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can view the cashier.
     *
     * @param  \App\User  $user
     * @param  \App\Cashier  $cashier
     * @return mixed
     */
    public function view(User $user, Cashier $cashier = null)
    {
        return $cashier === null || $user->cashiers()->get()->contains($cashier);
    }

    /**
     * Determine whether the user can create cashiers.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->type === 'manager';
    }

    /**
     * Determine whether the user can update the cashier.
     *
     * @param  \App\User  $user
     * @param  \App\Cashier  $cashier
     * @return mixed
     */
    public function update(User $user, Cashier $cashier)
    {
        return $user->type === 'manager';
    }

    /**
     * Determine whether the user can delete the cashier.
     *
     * @param  \App\User  $user
     * @param  \App\Cashier  $cashier
     * @return mixed
     */
    public function delete(User $user, Cashier $cashier)
    {
        return false;
    }
}
