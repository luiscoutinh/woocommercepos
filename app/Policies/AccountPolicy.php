<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AccountPolicy
{
    use HandlesAuthorization;

    /**
     * Determine if the account settings are not filled and whether the logged in user can see an alert about that.
     *
     * @param User $user
     * @return bool
     */
    public function seeSettingsAlert(User $user)
    {
        return $user->type === 'admin' && (
            !$user->account->wc_url
            || !$user->account->wc_consumer_key
            || !$user->account->wc_consumer_secret
        );
    }

    /**
     * Determine if the account can be updated by the user.
     *
     * @param User $user
     * @return bool
     */
    public function update(User $user)
    {
        return $user->type === 'admin';
    }
}
