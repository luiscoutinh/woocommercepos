<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Give full access to admins
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $loggedInUser, $ability)
    {
        if ($loggedInUser->type === 'admin') {
            return true;
        }
    }

    /**
     * Determine whether the user can view the user.
     *
     * @param  \App\User  $loggedInUser
     * @param  \App\User  $user
     * @return mixed
     */
    public function view(User $loggedInUser, User $user = null)
    {
        return false;
    }

    /**
     * Determine whether the user can create users.
     *
     * @param  \App\User  $loggedInUser
     * @return mixed
     */
    public function create(User $loggedInUser)
    {
        return false;
    }

    /**
     * Determine whether the user can update the user.
     *
     * @param  \App\User  $loggedInUser
     * @param  \App\User  $user
     * @return mixed
     */
    public function update(User $loggedInUser, User $user = null)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the user.
     *
     * @param  \App\User  $loggedInUser
     * @param  \App\User  $user
     * @return mixed
     */
    public function delete(User $loggedInUser, User $user = null)
    {
        return false;
    }
}
