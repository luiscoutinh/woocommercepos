<?php

namespace App\Policies;

use App\User;
use App\Store;
use Illuminate\Auth\Access\HandlesAuthorization;

class StorePolicy
{
    use HandlesAuthorization;

    /**
     * Give full access to admins
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->type === 'admin') {
            return true;
        }
    }

    /**
     * Determine if the account has not created stores and whether the logged in user can see an alert about that.
     *
     * @param User $user
     * @return bool
     */
    public function seeStoresAlert(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can view the store.
     *
     * @param  \App\User  $user
     * @param  \App\Store  $store
     * @return mixed
     */
    public function view(User $user, Store $store = null)
    {
        return ($user->type === 'manager' && $store === null) || $user->stores->contains($store);
    }

    /**
     * Determine whether the user can create stores.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the store.
     *
     * @param  \App\User  $user
     * @param  \App\Store  $store
     * @return mixed
     */
    public function update(User $user, Store $store = null)
    {
        return $user->type === 'manager' && $user->stores->contains($store);
    }

    /**
     * Determine whether the user can delete the store.
     *
     * @param  \App\User  $user
     * @param  \App\Store  $store
     * @return mixed
     */
    public function delete(User $user, Store $store = null)
    {
        return false;
    }
}
