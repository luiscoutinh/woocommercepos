const elixir = require('laravel-elixir');

require('laravel-elixir-vue');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(mix => {
    mix.copy('node_modules/font-awesome/fonts', 'public/fonts');
    mix.copy('resources/assets/fonts', 'public/fonts');
    mix.copy('resources/assets/libs/MDB_3.4.0/font', 'public/libs/MDB_3.4.0/font');
    mix.copy('resources/assets/libs/MDB_3.4.0/img', 'public/libs/MDB_3.4.0/img');
    mix.copy('resources/assets/libs/MDB_3.4.0/js', 'public/libs/MDB_3.4.0/js');

    mix.copy('resources/assets/sass/dashicons.css', 'public/css');
    mix.sass('app.scss');

    mix.webpack('bootstrap.js');
    mix.webpack('app.js');
});
