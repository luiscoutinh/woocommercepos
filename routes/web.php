<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// Sign Up
Route::group(['domain' => 'signup.' . config('app.domain')], function () {
    Route::get('/', 'Auth\RegisterController@showRegistrationForm')->name('signup');
    Route::post('/', 'Auth\RegisterController@register');
});


// Login without account name
Route::group(['domain' => 'login.' . config('app.domain')], function () {
    Route::get('/', 'Auth\LoginController@showLoginSubdomainForm')->name('login_subdomain');
    Route::post('/', 'Auth\LoginController@loginSubdomain');
});


// Accounts
Route::group(['domain' => '{account}.' . config('app.domain')], function () {
    // Login
    Route::get('/', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('/', 'Auth\LoginController@login');

    // Logout
    Route::post('logout', 'Auth\LoginController@logout');

    // Password Reset Routes...
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

    // Routes that need authentication
    Route::group(['middleware' => 'auth'], function () {
        // Home
        Route::get('/home', 'HomeController@index')->name('home');

        // Forget store/cashier
        Route::get('/forget_logged_in_cashier_store', 'StoreController@forgetLoggedInCashierStore')->name('forget_logged_in_cashier_store');

        // Profile
        Route::get('/profile', 'ProfileController@showProfileForm')->name('profile');
        Route::post('/profile/account', 'ProfileController@saveAccount')->middleware('can:update,App\Account');
        Route::post('/profile/user', 'ProfileController@saveUser');

        // Users
        Route::resource('users', 'UserController');
        Route::post('/users/{user_id}/attachStore', 'UserController@attachStore')->name('user.attach_store');
        Route::get('/users/{user_id}/detachStore/{store_id}', 'UserController@detachStore')->name('user.detach_store');

        // Stores
        Route::resource('stores', 'StoreController');
        Route::get('/stores/{store_id}/cashier/{cashier_id}', 'StoreController@showCashierLogin')->name('stores.cashier_login');
        Route::post('/stores/{store_id}/cashier/{cashier_id}', 'StoreController@doCashierLogin')->name('stores.do_cashier_login');
        Route::post('/stores/{store_id}/attachCashier', 'StoreController@attachCashier')->name('store.attach_cashier');
        Route::get('/stores/{store_id}/detachCashier/{cashier_id}', 'StoreController@detachCashier')->name('store.detach_cashier');

        // Cashiers
        Route::resource('cashiers', 'CashierController');
        Route::post('/cashiers/{cashier_id}/attachStore', 'CashierController@attachStore')->name('cashier.attach_store');
        Route::get('/cashiers/{cashier_id}/detachStore/{store_id}', 'CashierController@detachStore')->name('cashier.detach_store');

        // Products
        Route::resource('products', 'ProductController');

        // Customer
        Route::resource('customers', 'CustomerController');

        // Orders
        Route::resource('orders', 'OrderController');

        // POS
        Route::group(['middleware' => 'auth'], function () {
            Route::get('/pos', 'PosController@showDashboard')->name('showPos');
            // Cancel order
            Route::get('/pos/cancel-order', 'PosController@cancelOrder')->name('pos.cancel_order')->middleware('auth_cashier');
            // Products
            Route::get('/pos/search-product', 'PosController@searchProduct')->name('pos.search_product')->middleware('auth_cashier');
            Route::post('/pos/add-product', 'PosController@addProduct')->name('pos.add_product')->middleware('auth_cashier');
            Route::post('/pos/update-product/{line_item_id}', 'PosController@updateProduct')->name('pos.update_product')->middleware('auth_cashier');
            Route::post('/pos/remove-product/{line_item_id}', 'PosController@removeProduct')->name('pos.remove_product')->middleware('auth_cashier');
            // Customers
            Route::get('/pos/search-customer', 'PosController@searchCustomer')->name('pos.search_customer')->middleware('auth_cashier');
            Route::post('/pos/add-customer', 'PosController@addCustomer')->name('pos.add_customer')->middleware('auth_cashier');
            // Payment
            Route::post('/pos/add-payment', 'PosController@addPayment')->name('pos.add_payment')->middleware('auth_cashier');
        });

    });
});