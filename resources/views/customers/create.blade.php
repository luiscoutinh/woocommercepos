@extends('layouts.inside')

@section('content')

    @include('helpers/back')

    <div class="row">
        <div class="col-md-12">
            <div class="card hoverable">
                <form method="post" action="{{ routeWithAccount('customers.store') }}">
                    {{ csrf_field() }}
                    <div class="card-content">
                        <div class="row">
                            <div class="col-md-12">
                                <h2 class="text-center text-capitalize">Create a new customer</h2>
                                @include('errors/ul_list_group')
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-field">
                                    <i class="material-icons prefix">person</i>
                                    <input type="text" name="first_name" class="validate{{ $errors->has('first_name') ? ' invalid' : '' }}" id="first_name" value="{{ old('first_name') }}">
                                    <label for="first_name">First Name</label>
                                </div>

                                <div class="input-field">
                                    <i class="material-icons prefix">person</i>
                                    <input type="text" name="last_name" class="validate{{ $errors->has('last_name') ? ' invalid' : '' }}" id="last_name" value="{{ old('last_name') }}">
                                    <label for="last_name">Last Name</label>
                                </div>

                                <div class="input-field">
                                    <i class="material-icons prefix">email</i>
                                    <input type="email" name="email" class="validate{{ $errors->has('email') ? ' invalid' : '' }}" id="email" value="{{ old('email') }}">
                                    <label for="email">E-mail</label>
                                </div>

                                <div class="input-field">
                                    <i class="material-icons prefix">account_circle</i>
                                    <input type="text" name="username" class="validate{{ $errors->has('username') ? ' invalid' : '' }}" id="username" value="{{ old('username') }}">
                                    <label for="username">Username</label>
                                </div>

                                <hr>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <p class="bold-700">Billing</p>

                                <div class="input-field">
                                    <i class="material-icons prefix">business</i>
                                    <input type="text" name="billing[company]" class="validate{{ $errors->has('billing.company') ? ' invalid' : '' }}" id="billing_company" value="{{ old('billing.company') }}">
                                    <label for="billing_company">Company</label>
                                </div>

                                <div class="input-field">
                                    <i class="material-icons prefix">person</i>
                                    <input type="text" name="billing[first_name]" class="validate{{ $errors->has('billing.first_name') ? ' invalid' : '' }}" id="billing_first_name" value="{{ old('billing.first_name') }}">
                                    <label for="billing_first_name">First Name</label>
                                </div>

                                <div class="input-field">
                                    <i class="material-icons prefix">person</i>
                                    <input type="text" name="billing[last_name]" class="validate{{ $errors->has('billing.last_name') ? ' invalid' : '' }}" id="billing_last_name" value="{{ old('billing.last_name') }}">
                                    <label for="billing_last_name">Last Name</label>
                                </div>

                                <div class="input-field">
                                    <i class="material-icons prefix">location_on</i>
                                    <input type="text" name="billing[address_1]" class="validate{{ $errors->has('billing.address_1') ? ' invalid' : '' }}" id="billing_address_1" value="{{ old('billing.address_1') }}">
                                    <label for="billing_address_1">Address 1</label>
                                </div>

                                <div class="input-field">
                                    <i class="material-icons prefix">location_on</i>
                                    <input type="text" name="billing[address_2]" class="validate{{ $errors->has('billing.address_2') ? ' invalid' : '' }}" id="billing_address_2" value="{{ old('billing.address_2') }}">
                                    <label for="billing_address_2">Address 2</label>
                                </div>

                                <div class="input-field">
                                    <i class="material-icons prefix">location_on</i>
                                    <input type="text" name="billing[city]" class="validate{{ $errors->has('billing.city') ? ' invalid' : '' }}" id="billing_city" value="{{ old('billing.city') }}">
                                    <label for="billing_city">City</label>
                                </div>

                                <div class="input-field">
                                    <i class="material-icons prefix">location_on</i>
                                    <input type="text" name="billing[state]" class="validate{{ $errors->has('billing.state') ? ' invalid' : '' }}" id="billing_state" value="{{ old('billing.state') }}">
                                    <label for="billing_state">State</label>
                                </div>

                                <div class="input-field">
                                    <i class="material-icons prefix">location_on</i>
                                    <input type="text" name="billing[postcode]" class="validate{{ $errors->has('billing.postcode') ? ' invalid' : '' }}" id="billing_postcode" value="{{ old('billing.postcode') }}">
                                    <label for="billing_postcode">Postcode</label>
                                </div>

                                <div class="input-field">
                                    <i class="material-icons prefix">location_on</i>
                                    <input type="text" name="billing[country]" class="validate{{ $errors->has('billing.country') ? ' invalid' : '' }}" id="billing_country" value="{{ old('billing.country') }}">
                                    <label for="billing_country">Country</label>
                                </div>

                                <div class="input-field">
                                    <i class="material-icons prefix">email</i>
                                    <input type="email" name="billing[email]" class="validate{{ $errors->has('billing.email') ? ' invalid' : '' }}" id="billing_email" value="{{ old('billing.email') }}">
                                    <label for="billing_email">E-mail</label>
                                </div>

                                <div class="input-field">
                                    <i class="material-icons prefix">phone</i>
                                    <input type="text" name="billing[phone]" class="validate{{ $errors->has('billing.phone') ? ' invalid' : '' }}" id="billing_phone" value="{{ old('billing.phone') }}">
                                    <label for="billing_phone">Phone</label>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <p class="bold-700">Shipping</p>

                                <div class="input-field">
                                    <i class="material-icons prefix">business</i>
                                    <input type="text" name="shipping[company]" class="validate{{ $errors->has('shipping.company') ? ' invalid' : '' }}" id="shipping_company" value="{{ old('shipping.company') }}">
                                    <label for="shipping_company">Company</label>
                                </div>

                                <div class="input-field">
                                    <i class="material-icons prefix">person</i>
                                    <input type="text" name="shipping[first_name]" class="validate{{ $errors->has('shipping.first_name') ? ' invalid' : '' }}" id="shipping_first_name" value="{{ old('shipping.first_name') }}">
                                    <label for="shipping_first_name">First Name</label>
                                </div>

                                <div class="input-field">
                                    <i class="material-icons prefix">person</i>
                                    <input type="text" name="shipping[last_name]" class="validate{{ $errors->has('shipping.last_name') ? ' invalid' : '' }}" id="shipping_last_name" value="{{ old('shipping.last_name') }}">
                                    <label for="shipping_last_name">Last Name</label>
                                </div>

                                <div class="input-field">
                                    <i class="material-icons prefix">location_on</i>
                                    <input type="text" name="shipping[address_1]" class="validate{{ $errors->has('shipping.address_1') ? ' invalid' : '' }}" id="shipping_address_1" value="{{ old('shipping.address_1') }}">
                                    <label for="shipping_address_1">Address 1</label>
                                </div>

                                <div class="input-field">
                                    <i class="material-icons prefix">location_on</i>
                                    <input type="text" name="shipping[address_2]" class="validate{{ $errors->has('shipping.address_2') ? ' invalid' : '' }}" id="shipping_address_2" value="{{ old('shipping.address_2') }}">
                                    <label for="shipping_address_2">Address 2</label>
                                </div>

                                <div class="input-field">
                                    <i class="material-icons prefix">location_on</i>
                                    <input type="text" name="shipping[city]" class="validate{{ $errors->has('shipping.city') ? ' invalid' : '' }}" id="shipping_city" value="{{ old('shipping.city') }}">
                                    <label for="shipping_city">City</label>
                                </div>

                                <div class="input-field">
                                    <i class="material-icons prefix">location_on</i>
                                    <input type="text" name="shipping[state]" class="validate{{ $errors->has('shipping.state') ? ' invalid' : '' }}" id="shipping_state" value="{{ old('shipping.state') }}">
                                    <label for="shipping_state">State</label>
                                </div>

                                <div class="input-field">
                                    <i class="material-icons prefix">location_on</i>
                                    <input type="text" name="shipping[postcode]" class="validate{{ $errors->has('shipping.postcode') ? ' invalid' : '' }}" id="shipping_postcode" value="{{ old('shipping.postcode') }}">
                                    <label for="shipping_postcode">Postcode</label>
                                </div>

                                <div class="input-field">
                                    <i class="material-icons prefix">location_on</i>
                                    <input type="text" name="shipping[country]" class="validate{{ $errors->has('shipping.country') ? ' invalid' : '' }}" id="shipping_country" value="{{ old('shipping.country') }}">
                                    <label for="shipping_country">Country</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-btn text-center">
                        <a href="{{ routeWithAccount('customers.index') }}" class="btn btn-default btn-md waves-effect waves-light">Cancel</a>
                        <button type="submit" class="btn btn-primary btn-md waves-effect waves-light">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection