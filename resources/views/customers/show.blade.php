@extends('layouts.inside')

@section('content')

    @include('helpers/back')

    <section class="invoice row white z-depth-1 mb-r extra-margin-1">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12 text-center">
                    <p>
                        <img src="{{ $customer->getAvatarUrl() }}" width="100" height="100" class="img-circle center-block">
                    </p>

                    <h5>{{ $customer->getFirstName() }} {{ $customer->getLastName() }}</h5>
                    <h6>({{ $customer->getUsername() }})</h6>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <p class="small bold-700">General Information</p>

                    <p class="small text-left">
                        <span class="bold-700">E-mail</span> {{ $customer->getEmail() }}
                        <br><span class="bold-700">Phone</span> {{ $customer->getBilling()['phone'] }}
                    </p>

                    <p class="small">
                        <span class="bold-700">Total orders</span> {{ $customer->getOrdersCount() }}
                    </p>

                    <p class="small">
                        <span class="bold-700">Last order</span> <a href="{{ routeWithAccount('orders.show', ['id' => $customer->getLastOrder()['id']]) }}">{{ $customer->getLastOrder()['date'] }}</a>
                    </p>
                </div>

                <div class="col-md-4">
                    <p class="small bold-700">Billing</p>
                    <p class="small">
                        <span class="bold-700">Company</span> {{ $customer->getBilling()['company'] }}
                        <br><span class="bold-700">Name</span> {{ $customer->getBilling()['first_name'] }} {{ $customer->getBilling()['last_name'] }}
                        <br><span class="bold-700">E-mail</span> {{ $customer->getBilling()['email'] }}
                        <br><span class="bold-700">Phone</span> {{ $customer->getBilling()['phone'] }}
                        <br><span class="bold-700">Address</span> {{ $customer->getBilling()['address_1'] }}
                        <br><span class="bold-700">Address</span> {{ $customer->getBilling()['address_2'] }}
                        <br><span class="bold-700">City</span> {{ $customer->getBilling()['city'] }}
                        <br><span class="bold-700">State</span> {{ $customer->getBilling()['state'] }}
                        <br><span class="bold-700">Postcode</span> {{ $customer->getBilling()['postcode'] }}
                        <br><span class="bold-700">Country</span> {{ $customer->getBilling()['country'] }}
                    </p>
                </div>

                <div class="col-md-4">
                    <p class="small bold-700">Shipping</p>
                    <p class="small">
                        <span class="bold-700">Company</span> {{ $customer->getShipping()['company'] }}
                        <br><span class="bold-700">Name</span> {{ $customer->getShipping()['first_name'] }} {{ $customer->getShipping()['last_name'] }}
                        <br><span class="bold-700">Address</span> {{ $customer->getShipping()['address_1'] }}
                        <br><span class="bold-700">Address</span> {{ $customer->getShipping()['address_2'] }}
                        <br><span class="bold-700">City</span> {{ $customer->getShipping()['city'] }}
                        <br><span class="bold-700">State</span> {{ $customer->getShipping()['state'] }}
                        <br><span class="bold-700">Postcode</span> {{ $customer->getShipping()['postcode'] }}
                        <br><span class="bold-700">Country</span> {{ $customer->getShipping()['country'] }}
                    </p>
                </div>
            </div>
        </div>
    </section>
@endsection