@extends('layouts.inside')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <p class="text-center"><a href="{{ routeWithAccount('customers.create') }}" class="btn-floating btn-large waves-effect waves-light green" title="Add a new customer"><i class="material-icons">add</i></a></p>
        </div>
    </div>

    <div class="row">
        <form class="col-md-12" action="">
            <div class="row">
                <div class="input-field col-sm-4 col-md-3">
                    <i class="material-icons prefix">search</i>
                    <input type="text" name="search" class="validate" id="search" value="{{ $search['search'] }}">
                    <label for="search">Search</label>
                </div>

                <div class="input-field col-sm-4 col-md-3">
                    <i class="material-icons prefix">email</i>
                    <input type="text" name="email" class="validate" id="email" value="{{ $search['email'] }}">
                    <label for="email">E-mail</label>
                </div>

                <div class="input-field col-sm-4 col-md-1">
                    <select name="order">
                        <option value="" disabled{{ !$search['order'] ? ' selected' : '' }}>Order</option>
                        <option value="asc"{{ $search['order'] === 'asc' ? ' selected' : '' }}>ASC</option>
                        <option value="desc"{{ $search['order'] === 'desc' ? ' selected' : '' }}>DESC</option>
                    </select>
                </div>

                <div class="input-field col-sm-4 col-md-2">
                    <select name="orderby">
                        <option value="" disabled{{ !$search['orderby'] ? ' selected' : '' }}>Order by</option>
                        <option value="date"{{ $search['orderby'] === 'date' ? ' selected' : '' }}>Date</option>
                        <option value="id"{{ $search['orderby'] === 'id' ? ' selected' : '' }}>Id</option>
                        <option value="include"{{ $search['orderby'] === 'include' ? ' selected' : '' }}>Include</option>
                        <option value="title"{{ $search['orderby'] === 'title' ? ' selected' : '' }}>Title</option>
                        <option value="slug"{{ $search['orderby'] === 'slug' ? ' selected' : '' }}>Slug</option>
                    </select>
                </div>

                <div class="input-field col-sm-{{ !$has_search ? 2 : 1 }} col-md-{{ !$has_search ? 3 : 2 }}">
                    <button type="submit" class="btn btn-block btn-primary btn-md waves-effect waves-light">Search</button>
                </div>

                <div class="input-field col-sm-1{{ $has_search ? '' : ' hidden' }}">
                    <a class="btn btn-danger btn-block waves-effect waves-light" href="{{ routeWithAccount('customers.index') }}" title="Clear search">x</a>
                </div>
            </div>
        </form>
    </div>

    {{ $customers->setPath(routeWithAccount('customers.index'))->links() }}

    <div class="row">
        @foreach($customers as $customer)
            <?php /** @var $customer \App\WcCustomer */ ?>
            <div class="col-md-3">
                <div class="card extra-margin-1">
                    <img src="{{ $customer->getAvatarUrl() }}" width="100" height="100" class="img-circle center-block">
                    <h5 class="text-center">{{ $customer->getFirstName() }} {{ $customer->getLastName() }}</h5>
                    <p class="small text-left">
                        <span class="bold-700">Username</span> {{ $customer->getUsername() }}
                        <br><span class="bold-700">E-mail</span> {{ $customer->getEmail() }}
                        <br><span class="bold-700">Phone</span> {{ $customer->getBilling()['phone'] }}
                    </p>

                    <p class="small">
                        <span class="bold-700">Total orders</span> {{ $customer->getOrdersCount() }}
                    </p>

                    <p class="small">
                        <span class="bold-700">Last order</span> <a href="{{ routeWithAccount('orders.show', ['id' => $customer->getLastOrder()['id']]) }}">{{ $customer->getLastOrder()['date'] }}</a>
                    </p>

                    <div class="card-action text-center">
                        <a href="{{ routeWithAccount('customers.edit', ['id' => $customer->getId()]) }}" class="btn-primary btn-floating btn-large waves-effect waves-light" title="Edit customer"><i class="material-icons" aria-hidden="true">edit</i></a>
                        <a href="{{ routeWithAccount('customers.show', ['id' => $customer->getId()]) }}" class="btn-primary btn-floating btn-large waves-effect waves-light" title="See {{ $customer->getFirstName() }} {{ $customer->getLastName() }}"><i class="material-icons" aria-hidden="true">remove_red_eye</i></a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    {{ $customers->setPath(routeWithAccount('customers.index'))->links() }}
@endsection