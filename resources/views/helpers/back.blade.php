<div class="row">
    <div class="col-md-12">
        <p class="text-center"><a href="{{ redirect()->back()->getTargetUrl() }}" class="btn-primary btn-floating btn-large waves-effect waves-light" title="back to users"><i class="material-icons">arrow_back</i></a></p>
    </div>
</div>