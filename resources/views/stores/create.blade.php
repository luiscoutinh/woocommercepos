@extends('layouts.inside')

@section('content')

    @include('helpers/back')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="card hoverable">
                <form method="post" action="{{ routeWithAccount('stores.store') }}">
                    {{ csrf_field() }}
                    <div class="card-content">
                        <h2 class="text-center text-capitalize">Create a new store</h2>

                        @include('errors/ul_list_group')

                        <div class="input-field">
                            <i class="material-icons prefix">store</i>
                            <input type="text" name="name" class="validate{{ $errors->has('name') ? ' invalid' : '' }}" id="name" value="{{ old('name') }}" required>
                            <label for="name">Name</label>
                        </div>

                        <div class="input-field">
                            <select name="timezone" id="timezone">
                                <option value="" disabled>Timezone</option>
                                @foreach(timezones_by_region() as $region => $timezones)
                                    @foreach($timezones as $timezone)
                                        <option value="{{ $region . '/' . $timezone }}">{{ $timezone }}</option>
                                    @endforeach
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="card-btn text-center">
                        <a href="{{ routeWithAccount('stores.index') }}" class="btn btn-default btn-md waves-effect waves-light">Cancel</a>
                        <button type="submit" class="btn btn-primary btn-md waves-effect waves-light">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection