@extends('layouts.inside')

@section('content')
    @can('create', App\Store::class)
        <div class="row">
            <div class="col-md-12">
                <p class="text-center"><a href="{{ routeWithAccount('stores.create') }}" class="btn-floating btn-large waves-effect waves-light green" title="Add a new store"><i class="material-icons">add</i></a></p>
            </div>
        </div>
    @endcan

    <div class="row">
        <div class="col-md-9">
            <div class="row">
                @foreach($stores as $store)
                    <div class="col-md-4">
                        <form method="post" action="{{ routeWithAccount('store.attach_cashier', ['store_id' => $store->id]) }}" class="droppable attach">
                            {{ csrf_field() }}
                            <div class="card-wrapper has-footer">
                                <div id="card-store-{{$store->id}}" class="card-rotating effect__click hoverable">
                                    <div class="face card-rotating__front z-depth-1">
                                        <h4>{{ $store->name }}</h4>
                                        <p>&nbsp;</p>
                                        <p class="text-center"><strong>{{ $store->timezone }}</strong></p>
                                        <?php $datetime = new DateTime('now', new DateTimeZone($store->timezone)) ?>
                                        <p class="text-center">{{ $datetime->format('d-m-Y') }}</p>
                                        <p>&nbsp;</p>
                                        <p class="text-center"><strong>{{ $datetime->format('H:i') }}</strong></p>

                                        <a class="rotate-btn btn-default btn-floating btn-large waves-effect waves-light" data-card="card-store-{{$store->id}}" title="See Details"><i class="material-icons">arrow_forward</i></a>

                                        <div class="card-footer">
                                            @can('update', $store)<a href="{{ routeWithAccount('stores.edit', ['id' => $store->id]) }}" class="btn-primary btn-floating btn-large waves-effect waves-light" title="Edit store."><i class="material-icons" aria-hidden="true">edit</i></a>@endcan
                                            @can('view', $store)<a href="{{ routeWithAccount('stores.show', ['id' => $store->id]) }}" class="btn-primary btn-floating btn-large waves-effect waves-light" title="See {{ $store->name }}"><i class="material-icons" aria-hidden="true">remove_red_eye</i></a>@endcan
                                        </div>
                                    </div>

                                    <div class="face card-rotating__back z-depth-1">
                                        <h4>{{ $store->name }}</h4>
                                        <h5>{{ $store->username }}</h5>

                                        <ul class="list-group card-content text-left to-attach">
                                            @foreach($store->cashiers as $cashier)
                                                @include('stores/cashier_store')
                                            @endforeach
                                        </ul>

                                        <a class="rotate-btn btn-default btn-floating btn-large waves-effect waves-light" data-card="card-store-{{$store->id}}" title="Back"><i class="material-icons">arrow_back</i></a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="col-md-3">
            <div class="panel panel-info">
                <div class="panel-heading"><strong>Cashiers</strong></div>
                <div class="panel-body">
                    <ul class="list-unstyled draggable">
                        @foreach($cashiers as $cashier)
                            <li>
                                <a href="#" class="btn btn-default btn-block" data-cashier_id="{{ $cashier->id }}">{{ $cashier->name }}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="panel-footer">
                    Drag a cashier to a store and drop it to attach them.
                </div>
            </div>
        </div>
    </div>
@endsection