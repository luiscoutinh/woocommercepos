@extends('layouts.inside')

@section('content')

    @include('helpers/back')

    <div class="row">
        <div class="col-md-12">
            <h4 class="text-center text-uppercase">Open <strong>{{ $store->name }}</strong> as <strong>{{ $cashier->name }}</strong></h4>
        </div>
    </div>

    <hr>

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="card hoverable">
                <form method="post" action="{{ routeWithAccount('stores.do_cashier_login', ['store_id' => $store->id, 'cashier_id' => $cashier->id]) }}" class="form-inline">
                    {{ csrf_field() }}
                    <div class="card-content">
                        @include('errors/ul_list_group')

                        <div class="input-field">
                            <i class="material-icons prefix">lock</i>
                            <input type="password" name="password" class="validate{{ $errors->has('password') ? ' invalid' : '' }}" id="password" value="" required>
                            <label for="password">Password</label>
                        </div>
                    </div>

                    <div class="card-btn text-center">
                        <button type="submit" class="btn btn-primary btn-md waves-effect waves-light">Open</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection