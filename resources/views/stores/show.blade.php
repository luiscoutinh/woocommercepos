@extends('layouts.inside')

@section('content')

    @include('helpers/back')

    <div class="row">
        <div class="col-md-12">
            <h4 class="text-center text-capitalize">{{ $store->name }}</h4>
        </div>
    </div>

    <hr>

    @if($store->cashiers->isEmpty())
        <div class="row">
            <div class="col-md-12">
                <p class="bg-warning">No cashiers associated to this store yet.</p>
            </div>
        </div>
    @else
        <div class="row">
            @foreach($store->cashiers as $cashier)
                <div class="col-md-4">
                    <div class="card text-center">
                        <h4>{{ $cashier->name }}</h4>
                        <div class="card-action">
                            <a href="{{ routeWithAccount('stores.cashier_login', ['store' => $store->id, 'cashier' => $cashier->id]) }}" class="btn-primary btn-floating btn-large waves-effect waves-light" title="Open"><i class="material-icons" aria-hidden="true">remove_red_eye</i></a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @endif
@endsection