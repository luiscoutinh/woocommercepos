<li class="list-group-item"><a href="{{ routeWithAccount('store.detach_cashier', ['store_id' => $store->id, 'cashier_id' => $cashier->id]) }}" class="detach btn-floating waves-effect waves-light red"><i class="material-icons">remove_circle</i></a> {{ $cashier->name }}</li>

