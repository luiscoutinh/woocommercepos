@extends('layouts.inside')

@section('content')

    @include('helpers/back')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="card hoverable">
                <form method="post" action="{{ routeWithAccount('users.store') }}">
                    {{ csrf_field() }}
                    <div class="card-content">
                        <h2 class="text-center text-capitalize">Create a new user</h2>

                        @include('errors/ul_list_group')

                        <div class="input-field">
                            <select name="type" id="user_type">
                                <option value="" disabled>User Type</option>
                                <option value="admin">Administrator</option>
                                <option value="manager">Manager</option>
                                <option value="user"> User</option>
                            </select>
                        </div>

                        <div class="input-field">
                            <i class="material-icons prefix">person</i>
                            <input type="text" name="name" class="validate{{ $errors->has('name') ? ' invalid' : '' }}" id="name" value="{{ old('name') }}" required>
                            <label for="name">Name</label>
                        </div>

                        <div class="input-field">
                            <i class="material-icons prefix">account_circle</i>
                            <input type="text" name="username" class="validate{{ $errors->has('username') ? ' invalid' : '' }}" id="username" value="{{ old('username') }}" required>
                            <label for="username">Username</label>
                        </div>

                        <div class="input-field">
                            <i class="material-icons prefix">lock</i>
                            <input type="password" name="password" class="validate{{ $errors->has('password') ? ' invalid' : '' }}" id="password" value="">
                            <label for="password">Password</label>
                        </div>
                    </div>

                    <div class="card-btn text-center">
                        <a href="{{ routeWithAccount('users.index') }}" class="btn btn-default btn-md waves-effect waves-light">Cancel</a>
                        <button type="submit" class="btn btn-primary btn-md waves-effect waves-light">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection