@extends('layouts.inside')

@section('content')
    @can('create', App\User::class)
        <div class="row">
            <div class="col-md-12">
                <p class="text-center"><a href="{{ routeWithAccount('users.create') }}" class="btn-floating btn-large waves-effect waves-light green" title="Add a new user"><i class="material-icons">add</i></a></p>
            </div>
        </div>
    @endcan

    <div class="row">
        @foreach($admins as $user)
            <div class="col-md-3">
                <div class="card text-center">
                    <h4>{{ $user->name }}</h4>
                    <h5>{{ $user->username }}</h5>
                    <span class="btn btn-border-danger btn-rounded">ADMINISTRATOR</span>
                    <div class="card-action">
                        @can('update', $user)<a href="{{ routeWithAccount('users.edit', ['id' => $user->id]) }}" class="btn-primary btn-floating btn-large waves-effect waves-light" title="Edit store."><i class="material-icons" aria-hidden="true">edit</i></a>@endcan
                        @can('view', $user)<a href="{{ routeWithAccount('users.show', ['id' => $user->id]) }}" class="btn-primary btn-floating btn-large waves-effect waves-light" title="See as {{ $user->name }}"><i class="material-icons" aria-hidden="true">remove_red_eye</i></a>@endcan
                    </div>
                </div>
            </div>
        @endforeach

        <div class="clearfix"></div>

        <hr />
    </div>

    <div class="row">
        <div class="col-md-9">
            <div class="row">
                @foreach($users as $user)
                    <div class="col-md-4">
                        <form method="post" action="{{ routeWithAccount('user.attach_store', ['user_id' => $user->id]) }}" class="droppable attach">
                            {{ csrf_field() }}
                            <div class="card-wrapper has-footer">
                                <div id="card-user-{{$user->id}}" class="card-rotating effect__click hoverable">
                                    <div class="face card-rotating__front z-depth-1">
                                        <h4>{{ $user->name }}</h4>
                                        <h5>{{ $user->username }}</h5>
                                        <p>&nbsp;</p>
                                        <p class="text-center">
                                            @if($user->type === 'manager')
                                                <span class="btn btn-border-warning btn-rounded">MANAGER</span>
                                            @elseif($user->type === 'user')
                                                <span class="btn btn-border-info btn-rounded">USER</span>
                                            @endif
                                        </p>
                                        <p>&nbsp;</p>
                                        <a class="rotate-btn btn-default btn-floating btn-large waves-effect waves-light" data-card="card-user-{{$user->id}}" title="See Details"><i class="material-icons">arrow_forward</i></a>

                                        <div class="card-footer">
                                            @can('update', $user)<a href="{{ routeWithAccount('users.edit', ['id' => $user->id]) }}" class="btn-primary btn-floating btn-large waves-effect waves-light" title="Edit store."><i class="material-icons" aria-hidden="true">edit</i></a>@endcan
                                            @can('view', $user)<a href="{{ routeWithAccount('users.show', ['id' => $user->id]) }}" class="btn-primary btn-floating btn-large waves-effect waves-light" title="See as {{ $user->name }}"><i class="material-icons" aria-hidden="true">remove_red_eye</i></a>@endcan
                                        </div>
                                    </div>

                                    <div class="face card-rotating__back z-depth-1">
                                        <h4>{{ $user->name }}</h4>
                                        <h5>{{ $user->username }}</h5>

                                        <ul class="list-group card-content text-left to-attach">
                                            @foreach($user->stores as $store)
                                                @include('users/store_user')
                                            @endforeach
                                        </ul>

                                        <a class="rotate-btn btn-default btn-floating btn-large waves-effect waves-light" data-card="card-user-{{$user->id}}" title="Back"><i class="material-icons">arrow_back</i></a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="col-md-3">
            <div class="panel panel-info">
                <div class="panel-heading"><strong>Stores</strong></div>
                <div class="panel-body">
                    <ul class="list-unstyled draggable">
                        @foreach($user->account->stores as $store)
                            <li>
                                <a href="#" class="btn btn-default btn-block" data-store_id="{{ $store->id }}">{{ $store->name }}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="panel-footer">
                    Drag a store to an user and drop it to attach them.
                </div>
            </div>
        </div>
    </div>
@endsection