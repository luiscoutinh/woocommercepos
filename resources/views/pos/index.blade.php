@extends('layouts.inside')

@section('content')
    <section class="pos row white z-depth-1 extra-margin-1 mb-r">
        <div class="col-md-12">
            <ul class="nav nav-tabs tabs-4">
                <li class="active"><a data-toggle="tab" href="#pos_products">Products</a></li>
                <li><a data-toggle="tab" href="#pos_customer">Customer</a></li>
                <li><a data-toggle="tab" href="#pos_payment">Payment</a></li>
                <li><a href="#pos_totals" class="modal-trigger">TOTALS</a></li>
            </ul>

            <div class="tab-content card-panel">
                <div id="pos_products" class="tab-pane fade in active">
                    @include('pos/products/index')
                </div>

                <div id="pos_customer" class="tab-pane fade">
                    @include('pos/customers/index')
                </div>

                <div id="pos_payment" class="tab-pane fade">
                    @include('pos/payment/index')
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 text-center">
                <a href="{{ routeWithAccount('pos.cancel_order') }}" class="btn btn-danger btn-md waves-effect waves-light">Cancel</a>
            </div>
        </div>

        <div id="pos_totals" class="modal bottom-sheet">
            @include('pos/totals/index')
        </div>

        <div id="pos_product_variations" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Product Variations</h4>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection