<div class="row">
    <form method="post" action="{{ routeWithAccount('pos.add_payment') }}" class="pos-add-payment col-md-12">
        {{ csrf_field() }}

        <div class="input-field col-xs-4">
            <i class="material-icons prefix">attach_money</i>
            <input type="text" name="payment_balance_due" class="validate" value="{{ $order->getTotal() }}" disabled>
            <label>Balance due</label>
        </div>

        <div class="input-field col-xs-4">
            <i class="material-icons prefix">attach_money</i>
            <input type="text" name="payment_amount_tendered" class="validate" value="0">
            <label>Amount tendered</label>
        </div>

        <div class="input-field col-xs-4">
            <i class="material-icons prefix">attach_money</i>
            <input type="text" name="payment_change" value="" disabled>
            <label>Change</label>
        </div>

        <div class="input-field col-xs-12">
            <hr>
        </div>

        <?php $paymentMethods = explode('-', Auth::user()->account->wc_payment_methods) ?>
        @foreach($paymentMethods as $method)
            <div class="input-field col-xs-12">
                <i class="material-icons prefix">attach_money</i>
                <input type="text" name="payment_methods[{{ $method }}]" class="validate" value="@if ($loop->first){{ $order->getTotal() }}@endif">
                <label>{{ $method }}</label>
            </div>
        @endforeach

        <div class="input-field text-center col-xs-12">
            <button type="submit" class="btn btn-primary btn-md waves-effect waves-light">Save</button>
        </div>
    </form>
</div>