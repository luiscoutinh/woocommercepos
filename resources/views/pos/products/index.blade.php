<div class="row">
    <form method="post" action="{{ routeWithAccount('pos.add_product') }}" class="pos-add-product col-md-12">
        <div class="row">
            <div class="input-field col-sm-6">
                <i class="material-icons prefix">search</i>
                <input type="text" name="sku" id="sku" value="" autofocus>
                <label for="search">SKU</label>
            </div>

            <div class="input-field col-sm-6">
                <select name="search" class="md-select2 search-product" data-ajax-url="{{ routeWithAccount('pos.search_product') }}"></select>
                <label for="search">Search</label>
            </div>
        </div>
    </form>
</div>

<div class="row">
    <div class="col-md-12 line-items">
        @include('orders/order/line_items', ['isPos' => true])
    </div>
</div>