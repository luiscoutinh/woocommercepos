<table class="table table-hover small">
    <thead>
    <tr>
        <th>&nbsp;</th>
        <th>Variation</th>
        <th class="text-right">Stock</th>
        <th class="text-right">Price</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($product->getVariations() as $var)
        <tr class="pos-choose-variation" data-sku="{{ $var->getSku() }}" style="cursor: pointer;">
            <td>@if ($var->getImage())<img src="{{ $var->getImage()[0]['src'] }}" width="50" height="50">@endif</td>
            <td>
                <span class="bold-700">SKU</span> {{ $var->getSku() }}<br><br>
                @foreach ($var->getAttributes() as $attr)
                    <span class="bold-700 text-capitalize">{{ $attr['name'] }}</span> {{ $attr['option'] }}<br>
                @endforeach
            </td>
            <td class="text-right">
                {!! $var->inStock() ? '<span class="green-text">Yes</span>' : '<span class="red-text">No</span>' !!} {{ $var->getStockQuantity() ?: '-' }}
            </td>
            <td class="text-right">
                {!! formatted_price($var->getPrice()) !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>