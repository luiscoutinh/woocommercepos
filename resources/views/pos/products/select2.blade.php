<div class='select2-result-product clearfix'>
    <div class='select2-result-product__image'><img src='{{ $product->getImages()[0]['src'] }}' width='100%' /></div>
    <div class='select2-result-product__meta'>
        <div class='select2-result-product__title bold-700 black-text'>{{ $product->getName() }}</div>
        <div class='select2-result-product__categories grey-text text-darken-3'>{{ implode(' ', array_column($product->getCategories(), 'name')) }}</div>
        <div class='select2-result-product__info'>
            <div class='select2-result-product__sku'><span class='grey-text text-darken-3 bold-700'>Sku</span> <span class='grey-text text-darken-2'>{{ $product->getSku() ?: '-' }}</span></div>
            <div class='select2-result-product__stock'><span class='grey-text text-darken-3 bold-700'>Stock</span> <span class='{{ $product->inStock() ? 'green-text' : 'red-text' }}'>{{ $product->inStock() ? 'Yes' : 'No' }}</span></div>
            <div class='select2-result-product__price'><span class='grey-text text-darken-3 bold-700'>Price</span> <span class='grey-text text-darken-2'>{!! $product->getPriceHtml() !!}</span></div>
        </div>
    </div>
</div>