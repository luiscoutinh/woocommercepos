<div class="modal-content">
    <h4 class="text-center">TOTALS</h4>
    <div class="container-fluid col-md-4 col-md-offset-4">
        @include('orders/order/totals')
    </div>
</div>