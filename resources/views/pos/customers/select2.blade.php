<div class='select2-result-customer clearfix'>
    <div class='select2-result-customer__image'><img src='{{ $customer->getAvatarUrl() }}' width='100%' /></div>
    <div class='select2-result-customer__meta small'>
        <div class='select2-result-customer__title bold-700 black-text'>{{ $customer->getFirstName() }} {{ $customer->getLastName() }}</div>
        <div class='select2-result-customer__info small'>
            <div><i class="material-icons small">phone</i> <span class='grey-text text-darken-2'>{{ $customer->getPhone() ?: '-' }}</span></div>
            <div><i class="material-icons small">email</i> <span class='grey-text text-darken-2'>{{ $customer->getEmail() ?: '-' }}</span></div>
        </div>
    </div>
</div>