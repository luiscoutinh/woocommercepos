<div class="row">
    <form method="post" action="" class="pos-search-customer col-md-12">
        <div class="row">
            <div class="input-field col-xs-12">
                <select name="search" class="md-select2 search-customer" data-ajax-url="{{ routeWithAccount('pos.search_customer') }}" style="width: 100%"></select>
                <label for="search">Search</label>
            </div>
        </div>
    </form>
</div>

<hr>

<div class="row">
    <form method="post" action="{{ routeWithAccount('pos.add_customer') }}" class="pos-add-customer col-md-12">
        <input type="hidden" name="id" id="customer_id" value="">
        <div class="input-field">
            <i class="material-icons prefix">business</i>
            <input type="text" name="billing[company]" class="validate" id="billing_company" value="{{ $order->getBilling()['company'] }}">
            <label for="billing_company">Company</label>
        </div>

        <div class="input-field">
            <i class="material-icons prefix">person</i>
            <input type="text" name="billing[first_name]" class="validate" id="billing_first_name" value="{{ $order->getBilling()['first_name'] }}">
            <label for="billing_first_name">First Name</label>
        </div>

        <div class="input-field">
            <i class="material-icons prefix">person</i>
            <input type="text" name="billing[last_name]" class="validate" id="billing_last_name" value="{{ $order->getBilling()['last_name'] }}">
            <label for="billing_last_name">Last Name</label>
        </div>

        <div class="input-field">
            <i class="material-icons prefix">location_on</i>
            <input type="text" name="billing[address_1]" class="validate" id="billing_address_1" value="{{ $order->getBilling()['address_1'] }}">
            <label for="billing_address_1">Address 1</label>
        </div>

        <div class="input-field">
            <i class="material-icons prefix">location_on</i>
            <input type="text" name="billing[address_2]" class="validate" id="billing_address_2" value="{{ $order->getBilling()['address_2'] }}">
            <label for="billing_address_2">Address 2</label>
        </div>

        <div class="input-field">
            <i class="material-icons prefix">location_on</i>
            <input type="text" name="billing[city]" class="validate" id="billing_city" value="{{ $order->getBilling()['city'] }}">
            <label for="billing_city">City</label>
        </div>

        <div class="input-field">
            <i class="material-icons prefix">location_on</i>
            <input type="text" name="billing[state]" class="validate" id="billing_state" value="{{ $order->getBilling()['state'] }}">
            <label for="billing_state">State</label>
        </div>

        <div class="input-field">
            <i class="material-icons prefix">location_on</i>
            <input type="text" name="billing[postcode]" class="validate{" id="billing_postcode" value="{{ $order->getBilling()['postcode'] }}">
            <label for="billing_postcode">Postcode</label>
        </div>

        <div class="input-field">
            <i class="material-icons prefix">location_on</i>
            <input type="text" name="billing[country]" class="validate" id="billing_country" value="{{ $order->getBilling()['country'] }}">
            <label for="billing_country">Country</label>
        </div>

        <div class="input-field">
            <i class="material-icons prefix">email</i>
            <input type="email" name="billing[email]" class="validate" id="billing_email" value="{{ $order->getBilling()['email'] }}">
            <label for="billing_email">E-mail</label>
        </div>

        <div class="input-field">
            <i class="material-icons prefix">phone</i>
            <input type="text" name="billing[phone]" class="validate" id="billing_phone" value="{{ $order->getBilling()['phone'] }}">
            <label for="billing_phone">Phone</label>
        </div>

        <div class="input-field text-center">
            <button type="submit" class="btn btn-primary btn-md waves-effect waves-light">Save</button>
            <button type="submit" class="btn btn-danger btn-md waves-effect waves-light remove-customer">Remove</button>
        </div>
    </form>
</div>