@extends('layouts.inside')

@section('content')
    @if($stores)
        <div class="row">
            @foreach($stores as $store)
                <div class="col-sm-6 col-md-4">
                    <div class="card-wrapper has-footer">
                        <div id="card-store-{{$store->id}}" class="card-rotating effect__click hoverable">
                            <div class="face card-rotating__front z-depth-1">
                                <h4>{{ $store->name }}</h4>
                                <h5>{{ $store->timezone }}</h5>
                                <?php $datetime = new DateTime('now', new DateTimeZone($store->timezone)) ?>
                                <p>{{ $datetime->format('d-m-Y') }}</p>
                                <p><strong>{{ $datetime->format('H:i') }}</strong></p>

                                <a class="rotate-btn btn-default btn-floating btn-large waves-effect waves-light" data-card="card-store-{{$store->id}}" title="See Details"><i class="material-icons">arrow_forward</i></a>

                                <div class="card-footer">
                                    <a href="{{ routeWithAccount('stores.show', ['id' => $store->id]) }}" class="btn-primary btn-floating btn-large waves-effect waves-light" title="Open"><i class="material-icons" aria-hidden="true">remove_red_eye</i></a>
                                </div>
                            </div>

                            <div class="face card-rotating__back z-depth-1">
                                <h4>{{ $store->name }}</h4>
                                <h5>{{ $store->username }}</h5>

                                <ul class="list-group list-inline card-content text-left">
                                    @foreach($store->cashiers as $c)
                                        <li class="list-group-item"><i class="material-icons" aria-hidden="true" title="{{ $c->name }}">person</i></li>
                                    @endforeach
                                </ul>

                                <a class="rotate-btn btn-default btn-floating btn-large waves-effect waves-light" data-card="card-store-{{$store->id}}" title="Back"><i class="material-icons">arrow_back</i></a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @endif
@endsection