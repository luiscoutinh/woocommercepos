@extends('layouts.inside')

@section('content')
    <div class="row">
        <form class="col-md-12" action="">
            <div class="row">
                <div class="input-field col-sm-4 col-md-3">
                    <i class="material-icons prefix">search</i>
                    <input type="text" name="search" class="validate" id="search" value="{{ $search['search'] }}">
                    <label for="search">Search</label>
                </div>

                <div class="input-field col-sm-4 col-md-3">
                    <i class="material-icons prefix">search</i>
                    <input type="text" name="sku" class="validate" id="sku" value="{{ $search['sku'] }}">
                    <label for="search">Sku</label>
                </div>

                <div class="input-field col-sm-4 col-md-3">
                    <select name="status">
                        <option value="" disabled{{ !$search['status'] ? ' selected' : '' }}>Status</option>
                        <option value="any"{{ $search['status'] === 'any' ? ' selected' : '' }}>Any</option>
                        <option value="draft"{{ $search['status'] === 'draft' ? ' selected' : '' }}>Draft</option>
                        <option value="pending"{{ $search['status'] === 'pending' ? ' selected' : '' }}>Pending</option>
                        <option value="private"{{ $search['status'] === 'private' ? ' selected' : '' }}>Private</option>
                        <option value="publish"{{ $search['status'] === 'publish' ? ' selected' : '' }}>Publish</option>
                    </select>
                </div>

                <div class="input-field col-sm-4 col-md-3">
                    <select name="category">
                        <option value="" disabled{{ !$search['category'] ? ' selected' : '' }}>Category</option>
                        @foreach($categories as $category)
                            <option value="{{ $category->getId() }}"{{ $search['category'] == $category->getId() ? ' selected' : '' }}>{{ $category->getName() }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="input-field col-sm-4 col-md-3">
                    <i class="material-icons prefix">date_range</i>
                    <input type="date" name="before" class="datepicker" id="before" data-value="{{ $search['before'] }}">
                    <label for="before">Before</label>
                </div>

                <div class="input-field col-sm-4 col-md-3">
                    <i class="material-icons prefix">date_range</i>
                    <input type="date" name="after" class="datepicker" id="after" data-value="{{ $search['after'] }}">
                    <label for="after">After</label>
                </div>

                <div class="input-field col-sm-4 col-md-1">
                    <select name="order">
                        <option value="" disabled{{ !$search['order'] ? ' selected' : '' }}>Order</option>
                        <option value="asc"{{ $search['order'] === 'asc' ? ' selected' : '' }}>ASC</option>
                        <option value="desc"{{ $search['order'] === 'desc' ? ' selected' : '' }}>DESC</option>
                    </select>
                </div>

                <div class="input-field col-sm-4 col-md-2">
                    <select name="orderby">
                        <option value="" disabled{{ !$search['orderby'] ? ' selected' : '' }}>Order by</option>
                        <option value="date"{{ $search['orderby'] === 'date' ? ' selected' : '' }}>Date</option>
                        <option value="id"{{ $search['orderby'] === 'id' ? ' selected' : '' }}>Id</option>
                        <option value="include"{{ $search['orderby'] === 'include' ? ' selected' : '' }}>Include</option>
                        <option value="title"{{ $search['orderby'] === 'title' ? ' selected' : '' }}>Title</option>
                        <option value="slug"{{ $search['orderby'] === 'slug' ? ' selected' : '' }}>Slug</option>
                    </select>
                </div>

                <div class="input-field col-sm-{{ !$has_search ? 4 : 3 }} col-md-{{ !$has_search ? 3 : 2 }}">
                    <button type="submit" class="btn btn-block btn-primary btn-md waves-effect waves-light">Search</button>
                </div>

                <div class="input-field col-sm-1{{ $has_search ? '' : ' hidden' }}">
                    <a class="btn btn-danger btn-block waves-effect waves-light" href="{{ routeWithAccount('products.index') }}" title="Clear search">x</a>
                </div>
            </div>
        </form>
    </div>

    {{ $products->setPath(routeWithAccount('products.index'))->links() }}

    <div class="row">
        @foreach($products as $product)
            <?php /** @var $product \App\WcProduct */ ?>
            <div class="col-md-4">
                <div class="card card-product hoverable">
                    <ul class="text-right extra-buttons">
                        <li><a class="btn-floating btn-small waves-effect waves-light blue darken-4 top-btn" data-toggle="tooltip" data-placement="left" data-original-title="Featured? {{ $product->isFeatured() ? 'Yes' : 'No' }}"><i class="material-icons">{{ $product->isFeatured() ? 'start' : 'star_border' }}</i></a></li>
                        <li><a class="btn-floating btn-small waves-effect waves-light blue darken-4 top-btn activator" data-toggle="tooltip" data-placement="left" data-original-title="+ Info"><i class="material-icons">info</i></a></li>
                    </ul>

                    <div class="card-image waves-effect waves-block waves-light view overlay hm-white-slight">
                        <h5 class="card-label">@if ($product->isOnSale() && $product->getRegularPrice())<span class="label rgba-red-strong">-{!! round($product->getSalePrice() * 100 / $product->getRegularPrice()) !!}%</span>@endif&nbsp;</h5>
                        <a href="{{ routeWithAccount('products.show', ['id' => $product->getId()]) }}"><img src="{{ $product->getImages()[0]['src'] }}">
                            <div class="mask"> </div>
                        </a>
                    </div>

                    <a class="btn blue darken-4 btn-sm waves-effect waves-light rating">
                        <i class="material-icons">{{ $product->getAverageRating() > 0 ? 'star' : 'star_border' }}</i>
                        <i class="material-icons">{{ $product->getAverageRating() > 1 ? 'star' : 'star_border' }}</i>
                        <i class="material-icons">{{ $product->getAverageRating() > 2 ? 'star' : 'star_border' }}</i>
                        <i class="material-icons">{{ $product->getAverageRating() > 3 ? 'star' : 'star_border' }}</i>
                        <i class="material-icons">{{ $product->getAverageRating() > 4 ? 'star' : 'star_border' }}</i>
                    </a>

                    <div class="card-content text-center">
                        <div class="row">
                            <a href="{{ routeWithAccount('products.show', ['id' => $product->getId()]) }}"><h5 class="product-title">{{ $product->getName() }}</h5></a>
                        </div>
                        <div class="price">
                            <p class="green-text medium-500">{!! $product->getPriceHtml() !!}</p>
                        </div>
                    </div>

                    <div class="card-btn text-center">
                        <a href="{{ routeWithAccount('products.show', ['id' => $product->getId()]) }}" class="btn btn-primary btn-sm waves-effect waves-light">Open</a>
                        <a href="{{ $product->getPermalink() }}" target="_blank" class="btn btn-default btn-sm waves-effect waves-light">Open in the website</a>
                    </div>

                    <div class="card-reveal">
                        <span class="card-title grey-text text-darken-4">SKU <small>{{ $product->getSku() ?: '-' }}</small><i class="material-icons right">close</i></span>
                        <p class="small">
                            <strong>Categories</strong> @forelse($product->getCategories() as $category){{ $category['name'] }} @empty - @endforelse
                            <br><strong>Tags</strong> @forelse($product->getTags() as $tag){{ $tag['name'] }}@empty - @endforelse
                        </p>

                        @if ($product->getVariations())
                            <table class="table table-hover small">
                                <thead>
                                <tr>
                                    <th>Variation</th>
                                    <th class="text-right">Stock</th>
                                    <th class="text-right">Price</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($product->getVariations() as $var)
                                    <tr>
                                        <td>
                                            <span class="bold-700">SKU</span> {{ $var->getSku() }}<br><br>
                                            @foreach ($var->getAttributes() as $attr)
                                                <span class="bold-700 text-capitalize">{{ $attr['name'] }}</span> {{ $attr['option'] }}<br>
                                            @endforeach
                                        </td>
                                        <td class="text-right">
                                            <span class="bold-700" title="Stock Quantity">SQ</span> {{ $var->getStockQuantity() ?: '-' }}<br>
                                            <span class="bold-700" title="In stock?">IS?</span> {!! $var->inStock() ? '<span class="green-text">Yes</span>' : '<span class="red-text">No</span>' !!}<br>
                                            <span class="bold-700" title="Is stock manageable?">SM?</span> {!! $var->isStockManageable() ? '<span class="green-text">Yes</span>' : '<span class="red-text">No</span>' !!}
                                        </td>
                                        <td class="text-right">
                                            <span class="bold-700" title="Price">P</span> {!! formatted_price($var->getPrice()) !!}<br>
                                            <span class="bold-700" title="Regular Price">RP</span> {!! formatted_price($var->getRegularPrice()) !!}<br>
                                            <span class="bold-700" title="Sale Price">SP</span> {!! formatted_price($var->getSalePrice()) !!} @if ($var->getDateOnSaleFrom() || $var->getDateOnSaleTo())({{ $var->getDateOSaleFrom() }} - {{ $var->getDateOSaleTo() }})@endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    {{ $products->setPath(routeWithAccount('products.index'))->links() }}
@endsection