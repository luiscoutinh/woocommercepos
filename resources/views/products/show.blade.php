@extends('layouts.inside')

@section('content')

    @include('helpers/back')

    <section class="product row white z-depth-1 mb-r extra-margin-1">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <h3><i class="material-icons small" title="Featured? {{ $product->isFeatured() ? 'Yes' : 'No' }}">{{ $product->isFeatured() ? 'star' : 'star_border' }}</i> {{ $product->getName() }}</h3>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <p class="small"><span class="bold-700">Sku</span> {{ $product->getSku() }}</p>
                </div>

                <div class="col-md-6">
                    <p class="small"><span class="bold-700">Permalink</span> <a href="{{ $product->getPermalink() }}" target="_blank">Open on the website</a></p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <p class="small"><span class="bold-700">Categories</span> @forelse($product->getCategories() as $category){{ $category['name'] }} @empty - @endforelse</p>
                </div>

                <div class="col-md-6">
                    <p class="small"><span class="bold-700">Tags</span> @forelse($product->getTags() as $tag){{ $tag['name'] }}@empty - @endforelse</p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div id="carousel1" class="carousel slide carousel-fade hoverable">
                        <ol class="carousel-indicators">
                            <li data-target="#carousel1" data-slide-to="0" class="active">
                            </li>
                            <li data-target="#carousel1" data-slide-to="1"></li>
                            <li data-target="#carousel1" data-slide-to="2"></li>
                        </ol>

                        <div class="carousel-inner z-depth-2" role="listbox">
                            @foreach($product->getImages() as $image)
                                <div class="item {{ $loop->first ? 'active' : '' }}">
                                    <div class="view overlay hm-blue-slight">
                                        <a><img src="{{ $image['src'] }}" class="img-responsive" alt="{{ $image['alt'] }}">
                                            <div class="mask waves-effect waves-light"></div>
                                        </a>
                                        <div class="carousel-caption hidden-xs">
                                            <div class="animated fadeInDown">
                                                <h5>{{ $image['name'] }}</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <a class="left carousel-control new-control" href="#carousel1" role="button" data-slide="prev">
                            <span class="fa fa fa-angle-left waves-effect waves-light"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control new-control" href="#carousel1" role="button" data-slide="next">
                            <span class="fa fa fa-angle-right waves-effect waves-light"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                    </div>
                </div>

                <div class="col-md-8">
                    {!! $product->getShortDescription() !!}
                    <hr>
                    {!! $product->getDescription() !!}
                </div>
            </div>

            <div class="row small">
                <div class="col-md-6 extra-margin-1">
                    <table class="table table-hover">
                        <thead class="grey lighten-3">
                            <tr>
                                <th colspan="2">Product</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th>Type</th>
                                <td>{{ $product->getType() }}</td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <td>{{ $product->getStatus() }}</td>
                            </tr>
                            <tr>
                                <th>Catalog Visibility</th>
                                <td>{{ $product->getCatalogVisibility()['description'] }}</td>
                            </tr>
                            <tr>
                                <th>Backorders</th>
                                <td>{{ $product->getBackorders()['description'] }}</td>
                            </tr>
                            <tr>
                                <th>Attributes</th>
                                <td>
                                    @foreach ($product->getAttributes() as $attr)
                                        <span class="bold-700 text-capitalize">{{ $attr['name'] }}</span> {{ implode(', ', $attr['options']) }}<br>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th>Default Attributes</th>
                                <td>
                                    @foreach ($product->getDefaultAttributes() as $attr)
                                        <span class="bold-700 text-capitalize">{{ $attr['name'] }}</span> {{ $attr['option'] }}<br>
                                    @endforeach
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <table class="table table-hover">
                        <thead class="grey lighten-3">
                            <tr>
                                <th colspan="2">Stock Management</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th>Stock Quantity</th>
                                <td>{{ $product->getStockQuantity() ?: '-' }}</td>
                            </tr>
                            <tr>
                                <th>In stock?</th>
                                <td>{!! $product->inStock() ? '<span class="green-text">Yes</span>' : '<span class="red-text">No</span>' !!}</td>
                            </tr>
                            <tr>
                                <th>Is stock manageable?</th>
                                <td>{!! $product->isStockManageable() ? '<span class="green-text">Yes</span>' : '<span class="red-text">No</span>' !!}</td>
                            </tr>
                        </tbody>
                    </table>

                    <table class="table table-hover">
                        <thead class="grey lighten-3">
                            <tr>
                                <th colspan="2">Options</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th>Is purchaseable?</th>
                                <td>{!! $product->isPurchaseable() ? '<span class="green-text">Yes</span>' : '<span class="red-text">No</span>' !!}</td>
                            </tr>
                            <tr>
                                <th>Is sold individually?</th>
                                <td>{!! $product->isSoldIndividually() ? '<span class="green-text">Yes</span>' : '<span class="red-text">No</span>' !!}</td>
                            </tr>
                            <tr>
                                <th>Is on sale?</th>
                                <td>{!! $product->isOnSale() ? '<span class="green-text">Yes</span>' : '<span class="red-text">No</span>' !!}</td>
                            </tr>
                            <tr>
                                <th>Is virtual?</th>
                                <td>{!! $product->isVirtual() ? '<span class="green-text">Yes</span>' : '<span class="red-text">No</span>' !!}</td>
                            </tr>
                            <tr>
                                <th>Is downloadable?</th>
                                <td>{!! $product->isDownloadable() ? '<span class="green-text">Yes</span>' : '<span class="red-text">No</span>' !!}</td>
                            </tr>
                            <tr>
                                <th>Are backorders allowed?</th>
                                <td>{!! $product->areBackordersAllowed() ? '<span class="green-text">Yes</span>' : '<span class="red-text">No</span>' !!}</td>
                            </tr>
                            <tr>
                                <th>Is backordered?</th>
                                <td>{!! $product->isBackordered()  ? '<span class="green-text">Yes</span>' : '<span class="red-text">No</span>' !!}</td>
                            </tr>
                            <tr>
                                <th>Is reviewable?</th>
                                <td>{!! $product->isReviewable() ? '<span class="green-text">Yes</span>' : '<span class="red-text">No</span>' !!}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="col-md-6 extra-margin-1">
                    <table class="table table-hover">
                        <thead class="grey lighten-3">
                            <tr>
                                <th>Purchase Note</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{ $product->getPurchaseNote() ?: '-' }}</td>
                            </tr>
                        </tbody>
                    </table>

                    <table class="table table-hover">
                        <thead class="grey lighten-3">
                            <tr>
                                <th colspan="2">Prices</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th>Price</th>
                                <th>{!! $product->getPriceHtml() !!}</th>
                            </tr>
                            <tr>
                                <th>Price</th>
                                <td>{!! formatted_price($product->getPrice()) !!}</td>
                            </tr>
                            <tr>
                                <th>Regular price</th>
                                <td>{!! formatted_price($product->getRegularPrice()) !!}</td>
                            </tr>
                            <tr>
                                <th>Sale Price</th>
                                <td>{!! formatted_price($product->getSalePrice()) !!} @if ($product->getDateOnSaleFrom() || $product->getDateOnSaleTo())({{ $product->getDateOSaleFrom() }} - {{ $product->getDateOSaleTo() }})@endif</td>
                            </tr>
                            <tr>
                                <th>Tax Status</th>
                                <td>{{ $product->getTaxStatus() }}</td>
                            </tr>
                            <tr>
                                <th>Tax Class</th>
                                <td>{{ $product->getTaxClass() }}</td>
                            </tr>
                        </tbody>
                    </table>

                    <table class="table table-hover">
                        <thead class="grey lighten-3">
                            <tr>
                                <th colspan="2">Weight & Dimensions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th>Weight</th>
                                <th>{{ $product->getWeight() ?: '-' }}</th>
                            </tr>
                            <tr>
                                <th>Length</th>
                                <td>{{ $product->getDimensions()['length'] ?: '-' }}</td>
                            </tr>
                            <tr>
                                <th>Width</th>
                                <td>{{ $product->getDimensions()['width'] ?: '-' }}</td>
                            </tr>
                            <tr>
                                <th>Height</th>
                                <td>{{ $product->getDimensions()['height'] ?: '-' }}</td>
                            </tr>
                        </tbody>
                    </table>

                    <table class="table table-hover">
                        <thead class="grey lighten-3">
                            <tr>
                                <th colspan="2">Statistics</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th>Total Sales</th>
                                <td>{{ $product->getTotalSales() }}</td>
                            </tr>
                            <tr>
                                <th>Average Rating</th>
                                <td>{{ $product->getAverageRating() }}</td>
                            </tr>
                            <tr>
                                <th>Rating Count</th>
                                <td>{{ $product->getRatingCount() }}</td>
                            </tr>
                            <tr>
                                <th>Date modified</th>
                                <td>{{ $product->getDateModified() }}</td>
                            </tr>
                            <tr>
                                <th>Date created</th>
                                <td>{{ $product->getDateCreated() }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            @if ($product->getVariations())
                <div class="row small">
                    <div class="col-md-12">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Variation</th>
                                    <th class="text-right">Stock</th>
                                    <th class="text-right">Price</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($product->getVariations() as $var)
                                    <tr>
                                        <td>
                                            <span class="bold-700">SKU</span> {{ $var->getSku() }}<br><br>
                                            @foreach ($var->getAttributes() as $attr)
                                                <span class="bold-700 text-capitalize">{{ $attr['name'] }}</span> {{ $attr['option'] }}<br>
                                            @endforeach
                                        </td>
                                        <td class="text-right">
                                            <span class="bold-700">Stock Quantity</span> {{ $var->getStockQuantity() ?: '-' }}<br>
                                            <span class="bold-700">In stock?</span> {!! $var->inStock() ? '<span class="green-text">Yes</span>' : '<span class="red-text">No</span>' !!}<br>
                                            <span class="bold-700">Is stock manageable?</span> {!! $var->isStockManageable() ? '<span class="green-text">Yes</span>' : '<span class="red-text">No</span>' !!}
                                        </td>
                                        <td class="text-right">
                                            <span class="bold-700">Price</span> {!! formatted_price($var->getPrice()) !!}<br>
                                            <span class="bold-700">Regular price</span> {!! formatted_price($var->getRegularPrice()) !!}<br>
                                            <span class="bold-700">Sale Price</span> {!! formatted_price($var->getSalePrice()) !!} @if ($var->getDateOnSaleFrom() || $var->getDateOnSaleTo())({{ $var->getDateOSaleFrom() }} - {{ $var->getDateOSaleTo() }})@endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @endif
        </div>
    </section>
@endsection