@extends('layouts.inside')

@section('content')
    <div class="row">
        @can('update', App\Account::class)
        <div class="col-md-6">
            <div class="card hoverable">
                <form role="form" method="POST" action="{{ url('/profile/account') }}">
                    {{ csrf_field() }}
                    <div class="card-content">
                        <h2 class="text-center text-capitalize">Account</h2>

                        @include('errors/ul_list_group', ['errors' => $errors->accountErrors])

                        <div class="input-field">
                            <i class="material-icons prefix">business</i>
                            <input type="text" name="company" class="validate{{ $errors->has('company') ? ' invalid' : '' }}" id="company" value="{{ old('company', Auth::user()->account->company) }}" required>
                            <label for="company">Company</label>
                        </div>

                        <div class="input-field">
                            <i class="material-icons prefix">link</i>
                            <input type="text" name="subdomain" class="validate{{ $errors->has('subdomain') ? ' invalid' : '' }}" id="subdomain" value="{{ old('subdomain', Auth::user()->account->subdomain) }}" required>
                            <label for="subdomain">Subdomain</label>
                        </div>

                        <div class="input-field">
                            <i class="material-icons prefix">email</i>
                            <input type="text" name="email" class="validate{{ $errors->has('email') ? ' invalid' : '' }}" id="email" value="{{ old('email', Auth::user()->account->email) }}" required>
                            <label for="email">E-mail</label>
                        </div>

                        <hr>

                        <div class="input-field">
                            <i class="material-icons prefix">link</i>
                            <input type="text" name="wc_url" class="validate{{ $errors->has('wc_url') ? ' invalid' : '' }}" id="wc_url" value="{{ old('wc_url', Auth::user()->account->wc_url) }}" required>
                            <label for="name">Store URL</label>
                        </div>

                        <div class="input-field">
                            <i class="material-icons prefix">vpn_key</i>
                            <input type="text" name="wc_consumer_key" class="validate{{ $errors->has('wc_consumer_key') ? ' invalid' : '' }}" id="wc_consumer_key" value="{{ old('wc_consumer_key', Auth::user()->account->wc_consumer_key) }}" required>
                            <label for="wc_consumer_key">Consumer Key</label>
                        </div>

                        <div class="input-field">
                            <i class="material-icons prefix">lock</i>
                            <input type="text" name="wc_consumer_secret" class="validate{{ $errors->has('wc_consumer_secret') ? ' invalid' : '' }}" id="wc_consumer_secret" value="{{ old('wc_consumer_secret', Auth::user()->account->wc_consumer_secret) }}" required>
                            <label for="wc_consumer_secret">Consumer Secret</label>
                        </div>

                        <hr>

                        <div class="input-field">
                            <select name="wc_currency">
                                <option value="" disabled{{ Auth::user()->account->wc_currency ? ' selected' : '' }}>Currency</option>
                                <option value="single"{{ Auth::user()->account->wc_tax_total_display === 'single' ? ' selected' : '' }}>Single</option>
                                @foreach (get_currencies() as $code => $name)
                                    <option value="{{ $code }}"{{ Auth::user()->account->wc_currency === $code ? ' selected' : '' }}>{{ $name }} ({{ get_currency_symbol($code) }})</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="input-field">
                            <select name="wc_price_format">
                                <option value="" disabled{{ Auth::user()->account->wc_price_format ? ' selected' : '' }}>Price format</option>
                                <option value="left"{{ Auth::user()->account->wc_price_format === 'left' ? ' selected' : '' }}>Left</option>
                                <option value="right"{{ Auth::user()->account->wc_price_format === 'right' ? ' selected' : '' }}>Right</option>
                                <option value="left_space"{{ Auth::user()->account->wc_price_format === 'left_space' ? ' selected' : '' }}>Left space</option>
                                <option value="right_space"{{ Auth::user()->account->wc_price_format === 'right_space' ? ' selected' : '' }}>Right Space</option>
                            </select>
                        </div>

                        <div class="input-field">
                            <i class="material-icons prefix">text_format</i>
                            <input type="text" name="wc_thousand_separator" class="validate{{ $errors->has('wc_thousand_separator') ? ' invalid' : '' }}" id="wc_thousand_separator" value="{{ old('wc_thousand_separator', Auth::user()->account->wc_thousand_separator) }}" required>
                            <label for="wc_thousand_separator">Thousand separator</label>
                        </div>

                        <div class="input-field">
                            <i class="material-icons prefix">text_format</i>
                            <input type="text" name="wc_decimal_separator" class="validate{{ $errors->has('wc_decimal_separator') ? ' invalid' : '' }}" id="wc_decimal_separator" value="{{ old('wc_decimal_separator', Auth::user()->account->wc_decimal_separator) }}" required>
                            <label for="wc_decimal_separator">Decimal separator</label>
                        </div>

                        <div class="input-field">
                            <i class="material-icons prefix">text_format</i>
                            <input type="text" name="wc_price_decimals" class="validate{{ $errors->has('wc_price_decimals') ? ' invalid' : '' }}" id="wc_price_decimals" value="{{ old('wc_price_decimals', Auth::user()->account->wc_price_decimals) }}" required>
                            <label for="wc_price_decimals">Price decimals</label>
                        </div>

                        <div class="input-field">
                            <i class="material-icons prefix">money_off</i>
                            <input type="checkbox" name="wc_tax_enabled" class="validate{{ $errors->has('wc_tax_enabled') ? ' invalid' : '' }}" id="wc_tax_enabled" {{ Auth::user()->account->wc_tax_total_display ? 'checked' : '' }}>
                            <label for="wc_tax_enabled">Tax enabled?</label>
                        </div>

                        <div class="input-field">
                            <select name="wc_tax_total_display">
                                <option value="" disabled{{ Auth::user()->account->wc_tax_total_display ? ' selected' : '' }}>Tax Total Display</option>
                                <option value="single"{{ Auth::user()->account->wc_tax_total_display === 'single' ? ' selected' : '' }}>Single</option>
                                <option value="itemized"{{ Auth::user()->account->wc_tax_total_display === 'itemized' ? ' selected' : '' }}>Itemized</option>
                            </select>
                        </div>

                        <hr>

                        <div class="input-field">
                            <i class="material-icons prefix">text_fields</i>
                            <input type="text" name="wc_order_username" class="validate{{ $errors->has('wc_order_username') ? ' invalid' : '' }}" id="wc_order_username" value="{{ old('wc_order_username', Auth::user()->account->wc_order_username) }}" required>
                            <label for="wc_order_username">Custom field name for order username</label>
                        </div>

                        <div class="input-field">
                            <i class="material-icons prefix">text_fields</i>
                            <input type="text" name="wc_order_store" class="validate{{ $errors->has('wc_order_store') ? ' invalid' : '' }}" id="wc_order_store" value="{{ old('wc_order_store', Auth::user()->account->wc_order_store) }}" required>
                            <label for="wc_order_store">Custom field name for order store</label>
                        </div>

                        <div class="input-field">
                            <i class="material-icons prefix">text_fields</i>
                            <input type="text" name="wc_order_cashier" class="validate{{ $errors->has('wc_order_cashier') ? ' invalid' : '' }}" id="wc_order_cashier" value="{{ old('wc_order_cashier', Auth::user()->account->wc_order_cashier) }}" required>
                            <label for="wc_order_cashier">Custom field name for order cashier</label>
                        </div>

                        <hr>

                        <div class="input-field">
                            <i class="material-icons prefix">attach_money</i>
                            <input type="text" name="wc_payment_methods" class="validate{{ $errors->has('wc_payment_methods') ? ' invalid' : '' }}" id="wc_order_cashier" value="{{ old('wc_payment_methods', Auth::user()->account->wc_payment_methods) }}" required>
                            <label for="wc_payment_methods">Payment Methods (separed by comma)</label>
                        </div>
                    </div>

                    <div class="card-btn text-center">
                        <button type="submit" class="btn btn-primary btn-md waves-effect waves-light">Save</button>
                    </div>
                </form>
            </div>
        </div>
        @endcan

        <div class="@can('update', App\Account::class)col-md-6 @else col-md-8 col-md-offset-2 @endcan">
            <div class="card hoverable">
                <form role="form" method="POST" action="{{ url('/profile/user') }}">
                    {{ csrf_field() }}
                    <div class="card-content">
                        <h2 class="text-center text-capitalize">Profile</h2>

                        @include('errors/ul_list_group', ['errors' => $errors->profileErrors])

                        <div class="input-field">
                            <i class="material-icons prefix">block</i>
                            <input type="text" name="user_type" id="user_type" value="{{ ucfirst(Auth::user()->type) }}" disabled>
                            <label for="user_type">User Type</label>
                        </div>

                        <div class="input-field">
                            <i class="material-icons prefix">person</i>
                            <input type="text" name="name" class="validate{{ $errors->has('name') ? ' invalid' : '' }}" id="name" value="{{ old('name', Auth::user()->name) }}" required>
                            <label for="name">Name</label>
                        </div>

                        <hr>

                        <div class="input-field">
                            <i class="material-icons prefix">account_circle</i>
                            <input type="text" name="username" class="validate{{ $errors->has('username') ? ' invalid' : '' }}" id="username" value="{{ old('username', Auth::user()->username) }}" required>
                            <label for="username">Username</label>
                        </div>

                        <div class="input-field">
                            <i class="material-icons prefix">lock</i>
                            <input type="password" name="password" class="validate{{ $errors->has('password') ? ' invalid' : '' }}" id="password" value="">
                            <label for="password">Password</label>
                        </div>

                        <div class="input-field">
                            <i class="material-icons prefix">lock</i>
                            <input type="password" name="password_confirmation" class="validate{{ $errors->has('password_confirmation') ? ' invalid' : '' }}" id="password_confirmation" value="">
                            <label for="password_confirmation">Confirm password</label>
                        </div>
                    </div>

                    <div class="card-btn text-center">
                        <button type="submit" class="btn btn-primary btn-md waves-effect waves-light">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection