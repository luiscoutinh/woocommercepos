@extends('layouts.outside')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="card hoverable">
                <form role="form" method="POST" action="{{ url('/') }}">
                    {{ csrf_field() }}
                    <div class="card-content">
                        <h2 class="text-center text-capitalize">{{ $account->company }}</h2>

                        @include('errors/ul_list_group')

                        <div class="input-field">
                            <i class="material-icons prefix">account_circle</i>
                            <input type="text" name="username" class="form-control validate" id="username" value="{{ old('username') }}" required>
                            <label for="username">Username</label>
                        </div>

                        <div class="input-field">
                            <i class="material-icons prefix">lock</i>
                            <input type="password" name="password" class="form-control validate" id="password" required>
                            <label for="password">Password</label>
                        </div>

                        <div class="input-field">
                            <input type="checkbox" name="remember" id="remember"> <label for="remember">Remember Me</label>
                        </div>
                    </div>

                    <div class="card-btn text-center">
                        <button type="submit" class="btn btn-primary btn-md waves-effect waves-light">Login</button>
                        <a class="btn btn-default btn-md waves-effect waves-light" href="{{ url('/password/reset') }}">or recover password</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
