@extends('layouts.outside')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="card hoverable">
                <form role="form" method="POST" action="{{ url('/') }}">
                    {{ csrf_field() }}
                    <div class="card-content">
                        <h2 class="text-center text-capitalize">Sign Up</h2>

                        @include('errors/ul_list_group')

                        <div class="input-field">
                            <i class="material-icons prefix">business</i>
                            <input type="text" name="company" class="validate{{ $errors->has('company') ? ' invalid' : '' }}" id="company" value="{{ old('company') }}" required>
                            <label for="company">Company</label>
                        </div>

                        <div class="input-field">
                            <i class="material-icons prefix">link</i>
                            <input type="text" name="subdomain" class="validate{{ $errors->has('subdomain') ? ' invalid' : '' }}" id="subdomain" value="{{ old('subdomain') }}" required>
                            <label for="subdomain">Subdomain</label>
                        </div>

                        <div class="input-field">
                            <i class="material-icons prefix">email</i>
                            <input type="text" name="email" class="validate{{ $errors->has('email') ? ' invalid' : '' }}" id="email" value="{{ old('email') }}" required>
                            <label for="email">E-mail</label>
                        </div>

                        <hr>

                        <div class="input-field">
                            <i class="material-icons prefix">person</i>
                            <input type="text" name="name" class="validate{{ $errors->has('name') ? ' invalid' : '' }}" id="name" value="{{ old('name') }}" required>
                            <label for="name">Name</label>
                        </div>

                        <div class="input-field">
                            <i class="material-icons prefix">account_circle</i>
                            <input type="text" name="username" class="validate{{ $errors->has('username') ? ' invalid' : '' }}" id="username" value="{{ old('username') }}" required>
                            <label for="username">Username</label>
                        </div>

                        <div class="input-field">
                            <i class="material-icons prefix">lock</i>
                            <input type="password" name="password" class="validate{{ $errors->has('password') ? ' invalid' : '' }}" id="password" value="" required>
                            <label for="password">Password</label>
                        </div>

                        <div class="input-field">
                            <i class="material-icons prefix">lock</i>
                            <input type="password" name="password_confirmation" class="validate{{ $errors->has('password_confirmation') ? ' invalid' : '' }}" id="password_confirmation" value="" required>
                            <label for="password_confirmation">Confirm password</label>
                        </div>
                    </div>

                    <div class="card-btn text-center">
                        <button type="submit" class="btn btn-primary btn-md waves-effect waves-light">Sign Up</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
