@extends('layouts.outside')

<!-- Main Content -->
@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="card hoverable">
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                    {{ csrf_field() }}
                    <div class="card-content">
                        <h2 class="text-center text-capitalize">Reset Password</h2>

                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        @if ($errors->has('username'))
                            <p class="red-text">{{ $errors->first('username') }}</p>
                        @endif

                        <div class="input-field">
                            <i class="material-icons prefix">account_circle</i>
                            <input type="text" name="username" class="form-control validate" id="username" value="{{ old('username') }}">
                            <label for="username">Username</label>
                        </div>
                    </div>

                    <div class="card-btn text-center">
                        <button type="submit" class="btn btn-primary btn-md waves-effect waves-light">Send Password Reset Link</button>
                        <a class="btn btn-default btn-md waves-effect waves-light" href="{{ url('/') }}">or login</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
