@extends('layouts.outside')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="card hoverable">
                <form role="form" method="POST" action="{{ url('/') }}">
                    {{ csrf_field() }}
                    <div class="card-content">
                        <h2 class="text-center text-capitalize">Login</h2>

                        @include('errors.ul_list_group')

                        <div class="input-field">
                            <i class="material-icons prefix">link</i>
                            <input type="text" name="subdomain" class="validate{{ $errors->has('subdomain') ? ' invalid' : '' }}" id="subdomain" value="{{ old('subdomain') }}" required>
                            <label for="subdomain">Subdomain</label>
                        </div>

                        <div class="card-btn text-center">
                            <button type="submit" class="btn btn-primary btn-md waves-effect waves-light">Redirect</button>
                        </div>
                </form>
            </div>
        </div>
    </div>
@endsection
