@extends('layouts.inside')

@section('content')
    @can('create', App\Cashier::class)
        <div class="row">
            <div class="col-md-12">
                <p class="text-center"><a href="{{ routeWithAccount('cashiers.create') }}" class="btn-floating btn-large waves-effect waves-light green" title="Add a new user"><i class="material-icons">add</i></a></p>
            </div>
        </div>
    @endcan

    <div class="row">
        <div class="col-md-9">
            <div class="row">
                @foreach($cashiers as $cashier)
                    <div class="col-md-4">
                        <form method="post" action="{{ routeWithAccount('cashier.attach_store', ['cashier_id' => $cashier->id]) }}" class="droppable attach">
                            {{ csrf_field() }}
                            <div class="card-wrapper has-footer">
                                <div id="card-cashier-{{$cashier->id}}" class="card-rotating effect__click hoverable">
                                    <div class="face card-rotating__front z-depth-1">
                                        <h4>{{ $cashier->name }}</h4>
                                        <p>&nbsp;</p>
                                        <p>&nbsp;</p>
                                        <a class="rotate-btn btn-default btn-floating btn-large waves-effect waves-light" data-card="card-cashier-{{$cashier->id}}" title="See Details"><i class="material-icons">arrow_forward</i></a>

                                        <div class="card-footer">
                                            @can('update', $cashier)<a href="{{ routeWithAccount('cashiers.edit', ['id' => $cashier->id]) }}" class="btn-primary btn-floating btn-large waves-effect waves-light" title="Edit store."><i class="material-icons" aria-hidden="true">edit</i></a>@endcan
                                            @can('view', $cashier)<a href="{{ routeWithAccount('cashiers.show', ['id' => $cashier->id]) }}" class="btn-primary btn-floating btn-large waves-effect waves-light" title="See as {{ $cashier->name }}"><i class="material-icons" aria-hidden="true">remove_red_eye</i></a>@endcan
                                        </div>
                                    </div>

                                    <div class="face card-rotating__back z-depth-1">
                                        <h4>{{ $cashier->name }}</h4>

                                        <ul class="list-group card-content text-left to-attach">
                                            @foreach($cashier->stores as $store)
                                                @include('cashiers/cashier_store')
                                            @endforeach
                                        </ul>

                                        <a class="rotate-btn btn-default btn-floating btn-large waves-effect waves-light" data-card="card-cashier-{{$cashier->id}}" title="Back"><i class="material-icons">arrow_back</i></a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="col-md-3">
            <div class="panel panel-info">
                <div class="panel-heading"><strong>Stores</strong></div>
                <div class="panel-body">
                    <ul class="list-unstyled draggable">
                        @foreach($stores as $store)
                            <li>
                                <a href="#" class="btn btn-default btn-block" data-store_id="{{ $store->id }}">{{ $store->name }}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="panel-footer">
                    Drag a store to a cashier and drop it to attach them.
                </div>
            </div>
        </div>
    </div>
@endsection