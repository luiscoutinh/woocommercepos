@extends('layouts.inside')

@section('content')

    @include('helpers/back')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="card hoverable">
                <form method="post" action="{{ routeWithAccount('cashiers.update', ['id' => $cashier->id]) }}">
                    {{ method_field('PUT') }}
                    {{ csrf_field() }}
                    <div class="card-content">
                        <h2 class="text-center text-capitalize">Edit</h2>

                        @include('errors/ul_list_group')

                        <div class="input-field">
                            <i class="material-icons prefix">person</i>
                            <input type="text" name="name" class="validate{{ $errors->has('name') ? ' invalid' : '' }}" id="name" value="{{ old('name', $cashier->name) }}" required>
                            <label for="name">Name</label>
                        </div>

                        <div class="input-field">
                            <i class="material-icons prefix">lock</i>
                            <input type="password" name="password" class="validate{{ $errors->has('password') ? ' invalid' : '' }}" id="password" value="">
                            <label for="password">Password</label>
                        </div>
                    </div>

                    <div class="card-btn text-center">
                        <a href="{{ routeWithAccount('cashiers.index') }}" class="btn btn-default btn-md waves-effect waves-light">Cancel</a>
                        <button type="submit" class="btn btn-primary btn-md waves-effect waves-light">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @can('delete', $cashier)
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center">
                <form method="post" action="{{ routeWithAccount('cashiers.destroy', ['id' => $cashier->id]) }}" class="form-inline">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-danger" title="Delete cashier.">delete this cashier</button>
                </form>
            </div>
        </div>
    @endcan
@endsection