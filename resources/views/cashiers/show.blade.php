@extends('layouts.inside')

@section('content')

    @include('helpers/back')

    <div class="row">
        <div class="col-md-12">
            <h4 class="text-center">{{ $cashier->name }}</h4>
        </div>
    </div>

    <hr>

    @if($stores->isEmpty())
        <div class="row">
            <div class="col-md-12">
                <p class="bg-warning">This cashier doesn't have any associated stores yet.</p>
            </div>
        </div>
    @else
        <div class="row">
            @foreach($stores as $store)
                <div class="col-md-4">
                    <div class="card text-center">
                        <h4>{{ $store->name }}</h4>
                        <div class="card-action">
                            <a href="@if(Auth::user()->can('view', $store)){{ routeWithAccount('stores.cashier_login', ['store' => $store->id, 'cashier' => $cashier->id]) }}@else#@endif" class="@if(Auth::user()->can('view', $store)) btn-primary @else btn-danger @endif btn-floating btn-large waves-effect waves-light" title="Open"><i class="material-icons" aria-hidden="true">remove_red_eye</i></a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @endif
@endsection
