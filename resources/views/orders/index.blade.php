@extends('layouts.inside')

@section('content')
    <div class="row">
        <form class="col-md-12" action="">
            <div class="row">
                <div class="input-field col-sm-4 col-md-3">
                    <i class="material-icons prefix">search</i>
                    <input type="text" name="search" class="validate" id="search" value="{{ $search['search'] }}">
                    <label for="search">Search</label>
                </div>

                <div class="input-field col-sm-4 col-md-3">
                    <i class="material-icons prefix">search</i>
                    <input type="text" name="product" class="validate" id="product" value="{{ $search['product'] }}">
                    <label for="search">Product</label>
                </div>

                <div class="input-field col-sm-4 col-md-6">
                    <select name="status">
                        <option value="" disabled{{ !$search['status'] ? ' selected' : '' }}>Status</option>
                        <option value="any"{{ $search['status'] === 'any' ? ' selected' : '' }}>Any</option>
                        <option value="pending"{{ $search['status'] === 'pending' ? ' selected' : '' }}>Pending</option>
                        <option value="processing"{{ $search['status'] === 'processing' ? ' selected' : '' }}>Processing</option>
                        <option value="on-hold"{{ $search['status'] === 'on-hold' ? ' selected' : '' }}>On-hold</option>
                        <option value="completed"{{ $search['status'] === 'completed' ? ' selected' : '' }}>Completed</option>
                        <option value="cancelled"{{ $search['status'] === 'cancelled' ? ' selected' : '' }}>Cancelled</option>
                        <option value="refunded"{{ $search['status'] === 'refunded' ? ' selected' : '' }}>Refunded</option>
                        <option value="failed"{{ $search['status'] === 'failed' ? ' selected' : '' }}>Failed</option>
                    </select>
                </div>

                <div class="input-field col-sm-4 col-md-3">
                    <i class="material-icons prefix">date_range</i>
                    <input type="date" name="before" class="datepicker" id="before" data-value="{{ $search['before'] }}">
                    <label for="before">Before</label>
                </div>

                <div class="input-field col-sm-4 col-md-3">
                    <i class="material-icons prefix">date_range</i>
                    <input type="date" name="after" class="datepicker" id="after" data-value="{{ $search['after'] }}">
                    <label for="after">After</label>
                </div>

                <div class="input-field col-sm-4 col-md-1">
                    <select name="order">
                        <option value="" disabled{{ !$search['order'] ? ' selected' : '' }}>Order</option>
                        <option value="asc"{{ $search['order'] === 'asc' ? ' selected' : '' }}>ASC</option>
                        <option value="desc"{{ $search['order'] === 'desc' ? ' selected' : '' }}>DESC</option>
                    </select>
                </div>

                <div class="input-field col-sm-4 col-md-2">
                    <select name="orderby">
                        <option value="" disabled{{ !$search['orderby'] ? ' selected' : '' }}>Order by</option>
                        <option value="date"{{ $search['orderby'] === 'date' ? ' selected' : '' }}>Date</option>
                        <option value="id"{{ $search['orderby'] === 'id' ? ' selected' : '' }}>Id</option>
                        <option value="include"{{ $search['orderby'] === 'include' ? ' selected' : '' }}>Include</option>
                        <option value="title"{{ $search['orderby'] === 'title' ? ' selected' : '' }}>Title</option>
                        <option value="slug"{{ $search['orderby'] === 'slug' ? ' selected' : '' }}>Slug</option>
                    </select>
                </div>

                <div class="input-field col-sm-{{ !$has_search ? 8 : 7 }} col-md-{{ !$has_search ? 3 : 2 }}">
                    <button type="submit" class="btn btn-block btn-primary btn-md waves-effect waves-light">Search</button>
                </div>

                <div class="input-field col-sm-1{{ $has_search ? '' : ' hidden' }}">
                    <a class="btn btn-danger btn-block waves-effect waves-light" href="{{ routeWithAccount('orders.index') }}" title="Clear search">x</a>
                </div>
            </div>
        </form>
    </div>

    {{ $orders->setPath(routeWithAccount('orders.index'))->links() }}

    <div class="row">
        @foreach($orders as $order)
            <?php
		    /** @var $order \App\WcOrder */

		    /** @var array $billing */
            $billing = $order->getBilling();

            /** @var array $shipping */
            $shipping = $order->getShipping();
            ?>
            <div class="col-md-4 small">
                <div class="card-wrapper has-footer">
                    <div id="order-{{$order->getId()}}" class="card-rotating effect__click hoverable">
                        <div class="face card-rotating__front z-depth-1">
                            <h4>{{ $order->getNumber() ?: '-' }}</h4>
                            <p><span class="btn btn-border-{{ $order->getStatusClass() }} btn-rounded text-uppercase">{{ $order->getStatus() }}</span></p>
                            <p class="bold-700">{{ count($order->getLineItems()) }} Items</p>
                            <p>{!! formatted_price($order->getTotal(), ['currency' => $order->getCurrency()]) !!}</p>
                            <p class="small">{{ $billing['first_name'] }} {{ $billing['last_name'] }}</p>
                            @if ($billing['email'] || $billing['phone'])<p class="small">({{ $billing['email'] }} {{ $billing['phone'] }})</p>@endif
                            <a class="rotate-btn btn-default btn-floating btn-large waves-effect waves-light" data-card="order-{{$order->getId()}}" title="See Details"><i class="material-icons">arrow_forward</i></a>

                            <div class="card-footer">
                                <a href="{{ routeWithAccount('orders.show', ['id' => $order->getId()]) }}" class="btn-primary btn-floating btn-large waves-effect waves-light" title="See {{ $order->getNumber() }}"><i class="material-icons" aria-hidden="true">remove_red_eye</i></a>
                            </div>
                        </div>

                        <div class="face card-rotating__back z-depth-1">
                            <h4>{{ $order->getNumber() ?: '-' }}</h4>

                            <table class="table table-hover text-left small">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Qty</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($order->getLineItems() as $item)
                                        <tr>
                                            <td>
                                                {{ $item['name'] }}

                                                @if ($item['sku'])
                                                    <br><strong>SKU</strong> {{ $item['sku'] }}
                                                @endif
                                            </td>
                                            <td>
                                                x {{ $item['quantity'] }}

                                                @if ($refunded_qty = $order->getQtyRefundedForItem($item['product_id'], $item['variation_id']))
                                                    <br><span class="small dashicons-before dashicons-undo red-text text-darken-4"> {{ $refunded_qty * -1 }}</span>
                                                @endif
                                            </td>
                                            <td>
                                                {!! formatted_price($item['total']) !!}

                                                @if ($item['subtotal'] !== $item['total'])
                                                    <br><span class="small grey-text text-lighten-1">-{!! formatted_price($item['subtotal'] - $item['total']) !!}</span>
                                                @endif

                                                @if ($refunded = $order->getTotalRefundedForItem($item['product_id'], $item['variation_id']))
                                                    <br><span class="small dashicons-before dashicons-undo red-text text-darken-4"> {!! formatted_price($refunded) !!}</span>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                            <a class="rotate-btn btn-default btn-floating btn-large waves-effect waves-light" data-card="order-{{$order->getId()}}" title="Back"><i class="material-icons">arrow_back</i></a>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    {{ $orders->setPath(routeWithAccount('orders.index'))->links() }}
@endsection