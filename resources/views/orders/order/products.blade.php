<tbody>
@foreach($order->getLineItems() as $item)
    <tr data-href="{{ routeWithAccount('pos.update_product', ['line_item_id' => $item['id']]) }}">
        <td>
            @if (isset($isPos) && $isPos)
                <input type="checkbox" name="edit" class="modify" id="line_id_{{ $item['id'] }}" value="{{ $item['id'] }}" />
                <label for="line_id_{{ $item['id'] }}"></label>
            @endif
        </td>
        <td>
            {{ $item['name'] }}
            @if ($item['sku'])
                <br><strong>SKU</strong> {{ $item['sku'] }}
            @endif

            @if ($item['meta'])
                <br>&nbsp;
                @foreach ($item['meta'] as $meta)
                    <br><span class="small bold-700">{{ $meta['label'] }}:</span> <span class="small">{{ $meta['value'] }}</span>
                @endforeach
            @endif
        </td>
        <td>
            <div class="view text-right">
                {!! formatted_price($order->getItemTotal($item, false, true)) !!}

                @if (isset($item['subtotal']) && $item['subtotal'] != $item['total'])
                    <br><span class="small grey-text text-lighten-1">-{!! formatted_price($order->getItemSubtotal($item, false, false) - $order->getItemTotal($item, false, false)) !!}</span>
                @endif
            </div>

            <div class="edit">

            </div>

            <div class="refund">

            </div>
        </td>
        <td>
            <div class="view text-right">
                <span class="small">&times</span> {{ isset($item['quantity']) ? $item['quantity'] : '1' }}

                @if ($refunded_qty = $order->getQtyRefundedForItem($item['product_id'], $item['variation_id']))
                    <br><span class="small dashicons-before dashicons-undo red-text text-darken-4"> {{ $refunded_qty * -1 }}</span>
                @endif
            </div>

            <div class="edit">
                <div class="input-field col-xs-12 no-right-pad">
                    <input type="text" name="qty[{{ $item['id'] }}]" id="line_qty_{{ $item['id'] }}" value="{{ $item['quantity'] }}">
                    <label for="line_qty_{{ $item['id'] }}">Qty</label>
                </div>
            </div>

            <div class="refund">
            </div>
        </td>
        <td>
            <div class="view text-right">
                {!! formatted_price($item['total']) !!}

                @if ($item['subtotal'] !== $item['total'])
                    <br><span class="small grey-text text-lighten-1">-{!! formatted_price($item['subtotal'] - $item['total']) !!}</span>
                @endif

                @if ($refunded = $order->getTotalRefundedForItem($item['product_id'], $item['variation_id']))
                    <br><span class="small dashicons-before dashicons-undo red-text text-darken-4"> {!! formatted_price($refunded) !!}</span>
                @endif
            </div>

            <div class="edit">
                <div class="input-field col-xs-12 no-right-pad">
                    <input type="text" name="price[{{ $item['id'] }}]" id="line_price_{{ $item['id'] }}" value="{{ $item['total'] / $item['quantity'] }}">
                    <label for="line_price_{{ $item['id'] }}">Value</label>
                </div>
            </div>

            <div class="refund">

            </div>
        </td>
        @foreach ($order->getTaxLines() as $tax_item)
            @foreach ($item['taxes'] as $tax)
                @if ($tax_item['rate_id'] == $tax['id'])
                    <td title="{{ $tax_item['rate_code'] }}">
                        <div class="view text-right">
                            {!! $tax['total'] ? formatted_price($tax['total']) : '&ndash;' !!}
                            @if ($tax['subtotal'] != $tax['total'])
                                <br><span class="small grey-text text-lighten-1">- {!! formatted_price($tax['subtotal'] - $tax['total']) !!}</span>
                            @endif

                            @if ( $refunded = $order->getTaxRefundedForItem( $item['product_id'], $item['variation_id'], $tax['id'] ) )
                                <br><span class="small dashicons-before dashicons-undo red-text text-darken-4"> {!! formatted_price($refunded) !!}</span>
                            @endif
                        </div>

                        <div class="edit">

                        </div>

                        <div class="refund">

                        </div>
                    </td>
                @endif
            @endforeach
        @endforeach
        @if (isset($isPos) && $isPos)
            <td>
                <a href="{{ routeWithAccount('pos.remove_product', ['line_item_id' => $item['id']]) }}" class="btn-floating waves-effect waves-light red pos-remove-line-item" title="Remove this line"><i class="material-icons">remove_circle</i></a>
            </td>
        @endif
    </tr>
@endforeach
</tbody>