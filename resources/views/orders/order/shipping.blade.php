<tbody id="order_shipping_line_items">
@foreach ($order->getShippingLines() as $shipping_item)
    <tr>
        <td><i class="material-icons grey-text text-lighten-1">local_shipping</i></td>
        <td>{{ $shipping_item['method_title'] ?: 'Shipping'}} {{ $shipping_item['id'] }}</td>
        <td></td>
        <td></td>
        <td class="text-right">
            {!! formatted_price($shipping_item['total']) !!}


            @if ($refunded = $order->getTotalRefundedForItem(null, null, 'shipping'))
                <br><span class="small dashicons-before dashicons-undo red-text text-darken-4"> {!! formatted_price($refunded) !!}</span>
            @endif
        </td>
        @foreach ($order->getTaxLines() as $tax_item)
            @foreach ($shipping_item['taxes'] as $tax)
                @if ($tax_item['rate_id'] == $tax['id'])
                    <td class="text-right" title="{{ $tax_item['rate_code'] }}">
                        {!! $tax['total'] ? formatted_price($tax['total']) : '&ndash;' !!}
                    </td>
                @endif
            @endforeach
        @endforeach
    </tr>
@endforeach
</tbody>