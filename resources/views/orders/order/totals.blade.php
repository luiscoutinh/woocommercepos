<ul class="collection with-header">
    <li class="collection-header">
        <h6>Totals</h6>
    </li>
    <li class="collection-item bold-700" title="This is the total discount. Discounts are defined per line item.">
        <div>Discount<span class="secondary-content grey-text text-darken-1">{!! formatted_price($order->getTotalDiscount()) !!}</span></div>
    </li>
    <li class="collection-item bold-700" title="This is the shipping and handling total costs for the order.">
        <div>
            Shipping
            <span class="secondary-content grey-text text-darken-1">
                @if ($refunded = $order->getTotalShippingRefunded())
                    <del>{!! formatted_price($order->getTotalShipping()) !!}</del> <ins> {!! formatted_price($order->getTotalShipping() - $refunded) !!}</ins>
                @else
                    {!! formatted_price($order->getTotalShipping()) !!}
                @endif
            </span>
        </div>
    </li>
    @if (tax_enabled())
        @foreach ($order->getTaxTotals() as $tax_item)
            @if ($tax_item['tax_total'] !== '0.00' || $tax_item['shipping_tax_total'] !== '0.00')
                <li class="collection-item bold-700">
                    <div>
                        {{ $tax_item['label'] }}
                        <span class="secondary-content grey-text text-darken-1">
                            @if ($refunded = $order->getTotalTaxRefundedByRateId($tax_item['rate_id']))
                                <del>{!! formatted_price($tax_item['tax_total'] + $tax_item['shipping_tax_total']) !!}</del> <ins>{!! formatted_price($tax_item['tax_total'] + $tax_item['shipping_tax_total'] - $refunded) !!}</ins>
                            @else
                                {!! formatted_price($tax_item['tax_total'] + $tax_item['shipping_tax_total']) !!}
                            @endif
                        </span>
                    </div>
                </li>
            @endif
        @endforeach
    @endif
    <li class="collection-item bold-700">
        <div class="total-to-pay" data-value="{{ $order->getTotal() }}">Total<span class="secondary-content grey-text text-darken-1">{!! $order->getFormattedOrderTotal() !!}</span></div>
    </li>
    @if ($refunded = $order->getTotalRefunded())
        <li class="collection-item bold-700">
            <div>Refunded<span class="secondary-content dashicons-before dashicons-undo red-text text-darken-4"> {!! formatted_price($refunded) !!}</span></div>
        </li>
    @endif
</ul>