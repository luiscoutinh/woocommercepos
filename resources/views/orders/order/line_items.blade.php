<table id="line_items" class="table table-hover small">
    <thead>
        <tr class="grey lighten-4">
            <th width="1%"></th>
            <th class="normal-400 grey-text text-darken-1">Item</th>
            <th width="10%" class="normal-400 grey-text text-darken-1 text-right">Unit Price</th>
            <th width="10%" class="normal-400 grey-text text-darken-1 text-right">Qty</th>
            <th width="15%" class="normal-400 grey-text text-darken-1 text-right">Total</th>
            @foreach ($order->getTaxLines() as $tax_item)
                <th width="10%" class="normal-400 grey-text text-darken-1 text-right" title="{{ $tax_item['rate_code'] }}">
                    {{ $tax_item['label'] ?: 'Tax' }}
                </th>
            @endforeach
            @if (isset($isPos) && $isPos)
                <td width="1%">&nbsp;</td>
            @endif
        </tr>
    </thead>

    @if ($order->getLineItems())
        @include('orders/order/products')
    @endif

    @if ($order->getShippingLines())
        @include('orders/order/shipping')
    @endif

    @if ($order->getRefunds())
        @include('orders/order/refunds')
    @endif
</table>