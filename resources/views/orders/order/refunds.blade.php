<tbody id="order_refunds">
@if ($order->getCompleteRefunds())
    @foreach ($order->getCompleteRefunds() as $refund )
        <tr>
            <td><i class="material-icons grey-text text-lighten-1">repeat</i></td>
            <td>
                Refund #{{ $refund['id'] }} - {{ $refund['date_created'] }}
                @if ($refund['reason'])
                    <br><span class="normal-400-italic">{{ $refund['reason'] }}</span>
                @endif
            </td>
            <td></td>
            <td></td>
            <td class="dashicons-before dashicons-undo red-text text-darken-4 text-right"> -{!! formatted_price($refund['amount']) !!}</td>
            @for ($i = 0; $i < count($order->getTaxLines()); $i++)
                <td></td>
            @endfor
        </tr>
    @endforeach
@endif
</tbody>