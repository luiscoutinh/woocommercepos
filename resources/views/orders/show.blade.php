<?php
/** @var $order \App\WcOrder */

/** @var array */
$billing = $order->getBilling();

/** @var array */
$shipping = $order->getShipping();
?>
@extends('layouts.inside')

@section('content')

    @include('helpers/back')

    <section class="invoice row white z-depth-1 mb-r extra-margin-1">
        <div class="col-md-12">
            <div class="row">
                <p class="text-center"><span class="btn btn-border-{{ $order->getStatusClass() }} btn-rounded text-uppercase">{{ $order->getStatus() }}</span></p>
            </div>
        </div>
        <div class="col-md-12">
            <div class="row">
                <div class="col-sm-6">
                    <p class=" bold-700">Billing</p>
                    <p class="small">
                        @if ($billing['company'])<span class="bold-700">{{ $billing['company'] }}</span><br>@endif
                        @if ($billing['first_name'] || $billing['last_name']){{ $billing['first_name'] }} {{ $billing['last_name'] }}<br>@endif
                        @if ($billing['address_1']){{ $billing['address_1'] }}<br>@endif
                        @if ($billing['address_2']){{ $billing['address_2'] }}<br>@endif
                        @if ($billing['state']){{ $billing['state'] }}<br>@endif
                        @if ($billing['postcode'] || $billing['city']){{ $billing['postcode'] }} {{ $billing['city'] }}<br>@endif
                        <span class="bold-700">Date completed:</span> {{ $order->getDateCompleted() ?: '&ndash;' }}<br>
                        <span class="bold-700">Date paid:</span> {{ $order->getDatePaid() ?: '&ndash;' }}<br>
                    </p>
                </div>

                <div class="col-sm-6 text-right">
                    <p class="bold-700">Shipping</p>
                    <p class="small">
                        @if ($shipping['company'])<span class="bold-700">{{ $shipping['company'] }}</span><br>@endif
                        @if ($shipping['first_name'] || $shipping['last_name']){{ $shipping['first_name'] }} {{ $shipping['last_name'] }}<br>@endif
                        @if ($shipping['address_1']){{ $shipping['address_1'] }}<br>@endif
                        @if ($shipping['address_2']){{ $shipping['address_2'] }}<br>@endif
                        @if ($shipping['state']){{ $shipping['state'] }}<br>@endif
                        @if ($shipping['postcode'] || $shipping['city']){{ $shipping['postcode'] }} {{ $shipping['city'] }}<br>@endif
                    </p>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    @include('orders/order/line_items')
                </div>
            </div>

            <div class="row small">
                <div class="col-sm-6">
                    @if ($order->getCouponLineS())
                    <div class="wc-used-coupons">
                        <ul class="wc_coupon_list">
                            <li><span class="bold-700">Coupon(s) Used</span></li>
                            @foreach ($order->getCouponLines() as $item)
                                <li><span class="bold-700">{{ $item['code'] }}</span> {{ $item['discount'] }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                </div>

                <div class="col-sm-6">
                    @include('orders/order/totals')
                </div>
            </div>
        </div>
    </section>

    @if ($order->getCompleteNotes())
        <div class="row white z-depth-1 mb-r extra-margin-1">
            <div class="col-md-12">
                @foreach ($order->getCompleteNotes() as $note)
                    <p class="small extra-margin-1 {{ !$note['customer_note'] ? 'red' : 'blue' }} lighten-4"><span class="bold-700">{{ !$note['customer_note'] ? 'Private' : 'Public' }}</span> @ {{ $note['date_created'] }}<br>{{ $note['note'] }}</p>
                @endforeach
            </div>
        </div>
    @endif
@endsection