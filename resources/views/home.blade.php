@extends('layouts.inside')

@section('content')
    @can('seeSettingsAlert', App\Account::class)
        <div class="row">
            <div class="col-md-12">
                <p class="bg-warning">Please, correctly set the fields <strong>Store URL</strong>, <strong>Consumer Key</strong> and <strong>Consumer Secret</strong> on your <strong><a href="{{ url('/profile') }}">profile</a></strong>.</p>
            </div>
        </div>
    @endcan

    @can('seeStoresAlert', App\Store::class)
        @if(!$stores)
            <div class="row">
                <div class="col-md-12">
                    <p class="text-info">Sorry, but <mark>you have not created any store</mark>. Please, <strong><a href="{{ url('/stores') }}">create at least one store</a></strong>.</p>
                </div>
            </div>
        @endif
    @endcan

    @can('seeCashiersAlert', App\Cashier::class)
        @if(!$cashiers)
            <div class="row">
                <div class="col-sm-12">
                    <p class="text-info">Sorry, but <mark>you have not created any cashier</mark>. Please, <strong><a href="{{ url('/cashiers') }}">create at least one cashier</a></strong>.</p>
                </div>
            </div>
        @endif
    @endcan

    @include('errors/ul_list_group')

    @if ($errors->isEmpty())
        <div class="row white z-depth-1 extra-margin-2 mb-r">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <h5>Last 7 days</h5>
                        <div id="chart-last-7-days-legend" class="chart-legend"></div>
                        <canvas id="chart-last-7-days" data-chart="{{ json_encode($week_report[0], JSON_FORCE_OBJECT) }}" style="width: 100%"></canvas>
                    </div>

                    <div class="col-md-6">
                        <h5>Top sellers<small>last 7 days</small></h5>
                        <div id="chart-top-sellers-legend" class="chart-legend"></div>
                        <canvas id="chart-top-sellers" data-chart="{{ json_encode($week_top_sellers_report, JSON_FORCE_OBJECT) }}" style="width: 100%"></canvas>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <h5>This month</h5>
                        <div id="chart-this-month-legend" class="chart-legend"></div>
                        <canvas id="chart-this-month" data-chart="{{ json_encode($this_month_report[0], JSON_FORCE_OBJECT) }}" style="width: 100%"></canvas>
                    </div>

                    <div class="col-md-6">
                        <h5>Last month</h5>
                        <div id="chart-last-month-legend" class="chart-legend"></div>
                        <canvas id="chart-last-month" data-chart="{{ json_encode($last_month_report[0], JSON_FORCE_OBJECT) }}" style="width: 100%"></canvas>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <h5>This year</h5>
                        <div id="chart-this-year-legend" class="chart-legend"></div>
                        <canvas id="chart-this-year" data-chart="{{ json_encode($this_year_report[0], JSON_FORCE_OBJECT) }}" style="width: 100%"></canvas>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection
