<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ elixir('css/app.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body class="fixed-sn white-skin">
<header>
    <!-- Sidebar navigation -->
    <ul id="slide-out" class="side-nav fixed gradient">
        <!-- Logo -->
        <li>
            <div class="user-box">
                <p class="text-center"><i class="material-icons medium">account_circle</i></p>
                <p class="small text-center">@if(session('store') && session('cashier'))<a href="{{ routeWithAccount('forget_logged_in_cashier_store') }}" class="forget-logged-in-cashier-store">@endif <span class="bold-700">{{ session('store') ? session('store')->name : '-' }}</span> {{ session('cashier') ? '(' . session('cashier')->name . ')' : '-' }}@if(session('store') && session('cashier'))</a>@endif</p>
            </div>
        </li>
        <!--/. Logo -->
        <!-- Side navigation links -->
        <li>
            <ul class="collapsible collapsible-accordion">
                <li><a href="{{ url('/home') }}" class="collapsible-header waves-effect arrow-r"><i class="fa fa-home"></i> Home</a></li>
                <li role="separator" class="divider">&nbsp;</li>
                <li><a href="{{ routeWithAccount('showPos') }}" class="collapsible-header waves-effect arrow-r"><i class="fa fa-keyboard-o"></i> POS</a></li>
                <li><a href="{{ routeWithAccount('products.index') }}" class="collapsible-header waves-effect arrow-r"><i class="fa fa-product-hunt"></i> Products</a></li>
                <li><a href="{{ routeWithAccount('customers.index') }}" class="collapsible-header waves-effect arrow-r"><i class="fa fa-users"></i> Customers</a></li>
                <li><a href="{{ routeWithAccount('orders.index') }}" class="collapsible-header waves-effect arrow-r"><i class="fa fa-money"></i> Orders</a></li>
                <li role="separator" class="divider">&nbsp;</li>
                @can('view', App\User::class)<li><a href="{{ routeWithAccount('users.index') }}" class="collapsible-header waves-effect arrow-r"><i class="fa fa-users"></i> Users</a></li>@endcan
                @can('view', App\Store::class)<li><a href="{{ routeWithAccount('stores.index') }}" class="collapsible-header waves-effect arrow-r"><i class="fa fa-building"></i> Stores</a></li>@endcan
                <li><a href="{{ url('/cashiers') }}" class="collapsible-header waves-effect arrow-r"><i class="fa fa-users"></i> Cashiers</a></li>
                <li role="separator" class="divider">&nbsp;</li>
                <li><a href="{{ url('/profile') }}" class="collapsible-header waves-effect arrow-r"><i class="fa fa-user"></i> Profile</a></li>
                <li><a href="#" class="collapsible-header waves-effect arrow-r"><i class="fa fa-question-circle" aria-hidden="true"></i> FAQ</a></li>
            </ul>
        </li>
        <!--/. Side navigation links -->
    </ul>
    <!--/. Sidebar navigation -->
    <!-- Navbar -->
    <nav class="navbar navbar-fixed-top navbar-toggleable-md scrolling-navbar double-nav">
        <!-- SideNav slide-out button -->
        <div class="pull-left">
            <a href="#" data-activates="slide-out" class="button-collapse"><i class="fa fa-bars"></i></a>
        </div>
        <!-- Breadcrumb-->
        <div class="breadcrumb-dn">
            <p>{{ Auth::user()->account->company}}</p>
        </div>
        <ul class="nav navbar-nav pull-right">
            <li class="nav-item">
                <a href="{{ url('/logout') }}" class="nav-link waves-effect waves-light" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="fa fa-sign-out"></i> Logout ({{ Auth::user()->name }})
                </a>
                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>
        </ul>
    </nav>
    <!-- /.Navbar -->
</header>

<main>
    <div class="container-fluid">
        @yield('content')
    </div>
</main>

<!-- Scripts -->
<script src="{{ elixir('js/bootstrap.js') }}"></script>
<script src="{{ elixir('libs/MDB_3.4.0/js/mdb.min.js') }}"></script>
<script src="{{ elixir('js/app.js') }}"></script>
</body>
</html>