/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the body of the page. From here, you may begin adding components to
 * the application, or feel free to tweak this setup for your needs.
 */
/*
Vue.component('example', require('./components/Example.vue'));

const app = new Vue({
    el: 'body'
});
*/

/**
 * Keyboard to help touchscreens
 */
/*
$('.keyboard').keyboard({
    language: 'pt',
    layout: 'portuguese-qwerty',
    css: {
        // input & preview
        input: 'form-control',
        // keyboard container
        container: 'center-block dropdown-menu', // jumbotron
        // default state
        buttonDefault: 'btn btn-default',
        // hovered button
        buttonHover: 'btn-primary',
        // Action keys (e.g. Accept, Cancel, Tab, etc);
        // this replaces "actionClass" option
        buttonAction: 'active',
        // used when disabling the decimal button {dec}
        // when a decimal exists in the input area
        buttonDisabled: 'disabled'
    }
}).addTyping();
*/

/**
 * Sales report
 */
function salesReport(canvas_id) {

    var $chart = $('#' + canvas_id);
    if (!$chart.length) return;

    var data = JSON.parse($chart.attr('data-chart')),
        ctx = document.getElementById(canvas_id).getContext('2d'),
        options = {
            title: '',
            responsive: true,
            tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= value %>",
            multiTooltipTemplate: "<%= value %>"
        },
        chartData = {
            labels: [], // currently empty will contain all the labels for the data points
            datasets: [
                {
                    label: "Sales",
                    fillColor: "rgba(63, 191, 63,0.2)",
                    strokeColor: "rgba(63, 191, 63,1)",
                    pointColor: "rgba(63, 191, 63,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(63, 191, 63,1)",
                    data: [] // currently empty will contain all the data points for bills
                },
                {
                    label: "Taxes",
                    fillColor: "rgba(142, 61, 61,0.2)",
                    strokeColor: "rgba(142, 61, 61,1)",
                    pointColor: "rgba(142, 61, 61,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(142, 61, 61,1)",
                    data: [] // currently empty will contain all the data points for bills
                },
                {
                    label: "Shippings",
                    fillColor: "rgba(191, 127, 63,0.2)",
                    strokeColor: "rgba(191, 127, 63,1)",
                    pointColor: "rgba(191, 127, 63,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(191, 127, 63,1)",
                    data: [] // currently empty will contain all the data points for bills
                },
                {
                    label: "Discounts",
                    fillColor: "rgba(191, 191, 63,0.2)",
                    strokeColor: "rgba(191, 191, 63,1)",
                    pointColor: "rgba(191, 191, 63,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(191, 191, 63,1)",
                    data: [] // currently empty will contain all the data points for bills
                }
            ]
        };

    $.each(data.totals, function(date, total) {
        // Label
        chartData.labels.push(date);
        // Values
        chartData.datasets[0].data.push(total.sales);
        chartData.datasets[1].data.push(total.tax);
        chartData.datasets[2].data.push(total.shipping);
        chartData.datasets[3].data.push(total.discount);
    });

    var chart = new Chart(ctx).Line(chartData, options);
    $('#' + canvas_id + '-legend').html(chart.generateLegend());
}

/**
 * Sales report
 */
function topSellersReport(canvas_id) {
    var $chart = $('#' + canvas_id);
    if (!$chart.length) return;

    var data = JSON.parse($chart.attr('data-chart')),
        chartData = [],
        ctx = document.getElementById(canvas_id).getContext('2d'),
        options = {
            title: '',
            responsive: true,
        };

    $.each(data, function(key, product) {
        chartData.push(
            {
                value: product.quantity,
                label: product.name,
                color: "#46BFBD",
                highlight: "#5AD3D1"
            }
        );
    });

    var chart = new Chart(ctx).PolarArea(chartData, options);
    $('#' + canvas_id + '-legend').html(chart.generateLegend());
}

$(document).ready(function () {
    // Selects
    $('select:not(.md-select2)').material_select();

    salesReport('chart-last-7-days');
    salesReport('chart-this-month');
    salesReport('chart-last-month');
    salesReport('chart-this-year');
    topSellersReport('chart-top-sellers');
});

// Data Picker Initialization
$('.datepicker').pickadate({
    formatSubmit: 'yyyy-mm-dd',
    hiddenName: true,
    closeOnSelect: true
});

// Sidenav Initialization
$(".button-collapse").sideNav();

// Tooltips Initialization
$('[data-toggle="tooltip"]').tooltip()

// Modals
$('.modal-trigger').leanModal();

var $variations = $('#pos_product_variations').modal({show: false});

// Draggable items
$('.draggable li a').draggable({
    revert: 'invalid',
    helper: 'clone'
});

/**
 * Attach droppabling
 */
$('.droppable.attach').droppable({
    drop: function( event, ui ) {
        var $form = $(this),
            $draggable = $(ui.draggable),
            $toAttach = $form.find('.to-attach'),
            data = $draggable.data();
            delete data.uiDraggable;

        // Save cashier
        var jqxhr = $.ajax({
            method: 'POST',
            url: $form.attr('action'),
            data: data,
            datatype: 'html',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        })
            .done(function(data) {
                if (data) {
                    $toAttach.append(data);
                }
            })
            .fail(function() {
            })
            .always(function() {
            });
    }
});


/**
 * Detach
 */
$(document).on('click', '.detach', function(e) {
    e.preventDefault();

    var $toDetach = $(this);

    var jqxhr = $.ajax({
        method: 'GET',
        url: $(this).attr('href'),
        datatype: 'json'
    })
        .done(function(data) {
            if (data.removed) {
                $toDetach.closest('li').remove();
            }
        })
        .fail(function() {
        })
        .always(function() {
        });
});

/**
 * Search products with select2
 */
function searchBySku(sku) {
    var $form = $('.pos-add-product'),
        $sku =  $form.find('input[name="sku"]');
    $sku.val(sku);
    $form.submit();
}

function select2FormatProduct(product) {
    if (product.loading) return product.name;

    return product.view;
}

$('.md-select2.search-product', document).select2({
    ajax: {
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
                q: params.term, // search term
                page: params.page
            };
        },
        processResults: function (data, params) {
            params.page = params.page || 1;

            return {
                results: data.items.map(function(item) {
                    item.id = item.sku;
                    item.text = item.name;
                    return item;
                }),
                pagination: {
                    more: (params.page * 5) < data.total_count
                }
            };
        },
        cache: true
    },
    escapeMarkup: function (markup) { return markup; },
    minimumInputLength: 1,
    templateResult: select2FormatProduct
});

$('.md-select2.search-product', document).on('select2:select', function (e) {
    var $el = $(this),
        $form = $el.closest('form'),
        product = e.params.data;

    if (product.type === 'simple') {
        $form.submit();
    } else if (product.type === 'variable') {
        $variations.find('.modal-body').html(product.variations_view);
        $variations.modal('show');
    }

    return;
});

$(document).on('click', '.pos-choose-variation', function (e) {
    e.preventDefault();
    var $el = $(this),
        sku = $el.attr('data-sku');

    searchBySku(sku);
    $variations.modal('hide');

    return;
});

/**
 * Add product to the order by ajax
 */
$(document).on('submit', '.pos-add-product', function(e) {
    e.preventDefault();

    var $form = $(this),
        $toAddProducts = $('#pos_products .line-items'),
        $toAddTotals = $('#pos_totals'),
        $paymentBalanceDue = $('.pos-add-payment').find('input[name="payment_balance_due"]'),
        $firstPaymentMethod = $('.pos-add-payment').find('input:eq(4)'),
        $sku = $form.find('input[name="sku"]'),
        $search = $form.find('select[name="search"]');

    if (!$sku.val() && !$search) {
        toastr.warning('Please enter/search a SKU.');
        return;
    }

    var sku = $search.val() ? $search.val() : $sku.val();

    var jqxhr = $.ajax({
        method: 'POST',
        url: $form.attr('action'),
        data: {
            sku: sku,
        },
        datatype: 'html',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    })
        .done(function(data) {
            if (!data) {
                toastr.warning('This SKU doesn\'t exists.');
                return;
            }

            var total = $(data.totals).find('.total-to-pay').attr('data-value');

            $toAddProducts.html(data.products);
            $toAddTotals.html(data.totals);
            $paymentBalanceDue.val(total).trigger('change');
            $firstPaymentMethod.val(total).trigger('change');
            $form[0].reset();
            $search.val(null).trigger('change');
        })
        .fail(function() {
        })
        .always(function() {
        });
});

/**
 * Show/Hide inputs to edit order line items
 */
$(document).on('change', '#line_items .modify', function(e) {
    e.preventDefault();

    var $el = $(this),
        $toAddProducts = $('#pos_products .line-items'),
        $toAddTotals = $('#pos_totals'),
        $tr = $el.closest('tr'),
        $view = $tr.find('.view'),
        $edit = $tr.find('.edit'),
        line_item_id = $el.val(),
        line_item_qty = $('#line_qty_' + line_item_id).val(),
        line_item_price = $('#line_price_' + line_item_id).val(),
        url = $tr.data('href');

    // Only edit a line item at a time
    if ($el.is(':checked') && $('#line_items tr.editing').length === 1) {
        $el.prop('checked', false);
        toastr.warning('Edit one line at a time.');
        return;
    }

    // Editing new row
    if ($el.is(':checked')) {
        $tr.addClass('editing');
        $view.hide();
        $edit.show();
        return;
    }

    // Finish editing line item

    var jqxhr = $.ajax({
        method: 'POST',
        url: url,
        data: {
            qty : line_item_qty,
            price : line_item_price
        },
        datatype: 'html',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    })
        .done(function(data) {
            if (!data) {
                return;
            }

            $toAddProducts.html(data.products);
            $toAddTotals.html(data.totals);
        })
        .fail(function() {
        })
        .always(function() {
        });
});

/**
 * Remove product from the order
 */
$(document).on('click', '.pos-remove-line-item', function(e) {
    e.preventDefault();

    var $el = $(this),
        $toAddProducts = $('#pos_products .line-items'),
        $toAddTotals = $('#pos_totals'),
        url = $el.attr('href');

    var jqxhr = $.ajax({
        method: 'POST',
        url: url,
        datatype: 'html',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    })
        .done(function(data) {
            if (!data) {
                return;
            }

            $toAddProducts.html(data.products);
            $toAddTotals.html(data.totals);
        })
        .fail(function() {
        })
        .always(function() {
        });
});

/**
 * Search customers with select2
 */
function select2FormatCustomer(customer) {
    if (customer.loading) return customer.name;

    return customer.view;
}

$('.md-select2.search-customer', document).select2({
    ajax: {
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
                q: params.term, // search term
                page: params.page
            };
        },
        processResults: function (data, params) {
            params.page = params.page || 1;

            return {
                results: data.items.map(function(item) {
                    item.text = item.name;
                    return item;
                }),
                pagination: {
                    more: (params.page * 5) < data.total_count
                }
            };
        },
        cache: true
    },
    escapeMarkup: function (markup) { return markup; },
    minimumInputLength: 1,
    templateResult: select2FormatCustomer
});

$('.md-select2.search-customer', document).on('select2:select', function (e) {
    var $el = $(this),
        $form = $el.closest('form'),
        customer = e.params.data;

    $('#customer_id').val(customer.id);
    $('#billing_company').val(customer.billing.company).trigger('change');
    $('#billing_first_name').val(customer.billing.first_name).trigger('change');
    $('#billing_last_name').val(customer.billing.last_name).trigger('change');
    $('#billing_address_1').val(customer.billing.address_1).trigger('change');
    $('#billing_address_2').val(customer.billing.address_2).trigger('change');
    $('#billing_city').val(customer.billing.city).trigger('change');
    $('#billing_state').val(customer.billing.state).trigger('change');
    $('#billing_postcode').val(customer.billing.postcode).trigger('change');
    $('#billing_country').val(customer.billing.country).trigger('change');
    $('#billing_email').val(customer.billing.email).trigger('change');
    $('#billing_phone').val(customer.billing.phone).trigger('change');

    $('.pos-add-customer').submit();

    return;
});

$('.pos-add-customer').on('submit', function (e) {
    e.preventDefault();

    var $form = $(this),
        $clickedBtn = $(document.activeElement),
        url = $form.attr('action');

    if ($clickedBtn.hasClass('remove-customer')) {
        $('input', $form).each(function(key, elem) {
            $(elem).val('').trigger('change');
        });
    }

    var jqxhr = $.ajax({
        method: 'POST',
        url: url,
        data: $form.serialize(),
        datatype: 'json',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    })
        .done(function(data) {
            if (data.status == 1) {
                toastr.success('Customer saved successfully.');
                return;
            }

            toastr.warning('An error has occurred. Try again.');
            return;
        })
        .fail(function() {
        })
        .always(function() {
        });
});

/**
 * POS change calculator
 */
$('.pos-add-payment input[name="payment_balance_due"], .pos-add-payment input[name="payment_amount_tendered"]').on('change', function (e) {
    var $balanceDue = $('.pos-add-payment input[name="payment_balance_due"]'),
        $amountTendered = $('.pos-add-payment input[name="payment_amount_tendered"]'),
        $change = $('.pos-add-payment input[name="payment_change"]'),
        total = (parseFloat($amountTendered.val()) - parseFloat($balanceDue.val())).toFixed(2);

    $change.val(total).trigger('change');

    if (total >= 0) {
        $change
            .removeClass('invalid')
            .addClass('valid');
    } else {
        $change
            .removeClass('valid')
            .addClass('invalid');
    }

    return;
});