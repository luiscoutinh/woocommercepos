<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->increments('id')->comment('Account id');
            $table->string('company')->comment('Company name');
            $table->string('subdomain', 200)->unique()->comment('Account domain');
            $table->string('email')->comment('E-mail used to contact our customer');
            $table->tinyInteger('status')->default(1)->comment('Account status: 0 = inactive and 1 = active');
            $table->string('status_reason')->nullable()->comment('Reason for the current status - eg. Payment missing');
            $table->string('wc_url')->default('')->comment('Wordpress url');
            $table->string('wc_consumer_key')->default('')->comment('WooCommerce consumer key');
            $table->string('wc_consumer_secret')->default('')->comment('WooCommerce consumer secret');
            $table->string('wc_timezone')->default('')->comment('WooCommerce timezone');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
