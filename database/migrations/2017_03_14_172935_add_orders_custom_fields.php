<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrdersCustomFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accounts', function (Blueprint $table) {
            $table->string('wc_order_username', 30)->default('ets_username')->comment('Custom field name for order username');
            $table->string('wc_order_store', 30)->default('ets_store')->comment('Custom field name for order store');
            $table->string('wc_order_cashier', 30)->default('ets_cashier')->comment('Custom field name for order cashier');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounts', function (Blueprint $table) {
            $table->dropColumn('wc_order_username');
            $table->dropColumn('wc_order_store');
            $table->dropColumn('wc_order_cashier');
        });
    }
}
