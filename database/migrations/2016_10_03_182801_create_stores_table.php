<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->increments('id')->comment('Store id');
            $table->integer('account_id')->unsigned()->comment('Account id');
            $table->string('name')->comment('Store name');
            $table->string('timezone')->comment('Local timezone');
            $table->timestamps();
            $table->softDeletes();

            // Foreign keys
            $table->foreign('account_id')
                ->references('id')->on('accounts')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stores', function ($table) {
            $table->dropForeign(['account_id']);
        });

        Schema::dropIfExists('stores');
    }
}
