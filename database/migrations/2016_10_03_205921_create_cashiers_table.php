<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashiersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create 'cashiers' table
        Schema::create('cashiers', function (Blueprint $table) {
            $table->increments('id')->comment('Cashier id');
            $table->integer('account_id')->unsigned()->comment('Account id');
            $table->string('name', 120)->comment('Cashier name');
            $table->string('password')->nullable()->comment('Cashier password');
            $table->timestamps();
            $table->softDeletes();

            // Foreign keys
            $table->foreign('account_id')
                ->references('id')->on('accounts')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cashiers', function ($table) {
            $table->dropForeign(['account_id']);
        });

        Schema::dropIfExists('cashiers');
    }
}
