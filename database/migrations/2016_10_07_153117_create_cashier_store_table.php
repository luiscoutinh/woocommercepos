<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashierStoreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cashier_store', function (Blueprint $table) {
            $table->integer('cashier_id')->unsigned()->comment('Cashier id');
            $table->integer('store_id')->unsigned()->comment('Store id');
            $table->timestamps();
            $table->softDeletes();

            // Foreign keys
            $table->foreign('cashier_id')
                ->references('id')->on('cashiers')
                ->onDelete('cascade');

            $table->foreign('store_id')
                ->references('id')->on('stores')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cashier_store', function ($table) {
            $table->dropForeign(['cashier_id']);
            $table->dropForeign(['store_id']);
        });

        Schema::dropIfExists('cashier_store');
    }
}
