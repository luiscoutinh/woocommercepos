<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAccountSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::table('accounts', function (Blueprint $table) {
		    $table->char('wc_currency', 3)->default('EUR')->comment('Currency: ISO 4217');
		    $table->string('wc_price_format', 12)->default('left_space')->comment('Price format - left, right, left_space, right_space');
		    $table->char('wc_thousand_separator', 1)->default('.')->comment('Thousand separator');
		    $table->char('wc_decimal_separator', 1)->default(',')->comment('Decimal separator');
		    $table->tinyInteger('wc_price_decimals')->default(2)->comment('Price decimals');
		    $table->boolean('wc_tax_enabled')->default(true)->comment('Tax enabled?');
		    $table->string('wc_tax_total_display')->default('itemized')->comment('Tax total display - single, itemized');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('accounts', function (Blueprint $table) {
		    $table->dropColumn('wc_currency');
		    $table->dropColumn('wc_price_format');
		    $table->dropColumn('wc_thousand_separator');
		    $table->dropColumn('wc_decimal_separator');
		    $table->dropColumn('wc_price_decimals');
		    $table->dropColumn('wc_tax_enabled');
		    $table->dropColumn('wc_tax_total_display');
	    });
    }
}
