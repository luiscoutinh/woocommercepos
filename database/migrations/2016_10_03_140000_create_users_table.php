<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id')->comment('User id');
            $table->integer('account_id')->unsigned()->comment('Account id');
            $table->enum('type', ['admin', 'manager', 'user'])->default('admin')->comment('User type (admin, manager, user)');
            $table->string('username', 100)->comment('Username to sign in');
            $table->string('password')->comment('User password to sign in');
            $table->rememberToken();
            $table->string('name', 120)->comment('First name used to contact our customer');
            $table->timestamps();
            $table->softDeletes();

            // Foreign keys
            $table->foreign('account_id')
                ->references('id')->on('accounts')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function ($table) {
            $table->dropForeign(['account_id']);
        });

        Schema::drop('users');
    }
}
