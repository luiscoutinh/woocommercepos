<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Seed accounts table
        $this->call(AccountsTableSeeder::class);
        // Seed users table
        $this->call(UsersTableSeeder::class);
        // Seed stores table
        $this->call(StoresTableSeeder::class);
        // Seed cashiers table
        $this->call(CashiersTableSeeder::class);

        // Seed store_user table
        $this->call(StoreUserTableSeeder::class);
        // Seed cashier_store table
        $this->call(CashierStoreTableSeeder::class);
    }
}
