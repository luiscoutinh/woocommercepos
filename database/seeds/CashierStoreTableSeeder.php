<?php

use Illuminate\Database\Seeder;

class CashierStoreTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create associations between cashiers and stores for admin account
        $cashier_store = new \App\CashierStore();
        $cashier_store->cashier_id = 1;
        $cashier_store->store_id = 1;
        $cashier_store->save();

        $cashier_store = new \App\CashierStore();
        $cashier_store->cashier_id = 1;
        $cashier_store->store_id = 2;
        $cashier_store->save();

        $cashier_store = new \App\CashierStore();
        $cashier_store->cashier_id = 2;
        $cashier_store->store_id = 1;
        $cashier_store->save();

        $cashier_store = new \App\CashierStore();
        $cashier_store->cashier_id = 3;
        $cashier_store->store_id = 1;
        $cashier_store->save();

        $cashier_store = new \App\CashierStore();
        $cashier_store->cashier_id = 4;
        $cashier_store->store_id = 2;
        $cashier_store->save();


        // Create associations between cashiers and stores for demo account
        $cashier_store = new \App\CashierStore();
        $cashier_store->cashier_id = 6;
        $cashier_store->store_id = 7;
        $cashier_store->save();

        $cashier_store = new \App\CashierStore();
        $cashier_store->cashier_id = 6;
        $cashier_store->store_id = 8;
        $cashier_store->save();

        $cashier_store = new \App\CashierStore();
        $cashier_store->cashier_id = 7;
        $cashier_store->store_id = 7;
        $cashier_store->save();

        $cashier_store = new \App\CashierStore();
        $cashier_store->cashier_id = 8;
        $cashier_store->store_id = 7;
        $cashier_store->save();

        $cashier_store = new \App\CashierStore();
        $cashier_store->cashier_id = 9;
        $cashier_store->store_id = 8;
        $cashier_store->save();
    }
}
