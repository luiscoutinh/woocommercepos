<?php

use Illuminate\Database\Seeder;

class CashiersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create cashiers for admin account
        $cashier = new \App\Cashier();
        $cashier->account_id = 1;
        $cashier->name = 'Luis';
        $cashier->password = Hash::make('1234');
        $cashier->save();

        $cashier = new \App\Cashier();
        $cashier->account_id = 1;
        $cashier->name = 'Maria';
        $cashier->password = Hash::make('1234');
        $cashier->save();

        $cashier = new \App\Cashier();
        $cashier->account_id = 1;
        $cashier->name = 'Marcelo';
        $cashier->save();

        $cashier = new \App\Cashier();
        $cashier->account_id = 1;
        $cashier->name = 'António';
        $cashier->save();

        $cashier = new \App\Cashier();
        $cashier->account_id = 1;
        $cashier->name = 'Mariana';
        $cashier->save();


        // Create cashiers for demo account
        $cashier = new \App\Cashier();
        $cashier->account_id = 2;
        $cashier->name = 'Luis';
        $cashier->password = Hash::make('1234');
        $cashier->save();

        $cashier = new \App\Cashier();
        $cashier->account_id = 2;
        $cashier->name = 'Maria';
        $cashier->password = Hash::make('1234');
        $cashier->save();

        $cashier = new \App\Cashier();
        $cashier->account_id = 2;
        $cashier->name = 'Marcelo';
        $cashier->save();

        $cashier = new \App\Cashier();
        $cashier->account_id = 2;
        $cashier->name = 'António';
        $cashier->save();

        $cashier = new \App\Cashier();
        $cashier->account_id = 2;
        $cashier->name = 'Mariana';
        $cashier->save();
    }
}
