<?php

use Illuminate\Database\Seeder;

class StoreUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create associations between stores and users for admin account
        $store_user = new \App\StoreUser();
        $store_user->store_id = 1;
        $store_user->user_id = 2;
        $store_user->save();

        $store_user = new \App\StoreUser();
        $store_user->store_id = 2;
        $store_user->user_id = 2;
        $store_user->save();

        $store_user = new \App\StoreUser();
        $store_user->store_id = 3;
        $store_user->user_id = 2;
        $store_user->save();

        $store_user = new \App\StoreUser();
        $store_user->store_id = 4;
        $store_user->user_id = 2;
        $store_user->save();

        $store_user = new \App\StoreUser();
        $store_user->store_id = 5;
        $store_user->user_id = 2;
        $store_user->save();

        $store_user = new \App\StoreUser();
        $store_user->store_id = 6;
        $store_user->user_id = 3;
        $store_user->save();

        $store_user = new \App\StoreUser();
        $store_user->store_id = 1;
        $store_user->user_id = 4;
        $store_user->save();

        $store_user = new \App\StoreUser();
        $store_user->store_id = 2;
        $store_user->user_id = 4;
        $store_user->save();

        $store_user = new \App\StoreUser();
        $store_user->store_id = 3;
        $store_user->user_id = 4;
        $store_user->save();

        $store_user = new \App\StoreUser();
        $store_user->store_id = 4;
        $store_user->user_id = 5;
        $store_user->save();

        $store_user = new \App\StoreUser();
        $store_user->store_id = 5;
        $store_user->user_id = 5;
        $store_user->save();

        $store_user = new \App\StoreUser();
        $store_user->store_id = 6;
        $store_user->user_id = 5;
        $store_user->save();


        // Create associations between stores and users for demo account
        $store_user = new \App\StoreUser();
        $store_user->store_id = 7;
        $store_user->user_id = 7;
        $store_user->save();

        $store_user = new \App\StoreUser();
        $store_user->store_id = 8;
        $store_user->user_id = 7;
        $store_user->save();

        $store_user = new \App\StoreUser();
        $store_user->store_id = 9;
        $store_user->user_id = 7;
        $store_user->save();

        $store_user = new \App\StoreUser();
        $store_user->store_id = 10;
        $store_user->user_id = 7;
        $store_user->save();

        $store_user = new \App\StoreUser();
        $store_user->store_id = 11;
        $store_user->user_id = 7;
        $store_user->save();

        $store_user = new \App\StoreUser();
        $store_user->store_id = 12;
        $store_user->user_id = 8;
        $store_user->save();

        $store_user = new \App\StoreUser();
        $store_user->store_id = 7;
        $store_user->user_id = 9;
        $store_user->save();

        $store_user = new \App\StoreUser();
        $store_user->store_id = 8;
        $store_user->user_id = 9;
        $store_user->save();

        $store_user = new \App\StoreUser();
        $store_user->store_id = 9;
        $store_user->user_id = 9;
        $store_user->save();

        $store_user = new \App\StoreUser();
        $store_user->store_id = 10;
        $store_user->user_id = 10;
        $store_user->save();

        $store_user = new \App\StoreUser();
        $store_user->store_id = 11;
        $store_user->user_id = 10;
        $store_user->save();

        $store_user = new \App\StoreUser();
        $store_user->store_id = 12;
        $store_user->user_id = 10;
        $store_user->save();
    }
}
