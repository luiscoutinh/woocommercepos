<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Accunt: Admin; User: admin
        $user = new \App\User();
        $user->account_id = 1;
        $user->type = 'admin';
        $user->username = 'admin';
        $user->password = Hash::make('admin');
        $user->name = 'Admin';
        $user->save();

        // Accunt: Admin; User: manager
        $user = new \App\User();
        $user->account_id = 1;
        $user->type = 'manager';
        $user->username = 'manager';
        $user->password = Hash::make('manager');
        $user->name = 'Manager';
        $user->save();

        // Accunt: Admin; User: manager
        $user = new \App\User();
        $user->account_id = 1;
        $user->type = 'manager';
        $user->username = 'manager2';
        $user->password = Hash::make('manager2');
        $user->name = 'Manager 2';
        $user->save();

        // Accunt: Admin; User: user
        $user = new \App\User();
        $user->account_id = 1;
        $user->type = 'user';
        $user->username = 'user';
        $user->password = Hash::make('user');
        $user->name = 'User';
        $user->save();

        // Accunt: Admin; User: user
        $user = new \App\User();
        $user->account_id = 1;
        $user->type = 'user';
        $user->username = 'user2';
        $user->password = Hash::make('user2');
        $user->name = 'User 2';
        $user->save();


        // Accunt: Demo; User: admin
        $user = new \App\User();
        $user->account_id = 2;
        $user->type = 'admin';
        $user->username = 'admin';
        $user->password = Hash::make('admin');
        $user->name = 'Admin';
        $user->save();

        // Accunt: Demo; User: manager
        $user = new \App\User();
        $user->account_id = 2;
        $user->type = 'manager';
        $user->username = 'manager';
        $user->password = Hash::make('manager');
        $user->name = 'Manager';
        $user->save();

        // Accunt: Demo; User: manager
        $user = new \App\User();
        $user->account_id = 2;
        $user->type = 'manager';
        $user->username = 'manager2';
        $user->password = Hash::make('manager2');
        $user->name = 'Manager 2';
        $user->save();

        // Accunt: Demo; User: user
        $user = new \App\User();
        $user->account_id = 2;
        $user->type = 'user';
        $user->username = 'user';
        $user->password = Hash::make('user');
        $user->name = 'User';
        $user->save();

        // Accunt: Demo; User: user
        $user = new \App\User();
        $user->account_id = 2;
        $user->type = 'user';
        $user->username = 'user2';
        $user->password = Hash::make('user2');
        $user->name = 'User 2';
        $user->save();
    }
}
