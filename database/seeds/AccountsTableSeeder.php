<?php

use Illuminate\Database\Seeder;

class AccountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Admin user
        $account = new \App\Account();
        $account->company = 'Demo 1';
        $account->subdomain = 'demo1';
        $account->email = 'mail@demo1.com';
        $account->wc_url = 'https://demo1.dev';
        $account->wc_consumer_key = 'ck_924cf251f002464b99ef6bdbe4bd92154f02af1c';
        $account->wc_consumer_secret = 'cs_c9ead754d6fc659834222a1dcadf9d198d7d04ec';
        $account->wc_timezone = 'Europe/Lisbon';
        $account->save();

        // Demo user
        $account = new \App\Account();
        $account->company = 'Demo 2';
        $account->subdomain = 'demo 2';
        $account->email = 'mail@demo2.com';
        $account->wc_url = 'https://demo2.dev';
        $account->wc_consumer_key = 'ck_924cf251f002464b99ef6bdbe4bd92154f02af1c';
        $account->wc_consumer_secret = 'cs_c9ead754d6fc659834222a1dcadf9d198d7d04ec';
        $account->wc_timezone = 'Europe/Lisbon';
        $account->save();
    }
}
