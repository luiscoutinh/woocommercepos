<?php

use Illuminate\Database\Seeder;

class StoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Admin stores
        $store = new \App\Store();
        $store->account_id = 1;
        $store->name = 'Aveiro';
        $store->timezone = 'Europe/Lisbon';
        $store->save();

        $store = new \App\Store();
        $store->account_id = 1;
        $store->name = 'Porto';
        $store->timezone = 'Europe/Lisbon';
        $store->save();

        $store = new \App\Store();
        $store->account_id = 1;
        $store->name = 'Lisbon';
        $store->timezone = 'Europe/Lisbon';
        $store->save();

        $store = new \App\Store();
        $store->account_id = 1;
        $store->name = 'Paris';
        $store->timezone = 'Europe/Lisbon';
        $store->save();

        $store = new \App\Store();
        $store->account_id = 1;
        $store->name = 'London';
        $store->timezone = 'Europe/Lisbon';
        $store->save();

        $store = new \App\Store();
        $store->account_id = 1;
        $store->name = 'New York';
        $store->timezone = 'Europe/Lisbon';
        $store->save();

        // Demo stores
        $store = new \App\Store();
        $store->account_id = 2;
        $store->name = 'Aveiro';
        $store->timezone = 'Europe/Lisbon';
        $store->save();

        $store = new \App\Store();
        $store->account_id = 2;
        $store->name = 'Porto';
        $store->timezone = 'Europe/Lisbon';
        $store->save();

        $store = new \App\Store();
        $store->account_id = 2;
        $store->name = 'Lisbon';
        $store->timezone = 'Europe/Lisbon';
        $store->save();

        $store = new \App\Store();
        $store->account_id = 2;
        $store->name = 'Paris';
        $store->timezone = 'Europe/Lisbon';
        $store->save();

        $store = new \App\Store();
        $store->account_id = 2;
        $store->name = 'London';
        $store->timezone = 'Europe/Lisbon';
        $store->save();

        $store = new \App\Store();
        $store->account_id = 2;
        $store->name = 'New York';
        $store->timezone = 'Europe/Lisbon';
        $store->save();
    }
}
