# WooCommerce POS

**WooCommerce POS** is an online **POS (Point Of Sale)** for **WooCommerce** that works through the **WooCommerce API** (**not a plugin for Wordpress!**).

## Easy to use

To start using WooCommerce POS a customer only needs to create an account and set the URL of his online store, a consumer key and a consumer secret previously created with the read and write permissions.

## Features

* Multiple accounts (each account has a subdomain, eg account1.example.com)
* An account is associated with a website
* An account can have multiple stores/points of sale
* An account can have multiple users (administrators, managers, and users)
* An account can have multiple cashiers
* A user can have access to one or more stores/points of sale
* A cashier can have access to one or more stores/points of sale
* Search/view products
* Search/view/create/edit/delete customers
* Search/view/create orders
* Add item to order by SKU or search
* Various types of discounts (set final value or subtract value - in value or percentage)
* Automatic exchange calculation
* Etc.

## What I used?

- Laravel Framework version 5.3.30 https://laravel.com/docs/5.3/
- MDBootstrap PRO https://mdbootstrap.com/

## Attention

I have not changed the code since 2017-03-20. The latest tests were made with version 2.6.14 of WooCommerce. So it is normal that some changes are needed to work correctly with the latest version of WooCommerce.

## Contributing

Thank you for considering contributing to the WooCommerce POS! If you are interested in contributing to the WooCommerce POS, please send an e-mail to Luis Coutinho at luis@luiscoutinho.pt. 

## License

The WooCommerce POS is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).

## Screenshots

![](https://gitlab.com/luiscoutinh/woocommercepos/raw/master/docs/images/signup.png)
![](https://gitlab.com/luiscoutinh/woocommercepos/raw/master/docs/images/login1.png)
![](https://gitlab.com/luiscoutinh/woocommercepos/raw/master/docs/images/login2.png)
![](https://gitlab.com/luiscoutinh/woocommercepos/raw/master/docs/images/users-list.png)
![](https://gitlab.com/luiscoutinh/woocommercepos/raw/master/docs/images/users-edit.png)
![](https://gitlab.com/luiscoutinh/woocommercepos/raw/master/docs/images/users-add.png)
![](https://gitlab.com/luiscoutinh/woocommercepos/raw/master/docs/images/stores-list.png)
![](https://gitlab.com/luiscoutinh/woocommercepos/raw/master/docs/images/stores-edit.png)
![](https://gitlab.com/luiscoutinh/woocommercepos/raw/master/docs/images/stores-add.png)
![](https://gitlab.com/luiscoutinh/woocommercepos/raw/master/docs/images/cashiers-list.png)
![](https://gitlab.com/luiscoutinh/woocommercepos/raw/master/docs/images/cashiers-edit.png)
![](https://gitlab.com/luiscoutinh/woocommercepos/raw/master/docs/images/cashiers-add.png)
![](https://gitlab.com/luiscoutinh/woocommercepos/raw/master/docs/images/customers-list.png)
![](https://gitlab.com/luiscoutinh/woocommercepos/raw/master/docs/images/customers-details.png)
![](https://gitlab.com/luiscoutinh/woocommercepos/raw/master/docs/images/customers-edit1.png)
![](https://gitlab.com/luiscoutinh/woocommercepos/raw/master/docs/images/customers-edit2.png)
![](https://gitlab.com/luiscoutinh/woocommercepos/raw/master/docs/images/products-list.png)
![](https://gitlab.com/luiscoutinh/woocommercepos/raw/master/docs/images/products-details1.png)
![](https://gitlab.com/luiscoutinh/woocommercepos/raw/master/docs/images/products-details2.png)
![](https://gitlab.com/luiscoutinh/woocommercepos/raw/master/docs/images/orders-list1.png)
![](https://gitlab.com/luiscoutinh/woocommercepos/raw/master/docs/images/orders-list2.png)
![](https://gitlab.com/luiscoutinh/woocommercepos/raw/master/docs/images/orders-edit1.png)
![](https://gitlab.com/luiscoutinh/woocommercepos/raw/master/docs/images/orders-edit2.png)
![](https://gitlab.com/luiscoutinh/woocommercepos/raw/master/docs/images/pos1.png)
![](https://gitlab.com/luiscoutinh/woocommercepos/raw/master/docs/images/pos2.png)
![](https://gitlab.com/luiscoutinh/woocommercepos/raw/master/docs/images/pos3.png)
![](https://gitlab.com/luiscoutinh/woocommercepos/raw/master/docs/images/pos4.png)
![](https://gitlab.com/luiscoutinh/woocommercepos/raw/master/docs/images/pos5.png)
![](https://gitlab.com/luiscoutinh/woocommercepos/raw/master/docs/images/pos6.png)
![](https://gitlab.com/luiscoutinh/woocommercepos/raw/master/docs/images/pos7.png)
![](https://gitlab.com/luiscoutinh/woocommercepos/raw/master/docs/images/pos8.png)
![](https://gitlab.com/luiscoutinh/woocommercepos/raw/master/docs/images/pos9.png)
